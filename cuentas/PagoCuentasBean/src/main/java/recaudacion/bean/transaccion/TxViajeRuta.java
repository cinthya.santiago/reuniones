package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.ViajeRuta;
import recaudacion.bean.entidades.dao.ViajeRutaDAO;
import recaudacion.interacion.InteraccionBD;

public class TxViajeRuta extends InteraccionBD
{
	private static final long serialVersionUID = 1L;
	
	public static final int BUSCA_VIAJES_RUTA = 1;  
	public static final int GUARDA_VIAJE_RUTA = 2;
	public static final int BUSCA_VIAJES_RUTA_CONDUCTOR = 3;
	
	private Integer opcion = null;
	private Integer idconductor = null;
	
	private List<ViajeRuta> listViajesRuta = null;
	
	private Date fechaIni = null;
	private Date fechaFin = null;
	private ViajeRuta viajeRuta = null;
	
	@Override
	public Serializable ejecutaTransaccion() throws Exception
	{
		switch(getOpcion().intValue())
		{
			case BUSCA_VIAJES_RUTA:
				buscaViajesRuta();
				break;
				
			case GUARDA_VIAJE_RUTA:
				guardaViajeRuta();
				break;
			case BUSCA_VIAJES_RUTA_CONDUCTOR:
				buscaViajesRutaConductor();
				break;
				
				default:
					break;
		}
		return this;
	}

	private void guardaViajeRuta() throws Exception {
		ViajeRutaDAO viajeRutaDAO = new ViajeRutaDAO();
		viajeRutaDAO.setDb(getDb());
		ViajeRuta vr = (ViajeRuta) viajeRutaDAO.guarda(getViajeRuta());
		setViajeRuta(vr);
	}

	private void buscaViajesRuta(){
		ViajeRutaDAO viajeRutaDAO = new ViajeRutaDAO();
		viajeRutaDAO.setDb(getDb());
		setListViajesRuta(viajeRutaDAO.buscaViajesRuta(getFechaIni(),getFechaFin()));
	}
	
	private void buscaViajesRutaConductor(){
		try {
			ViajeRutaDAO dao = new ViajeRutaDAO();
			dao.setDb(getDb());
			setListViajesRuta(dao.buscaViajesRutaConductor(getIdconductor()));
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public List<ViajeRuta> getListViajesRuta() {
		return listViajesRuta;
	}

	public void setListViajesRuta(List<ViajeRuta> listViajesRuta) {
		this.listViajesRuta = listViajesRuta;
	}

	public Date getFechaIni() {
		return fechaIni;
	}

	public void setFechaIni(Date fechaIni) {
		this.fechaIni = fechaIni;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public ViajeRuta getViajeRuta() {
		return viajeRuta;
	}

	public void setViajeRuta(ViajeRuta viajeRuta) {
		this.viajeRuta = viajeRuta;
	}

	public Integer getIdconductor() {
		return idconductor;
	}

	public void setIdconductor(Integer idconductor) {
		this.idconductor = idconductor;
	}
	
}