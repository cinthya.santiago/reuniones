package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.DepositoEnCamino;
import recaudacion.bean.entidades.dao.DepositoEnCaminoDAO;
import recaudacion.interacion.InteraccionBD;

public class TxDepositoEnCamino extends InteraccionBD {

	private static final long serialVersionUID = 1L;

	private Integer opcion = null;
	private Integer idConductor = null;

	private DepositoEnCamino deposito = null;
	
	private List<DepositoEnCamino> lstDepositos = null;
	private LinkedHashMap<?, ?> hshTarjetas = null;

	public static final int BUSCA_DEPOSITOS_POR_CONDUCTOR = 1;
	public static final int GUARDA_DEPOSITOS_POR_CONDUCTOR = 2;
	public static final int BUSCA_TARJETAS_DEPOSITOS_CONDCUTOR = 3;

	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		
		switch (getOpcion().intValue()) {
		case BUSCA_DEPOSITOS_POR_CONDUCTOR:
			buscaDepositosConductor();
			break;

		case GUARDA_DEPOSITOS_POR_CONDUCTOR:
			guardaDepositos();
			break;
			
		case BUSCA_TARJETAS_DEPOSITOS_CONDCUTOR:
			recuperaTarjetasConductor();
			break;
			
		default:
			break;
		}
		return this;
	}
	

	private void recuperaTarjetasConductor() throws Exception {
		
		DepositoEnCaminoDAO dao = new DepositoEnCaminoDAO();
		dao.setConexion(getConexion());
		dao.setDb(getDb());
		List<DepositoEnCamino> depositos = dao.recuperaTarjetasConductor(getIdConductor());
		setLstDepositos(depositos);
		
		Map<Integer, List<DepositoEnCamino>> mapa = depositos.stream().collect(
				Collectors.groupingBy(deposito -> ((DepositoEnCamino)deposito).getTarjetaCuenta().getIdTarjetaCuenta()));
		setHshTarjetas(new LinkedHashMap<Integer,List<DepositoEnCamino>>(mapa));
		
		
	}
	


	private void buscaDepositosConductor(){
		try {
			DepositoEnCaminoDAO dao = new DepositoEnCaminoDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.buscaDepositoConducto(getIdConductor());
			setLstDepositos(dao.getLstDepositos());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	private void guardaDepositos() {
		try {
			DepositoEnCaminoDAO dao = new DepositoEnCaminoDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.guarda(getDeposito());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public DepositoEnCamino getDeposito() {
		return deposito;
	}

	public void setDeposito(DepositoEnCamino deposito) {
		this.deposito = deposito;
	}

	public Integer getIdConductor() {
		return idConductor;
	}

	public void setIdConductor(Integer idConductor) {
		this.idConductor = idConductor;
	}

	public List<DepositoEnCamino> getLstDepositos() {
		return lstDepositos;
	}

	public void setLstDepositos(List<DepositoEnCamino> lstDepositos) {
		this.lstDepositos = lstDepositos;
	}


	public LinkedHashMap<?, ?> getHshTarjetas() {
		return hshTarjetas;
	}


	public void setHshTarjetas(LinkedHashMap<?, ?> hshTarjetas) {
		this.hshTarjetas = hshTarjetas;
	}
}