package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.CampoVista;
import recaudacion.bean.entidades.ccuentas.CatalogoVistas;
import recaudacion.interacion.InteraccionBD;

public class TxExportadorDatos extends InteraccionBD {

	private static final long serialVersionUID = 1L;

	private Integer opcion = null;

	public static final int RECUPERA_CATALOGO_VISTAS = 1;

	public static final int GENERA_INFORMACION_REPORTE = 2;

	private List<Object> lstCatalogoVistas = null;
	private ArrayList<Object> datosVistas = null;
	private ArrayList<Object> filtros = new ArrayList<>();
	private ArrayList<Object> campos = new ArrayList<>();
	private CatalogoVistas vista = null;

	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		switch (getOpcion().intValue()) {
		case RECUPERA_CATALOGO_VISTAS:
			consultaCatalogoVistas();
			break;

		case GENERA_INFORMACION_REPORTE:
			generaReporteVista();
			break;

		default:
			break;
		}
		return this;
	}

	private void consultaCatalogoVistas() throws SQLException, Exception {
		setLstCatalogoVistas(recuperaCatalogosVistas());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			int siz = getLstCatalogoVistas().size();
			int i = 0;
			while (i < siz) {
				CatalogoVistas catalogo = (CatalogoVistas) getLstCatalogoVistas().get(i);

				String sql = "select * from " + catalogo.getEsquema() + "." + catalogo.getNombreVista();

				System.out.println(sql);
				ps = getConexion().prepareStatement(sql);
				rs = ps.executeQuery();

				ResultSetMetaData rsmd = rs.getMetaData();
				int count = rsmd.getColumnCount();
				ArrayList<Object> campos = new ArrayList<>();

				for (int j = 1; j <= count; j++) {
					CampoVista campoVista = new CampoVista();
					campoVista.setEscala(rsmd.getScale(j));
					campoVista.setNombCampo(rsmd.getColumnName(j));
					campoVista.setPresicion(rsmd.getPrecision(j));
					campoVista.setTypeClassName(rsmd.getColumnClassName(j));
					campos.add(campoVista);
				}
				campos.trimToSize();
				catalogo.setCamposVista(campos);
				i++;
			}
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);

		} finally {
			closePreparedStatement(ps);
			
			closeResultSet(rs);
		}
	}

	private ArrayList<Object> recuperaCatalogosVistas() {
		ArrayList<Object> datos = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getConexion().prepareStatement(getQueryBuscaCatalogos());
			rs = ps.executeQuery();
			while (rs.next()) {
				datos.add(getCatalogoVistas(rs));
			}
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		} finally {
			closePreparedStatement(ps);
			closeResultSet(rs);
		}
		return datos;
	}

	private CatalogoVistas getCatalogoVistas(ResultSet rs) throws SQLException {
		CatalogoVistas catalogo = new CatalogoVistas();
		catalogo.setCatalogoVistaId(new Integer(rs.getInt("CATALOGO_VISTAS_CUENTAS_ID")));
		catalogo.setDescVista(rs.getString("DESCVISTA"));
		catalogo.setEsquema(rs.getString("ESQUEMA"));
		catalogo.setEstatus(rs.getInt("ESTATUS"));
		catalogo.setNombreVista(rs.getString("NOMBREVISTA"));
		return catalogo;
	}

	private String getQueryBuscaCatalogos() {
		return "SELECT * FROM CCUENTAS.CATALOGO_VISTAS_CUENTAS WHERE ESTATUS = 1";
	}

	private void generaReporteVista() throws SQLException, Exception {
		setDatosVistas(new ArrayList<>());
		String sql = generaSQL();
		SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy"); 

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			int siz = getFiltros().size();
			int i = 0;
			int pos = 1;

			ps = getConexion().prepareStatement(sql);
			while (i < siz) {
				CampoVista vo = (CampoVista) getFiltros().get(i);

				if (vo.getClausula() != null & !vo.getClausula().equals("")) {
					ps.setObject(pos, vo.getValor());

					if (vo.getValor2() != null) {
						pos++;
						ps.setObject(pos, vo.getValor2());
					}
					pos++;
				}
				i++;
			}

			MensajesDebug.imprimeMensaje(sql);
			rs = ps.executeQuery();
			boolean flag = rs.next();

			i = 0;
			while (flag) {
				int cSiz = getCampos().size();
				Object[] obj = new Object[cSiz];
				for (int j = 0; j < cSiz; j++) {
					CampoVista vo = (CampoVista) getCampos().get(j);
					if(vo.getNombCampo().equals("FECHA_VIAJE")){ 
						obj[j] = formato.format(rs.getTimestamp(vo.getNombCampo()));
						continue;
					}
					if (vo.getTypeClassName().equals("java.sql.Timestamp")) {
						obj[j] = rs.getTimestamp(vo.getNombCampo());
					} else {
						obj[j] = rs.getObject(vo.getNombCampo());
					}
				}
				getDatosVistas().add(obj);
				flag = rs.next();
			}
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		} finally {
			closePreparedStatement(ps);
			
			closeResultSet(rs);
		}
	}

	private String generaSQL() {

		String sql = " SELECT ";
		String fields = " ";
		String from = " FROM " + getVista().getEsquema() + "." + getVista().getNombreVista() + " ";
		String where = " ";
		String coma = " , ";
		int i = 0;
		int siz = getCampos().size();
		while (i < siz) {
			CampoVista vo = (CampoVista) getCampos().get(i);
			fields += "\"" + vo.getNombCampo() + "\"";
			if (i < siz - 1) {
				fields += coma;
			}
			i++;
		}

		siz = getFiltros().size();
		i = 0;
		if (i < siz) {
			where += " WHERE ";
			while (i < siz) {
				CampoVista vo = (CampoVista) getFiltros().get(i);
				where += " " + vo.getAndOr();

				String nombCampo = vo.getNombCampo();

				if (vo.getTypeClassName().equals("java.lang.String")) {
					nombCampo = " upper(trim( \"" + nombCampo + "\" ))";
				} else {
					nombCampo = " \"" + nombCampo + "\" ";
				}

				where += nombCampo;

				if (vo.getClausula() == null || vo.getClausula().trim().equals("")) {
					where += " " + vo.getValor() + " ";
				} else {
					where += " " + vo.getClausula() + " ";
					if (vo.getValor2() != null) {
						where += "  ? AND ? ";
					} else {
						where += " ? ";
					}

				}

				i++;
			}
		}
		return sql + fields + from + where;
	}

	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public List<Object> getLstCatalogoVistas() {
		return lstCatalogoVistas;
	}

	public void setLstCatalogoVistas(List<Object> lstCatalogoVistas) {
		this.lstCatalogoVistas = lstCatalogoVistas;
	}

	public ArrayList<Object> getFiltros() {
		return filtros;
	}

	public void setFiltros(ArrayList<Object> filtros) {
		this.filtros = filtros;
	}

	public ArrayList<Object> getCampos() {
		return campos;
	}

	public void setCampos(ArrayList<Object> campos) {
		this.campos = campos;
	}

	public ArrayList<Object> getDatosVistas() {
		return datosVistas;
	}

	public void setDatosVistas(ArrayList<Object> datosVistas) {
		this.datosVistas = datosVistas;
	}

	public CatalogoVistas getVista() {
		return vista;
	}

	public void setVista(CatalogoVistas vista) {
		this.vista = vista;
	}
}
