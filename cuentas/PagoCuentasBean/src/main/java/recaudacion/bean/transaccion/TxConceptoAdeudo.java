package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.util.List;

import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.ConceptoAdeudo;
import recaudacion.bean.entidades.dao.ConceptoAdeudoDAO;
import recaudacion.interacion.InteraccionBD;

public class TxConceptoAdeudo extends InteraccionBD{

	private static final long serialVersionUID = 1L;

	private Integer opcion;
	
	private List<ConceptoAdeudo> lstConceptoAdeudo = null;
	
	public static final int BUSCA_CONCEPTOS_ACTIVOS = 1;
	
	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		switch (getOpcion().intValue()) {
		case BUSCA_CONCEPTOS_ACTIVOS:
			buscaConceptosActivos();
			break;

		default:
			break;
		}
		return this;
	}

	private void buscaConceptosActivos()
	{
		try {
			ConceptoAdeudoDAO dao = new ConceptoAdeudoDAO();
			dao.setDb(getDb());
			dao.setConexion(getConexion());
			dao.buscaConceptoAdeudosActivos();
			setLstConceptoAdeudo(dao.getLstConceptoAdeudos());
			
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public List<ConceptoAdeudo> getLstConceptoAdeudo() {
		return lstConceptoAdeudo;
	}

	public void setLstConceptoAdeudo(List<ConceptoAdeudo> lstConceptoAdeudo) {
		this.lstConceptoAdeudo = lstConceptoAdeudo;
	}

}
