package recaudacion.bean.transaccion;

import java.io.Serializable;

import recaudacion.bean.entidades.ccuentas.Viajes;
import recaudacion.bean.entidades.dao.ViajeDAO;
import recaudacion.interacion.InteraccionBD;

public class TxCambioRecursos extends InteraccionBD{

	private static final long serialVersionUID = 1L;

	private Integer opcion = null;
	
	private Viajes viaje = null;
	
	public static final int ACTUALIZA_RECURSOS = 1;
	
	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		
		switch (getOpcion().intValue()) 
		{
			case ACTUALIZA_RECURSOS:
				actualizaRecursos();
				break;
	
			default:
				break;
		}
		
		return this;
	}
	
	private void actualizaRecursos()
	{
		try {
			ViajeDAO dao = new ViajeDAO();
			dao.setDb(getDb());
			dao.setConexion(getConexion());
			dao.setViaje(getViaje());
			dao.actualizaViaje();
			
		} catch (Exception e) {
			throw e;
		}
	}

	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public Viajes getViaje() {
		return viaje;
	}

	public void setViaje(Viajes viaje) {
		this.viaje = viaje;
	}

}
