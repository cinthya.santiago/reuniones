package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.util.List;

import recaudacion.bean.entidades.ccuentas.IslaCombustible;
import recaudacion.bean.entidades.dao.IslaCombustibleDAO;
import recaudacion.interacion.InteraccionBD;

public class TxIslaCombustible extends InteraccionBD{
	
	private static final long serialVersionUID = 1L;

	public static final int BUSCA_ISLAS_COMBUSTIBLE = 1;
	
	private Integer opcion = null;
	private List<IslaCombustible> listaIslas;
	
	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		switch(getOpcion().intValue())
		{
			case BUSCA_ISLAS_COMBUSTIBLE:
				buscaIslasCombustible();
				break;
				
				default:
					break;
		}
		return this;
	}

	private void buscaIslasCombustible() {
		IslaCombustibleDAO islaCombustibleDAO = new IslaCombustibleDAO();
		islaCombustibleDAO.setConexion(getConexion());
		islaCombustibleDAO.setDb(getDb());
		setListaIslas(islaCombustibleDAO.buscaIslasCombustible());
	}

	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public List<IslaCombustible> getListaIslas() {
		return listaIslas;
	}

	public void setListaIslas(List<IslaCombustible> listaIslas) {
		this.listaIslas = listaIslas;
	}
	
}