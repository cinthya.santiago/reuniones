package recaudacion.bean.transaccion;

import java.io.Serializable;

import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.dao.UsuarioDAO;
import recaudacion.bean.entidades.general.Usuario;
import recaudacion.interacion.InteraccionBD;

public class TxRecuperaUsuario extends InteraccionBD{

	private static final long serialVersionUID = 1L;
	
	private Integer opcion = null;
	private String clave = null;
	private Usuario usuario = null;
	
	public static final int RECUPERA_USUARIO_CLAVE = 1;
	
	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		switch (getOpcion().intValue()) {
		case RECUPERA_USUARIO_CLAVE:
			recuperaUsuarioClave();
			break;

		default:
			break;
		}
		return this;
	}
	
	
	private void recuperaUsuarioClave(){
		try {
			UsuarioDAO dao = new UsuarioDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			setUsuario(dao.buscaUsuarioPorClave(getClave()));
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	
	public Integer getOpcion() {
		return opcion;
	}
	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}


	public Usuario getUsuario() {
		return usuario;
	}


	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
