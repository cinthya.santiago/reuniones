package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.util.Date;

import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.RegistroOdometro;
import recaudacion.bean.entidades.dao.RegistroOdometroDAO;
import recaudacion.interacion.InteraccionBD;

public class TxRegistraOdometro extends InteraccionBD {

	private static final long serialVersionUID = 1L;

	private Integer opcion = null;

	private RegistroOdometro odometro = null;
	
	private Integer idEmpleado = null;
	private Integer idAutobus = null;
	private Integer kilometros = null;
	private Date fecha = null;

	public static final int GUARDA_ODOMETRO = 1;
	public static final int BUSCA_ULTIMO_ODOMETRO = 2;

	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		switch (getOpcion().intValue()) {
		case GUARDA_ODOMETRO:
			guardaOdometro();
			break;
		case BUSCA_ULTIMO_ODOMETRO:
			buscaUltimoOdometro();
			break;
		default:
			break;
		}
		return this;
	}

	private void guardaOdometro() {
		try {
			RegistroOdometroDAO dao = new RegistroOdometroDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.setOdometro(getOdometro());
			dao.insertaOdometro();
		} catch (Exception e) {
		}
	}

	private void buscaUltimoOdometro() {
		try {
			RegistroOdometroDAO dao = new RegistroOdometroDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			setOdometro(dao.buscaUltimoOdometroAutobus(getIdEmpleado(), getIdAutobus()));
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public RegistroOdometro getOdometro() {
		return odometro;
	}

	public void setOdometro(RegistroOdometro odometro) {
		this.odometro = odometro;
	}

	public Integer getIdEmpleado() {
		return idEmpleado;
	}

	public void setIdEmpleado(Integer idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public Integer getIdAutobus() {
		return idAutobus;
	}

	public void setIdAutobus(Integer idAutobus) {
		this.idAutobus = idAutobus;
	}

	public Integer getKilometros() {
		return kilometros;
	}

	public void setKilometros(Integer kilometros) {
		this.kilometros = kilometros;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
}
