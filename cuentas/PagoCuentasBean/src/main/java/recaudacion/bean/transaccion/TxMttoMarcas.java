package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.util.List;

import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.MarcaRol;
import recaudacion.bean.entidades.dao.MarcaRolDAO;
import recaudacion.interacion.InteraccionBD;

public class TxMttoMarcas extends InteraccionBD {
	private static final long serialVersionUID = 1L;

	private Integer opcion = null;
	
	private MarcaRol rol = null;

	public static final int BUSCA_MARCA_ROL = 1;
	public static final int GUARDA_MARCA_ROL = 2;

	private List<MarcaRol> lstMarcaRol = null;
	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		switch (getOpcion().intValue()) {
		case BUSCA_MARCA_ROL:
			buscaMarcaRol();
			break;
		case GUARDA_MARCA_ROL:
			guardaMarcaRol();
			break;
		default:
			break;
		}
		return this;
	}

	private void buscaMarcaRol() throws Exception {
		
		try {
			MarcaRolDAO dao = new MarcaRolDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			 setLstMarcaRol(dao.buscaMarcaRol());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}
	
	private void guardaMarcaRol() throws Exception {
		try {
			MarcaRolDAO dao = new MarcaRolDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.guarda(getRol());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}
	
	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}


	public MarcaRol getRol() {
		return rol;
	}

	public void setRol(MarcaRol rol) {
		this.rol = rol;
	}

	public List<MarcaRol> getLstMarcaRol() {
		return lstMarcaRol;
	}

	public void setLstMarcaRol(List<MarcaRol> lstMarcaRol) {
		this.lstMarcaRol = lstMarcaRol;
	}
}
