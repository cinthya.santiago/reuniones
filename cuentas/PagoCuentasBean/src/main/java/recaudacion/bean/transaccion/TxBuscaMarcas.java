package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.util.List;

import recaudacion.bean.entidades.dao.MarcaDAO;
import recaudacion.bean.entidades.general.Marca;
import recaudacion.interacion.InteraccionBD;

public class TxBuscaMarcas extends InteraccionBD
{
	private static final long serialVersionUID = 1L;

	private Integer opcion = null;
	
	private String descripcion = null;
	
	private List<Marca> lstMarcas = null;
	
	private Marca marca = null;

	public static final int BUSCA_MARCAS_CLASESERV = 1;
	public static final int BUSCA_MARCAS_DESCRIPCION = 2;
	public static final int RECUPERA_MARCA_DESCRIPCION = 3;
	
	
	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		switch (getOpcion().intValue()) {
		case BUSCA_MARCAS_CLASESERV:
			buscaMarcasClaseServicio();
			break;
		
		case BUSCA_MARCAS_DESCRIPCION:
			buscaMarcasDescripcion();
			break;
		
		case RECUPERA_MARCA_DESCRIPCION:
			recuperaMarcaDescipcion();
			break;
			
		default:
			break;
		}
		return this;
	}

	private void buscaMarcasClaseServicio() throws Exception{
		MarcaDAO ado = new MarcaDAO();
		ado.setConexion(getConexion());
		ado.setDb(getDb());
		List<Marca> marcas = ado.buscaMarcas();
		setLstMarcas(marcas);
	}
	

	
	
	private void buscaMarcasDescripcion() throws Exception
	{
		MarcaDAO dao = new MarcaDAO();
		dao.setDescripcion(getDescripcion());
		dao.setConexion(getConexion());
		dao.setDb(getDb());
		setLstMarcas(dao.buscaMarcasDescripcion());
	}
	
	private void recuperaMarcaDescipcion()
	{
		MarcaDAO dao = new MarcaDAO();
		dao.setConexion(getConexion());
		dao.setDb(getDb());
		setMarca(dao.recuperaMarcaXDescripcion(getDescripcion()));
	}
	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}
	
	public List<Marca> getLstMarcas() {
		return lstMarcas;
	}

	public void setLstMarcas(List<Marca> lstMarcas) {
		this.lstMarcas = lstMarcas;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}
}
