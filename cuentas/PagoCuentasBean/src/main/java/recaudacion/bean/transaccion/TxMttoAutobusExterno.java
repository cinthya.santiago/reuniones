package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.util.List;

import asu.bean.transaccion.Transaccion;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.Asociados;
import recaudacion.bean.entidades.ccuentas.AutobusExterno;
import recaudacion.bean.entidades.dao.AsociadosDAO;
import recaudacion.bean.entidades.dao.AutobusExternoDAO;
import recaudacion.bean.entidades.dao.MarcaDAO;
import recaudacion.bean.entidades.dao.RegionDAO;
import recaudacion.bean.entidades.dao.ZonaDAO;
import recaudacion.bean.entidades.general.Marca;
import recaudacion.bean.entidades.general.Region;
import recaudacion.bean.entidades.general.Zona;

public class TxMttoAutobusExterno extends Transaccion {

	private static final long serialVersionUID = 1L;
	private Integer opcion = null;

	private List<Region> lstRegion = null;
	private List<Marca> lstMarca = null;
	private List<Zona> lstZona = null;
	private List<Asociados> lstAsociados = null;
	private List<AutobusExterno> lstBusExterno = null;

	private Integer regionId = null;
	private Integer marcaId = null;

	private AutobusExterno autobus = null;

	public static final int BUSCA_REGIONES = 1;
	public static final int BUSCA_MARCAS = 2;
	public static final int BUSCA_ZONAS = 3;
	public static final int BUSCA_ASOCIADOS = 4;
	public static final int BUSCA_AUTOBUS_EXTERNO = 5;
	public static final int GUARDA_AUTOBUS_EXTERNO = 6;

	@Override
	public Serializable ejecutaTransaccion() {
		switch (opcion.intValue()) {
		case BUSCA_REGIONES:
			buscaRegiones();
			break;
		case BUSCA_MARCAS:
			buscaMarcas();
			break;

		case BUSCA_ZONAS:
			buscaZonas();
			break;

		case BUSCA_ASOCIADOS:
			buscaAsociados();
			break;

		case BUSCA_AUTOBUS_EXTERNO:
			buscaDatosBusExterno();
			break;

		case GUARDA_AUTOBUS_EXTERNO:
			eventoGuardar();
			break;


		default:
			break;
		}
		return this;
	}

	private void buscaRegiones() {
		try {
			RegionDAO dao = new RegionDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			setLstRegion(dao.buscaRegionesActivas());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	private void buscaMarcas() {
		try {
			MarcaDAO dao = new MarcaDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.setRegionId(getRegionId());
			setLstMarca(dao.buscaMarcasRegionActivas());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	private void buscaZonas() {
		try {
			ZonaDAO dao = new ZonaDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.setMarcaId(getMarcaId());
			setLstZona(dao.buscaZonaMarcaActivas());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	private void buscaAsociados() {
		try {
			AsociadosDAO dao = new AsociadosDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			setLstAsociados(dao.buscaAsociadosActivos());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	private void buscaDatosBusExterno() {
		try {
			AutobusExternoDAO dao = new AutobusExternoDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.setIdRegion(getRegionId());
			setLstBusExterno(dao.buscaBusExterno());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	public void eventoGuardar() {
		try {
			AutobusExternoDAO dao = new AutobusExternoDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.setAutobus(getAutobus());
			dao.guardaAutobusExterno();
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	public List<Region> getLstRegion() {
		return lstRegion;
	}

	public void setLstRegion(List<Region> lstRegion) {
		this.lstRegion = lstRegion;
	}

	public List<Marca> getLstMarca() {
		return lstMarca;
	}

	public List<Zona> getLstZona() {
		return lstZona;
	}

	public void setLstZona(List<Zona> lstZona) {
		this.lstZona = lstZona;
	}

	public void setLstMarca(List<Marca> lstMarca) {
		this.lstMarca = lstMarca;
	}

	public List<Asociados> getLstAsociados() {
		return lstAsociados;
	}

	public List<AutobusExterno> getLstBusExterno() {
		return lstBusExterno;
	}

	public void setLstBusExterno(List<AutobusExterno> lstBusExterno) {
		this.lstBusExterno = lstBusExterno;
	}

	public void setLstAsociados(List<Asociados> lstAsociados) {
		this.lstAsociados = lstAsociados;
	}

	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public Integer getRegionId() {
		return regionId;
	}

	public void setRegionId(Integer regionId) {
		this.regionId = regionId;
	}

	public Integer getMarcaId() {
		return marcaId;
	}

	public void setMarcaId(Integer marcaId) {
		this.marcaId = marcaId;
	}

	public AutobusExterno getAutobus() {
		return autobus;
	}

	public void setAutobus(AutobusExterno autobus) {
		this.autobus = autobus;
	}
}
