package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.util.List;
import java.util.Vector;

import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.DetalleTramo;
import recaudacion.bean.entidades.dao.DetalleTramoDAO;
import recaudacion.interacion.InteraccionBD;

public class TxDetalleTramo extends InteraccionBD {

	private static final long serialVersionUID = 1L;
	
	private Integer opcion = null;
	private Integer idTramo = null;
	
	private List<DetalleTramo> lstDetalles = null;
	private Vector<Object> lstModificados = null;
	
	public static final int BUSCA_TODOS_DETALLE_TRAMO = 1;
	public static final int BUSCA_DETALLE_TRAMO_TRAMOID = 2;
	public static final int GUARDA_DETALLE_TRAMO = 3;
	public static final int ELIMINA_DETALLE_TRAMO = 4;

	
	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		switch (getOpcion().intValue()) {
		
			case BUSCA_TODOS_DETALLE_TRAMO:
				buscaTodosDetalleTramo();
				break;
			case BUSCA_DETALLE_TRAMO_TRAMOID:
				buscaDetallesTramoXTramo();
				break;
				
			case GUARDA_DETALLE_TRAMO:
				guardaDetallesTramo();
				break;
				
			case ELIMINA_DETALLE_TRAMO:
				eliminaDetallesTramo();
				break;
				
			default:
				break;
		}
		return this;
	}
	
	private void buscaTodosDetalleTramo(){ 
		try {
			DetalleTramoDAO dao = new DetalleTramoDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			setLstDetalles(dao.buscaTodosDetalleTramos());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	
	private void buscaDetallesTramoXTramo(){
		try {
			DetalleTramoDAO dao = new DetalleTramoDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			setLstDetalles(dao.buscaDetalleTramosXTramoXTramo(getIdTramo()));
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}
	
	private void guardaDetallesTramo() throws Exception{
		try {
			DetalleTramoDAO dao = new DetalleTramoDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.guardaDetalle(getLstModificados());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}
	
	private void eliminaDetallesTramo(){ 
		try {
			DetalleTramoDAO dao = new DetalleTramoDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.eliminaSecuenciaTramo(getIdTramo());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public List<DetalleTramo> getLstDetalles() {
		return lstDetalles;
	}

	public void setLstDetalles(List<DetalleTramo> lstDetalles) {
		this.lstDetalles = lstDetalles;
	}

	public Integer getIdTramo() {
		return idTramo;
	}

	public void setIdTramo(Integer idTramo) {
		this.idTramo = idTramo;
	}

	public Vector<Object> getLstModificados() {
		return lstModificados;
	}

	public void setLstModificados(Vector<Object> lstModificados) {
		this.lstModificados = lstModificados;
	}

}
