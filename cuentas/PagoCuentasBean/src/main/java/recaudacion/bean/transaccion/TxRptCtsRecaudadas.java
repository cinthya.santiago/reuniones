package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.Rol;
import recaudacion.bean.entidades.ccuentas.TarjetaCuenta;
import recaudacion.bean.entidades.dao.RolDAO;
import recaudacion.bean.entidades.dao.TarjetaCuentaDAO;
import recaudacion.interacion.InteraccionBD;

public class TxRptCtsRecaudadas extends InteraccionBD {

	private static final long serialVersionUID = 1L;

	private Integer opcion = null;

	public static final int RECUPERA_TOTALES_REPORTE = 1;

	private Date fechaInicio = null;
	private Date fechaFinal = null;
	private Integer idRecaudador = null;
	private Integer marcaId = null;
	private Integer marcaRolId = null;

	
	private List<TarjetaCuenta> lstCtasRecaudadas = null;
	
	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		switch (getOpcion().intValue()) {
		case RECUPERA_TOTALES_REPORTE:
			recuperaCuentasRecaudadas();
			break;

		default:
			break;
		}
		return this;
	}

	private Rol recuperaRol() {
		Rol rol = null;
		try {
			RolDAO dao = new RolDAO();
			dao.setDb(getDb());
			dao.setConexion(getConexion());
			rol = dao.recuperaRolPorFecha(getMarcaId(), getMarcaRolId(), getFechaInicio(), getFechaFinal());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
		return rol;
	}
	
	//NUEVO METODO DE CUENTAS RECAUDADAS
	private void recuperaCuentasRecaudadas() throws Exception{ 
		setLstCtasRecaudadas(new ArrayList<>());
		Rol rol = null;
		
		if(getMarcaRolId()!= null)
			rol = recuperaRol();
		
		TarjetaCuentaDAO dao = new TarjetaCuentaDAO();
		dao.setConexion(getConexion());
		dao.setDb(getDb());
		
		List<TarjetaCuenta> cuentas = dao.buscaCuentasRecaudadas(rol, getFechaInicio(), getFechaFinal(), getIdRecaudador());
		setLstCtasRecaudadas(cuentas);
		
	}

	

	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public Integer getIdRecaudador() {
		return idRecaudador;
	}

	public void setIdRecaudador(Integer idRecaudador) {
		this.idRecaudador = idRecaudador;
	}

	public Integer getMarcaId() {
		return marcaId;
	}

	public void setMarcaId(Integer marcaId) {
		this.marcaId = marcaId;
	}

	public Integer getMarcaRolId() {
		return marcaRolId;
	}

	public void setMarcaRolId(Integer marcaRolId) {
		this.marcaRolId = marcaRolId;
	}


	public List<TarjetaCuenta> getLstCtasRecaudadas() {
		return lstCtasRecaudadas;
	}

	public void setLstCtasRecaudadas(List<TarjetaCuenta> lstCtasRecaudadas) {
		this.lstCtasRecaudadas = lstCtasRecaudadas;
	}

}
