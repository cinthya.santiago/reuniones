package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.util.List;

import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.MarcaParada;
import recaudacion.bean.entidades.dao.MarcaParadaDAO;
import recaudacion.bean.entidades.dao.ParadaDAO;
import recaudacion.bean.entidades.general.Parada;
import recaudacion.interacion.InteraccionBD;

public class TxMttoParadas extends InteraccionBD{

	private static final long serialVersionUID = 1L;

	private Integer opcion = null;
	
	private List<Parada> lstParadas = null;
	
	private Parada parada = null;
	
	private MarcaParada marcaParada = null;
	
	private List<MarcaParada> lstMarcaParada = null;
	
	public static final int BUSCA_PARADAS = 1;
	public static final int GUARDA_PARADAS = 2;
	public static final int GUARDA_MARCA_PARADA = 3;
	
	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		switch (getOpcion().intValue()) {
		case BUSCA_PARADAS:
			buscaParadas();
			buscaMarcaParadas();
			break;
			
		case GUARDA_PARADAS:
			guardaParadas();
			break;
			
		case GUARDA_MARCA_PARADA:
			guardaMarcaParada();
			break;

		default:
			break;
		}
		return this;
	}
	
	
	
	private void buscaMarcaParadas() throws Exception {
		MarcaParadaDAO dao = new MarcaParadaDAO();
		dao.setConexion(getConexion());
		List<MarcaParada> mps = dao.buscaMarcaParadas();
		setLstMarcaParada(mps);
	}



	private void buscaParadas() {
		try {
			ParadaDAO dao = new ParadaDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.buscaParadas();
			setLstParadas(dao.getLstParadas());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}
	
	private void guardaParadas() throws Exception {
		try {
			ParadaDAO dao = new ParadaDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			parada = (Parada) dao.guarda(getParada());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}
	
	private void guardaMarcaParada() throws Exception{
		MarcaParadaDAO dao = new MarcaParadaDAO();
		dao.setConexion(getConexion());
		dao.setDb(getDb());
		dao.guarda(getMarcaParada());
	}
	
	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public List<Parada> getLstParadas() {
		return lstParadas;
	}

	public void setLstParadas(List<Parada> lstParadas) {
		this.lstParadas = lstParadas;
	}

	public Parada getParada() {
		return parada;
	}

	public void setParada(Parada parada) {
		this.parada = parada;
	}

	public MarcaParada getMarcaParada() {
		return marcaParada;
	}

	public void setMarcaParada(MarcaParada marcaParada) {
		this.marcaParada = marcaParada;
	}

	public List<MarcaParada> getLstMarcaParada() {
		return lstMarcaParada;
	}

	public void setLstMarcaParada(List<MarcaParada> lstMarcaParada) {
		this.lstMarcaParada = lstMarcaParada;
	}
}
