package recaudacion.bean.transaccion.catalogos;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.AutobusExterno;
import recaudacion.bean.entidades.dao.AutobusDAO;
import recaudacion.bean.entidades.dao.AutobusExternoDAO;
import recaudacion.bean.entidades.dao.EmpleadoDAO;
import recaudacion.bean.entidades.dao.ParadaDAO;
import recaudacion.bean.entidades.dao.ViaDAO;
import recaudacion.bean.entidades.general.Autobus;
import recaudacion.bean.entidades.general.Empleado;
import recaudacion.bean.entidades.general.Parada;
import recaudacion.bean.entidades.general.Via;
import recaudacion.interacion.InteraccionBD;

public class TxCatalogos extends InteraccionBD {
	private static final long serialVersionUID = 1L;

	private Integer opcion = null;
	private Integer marcaId = null;
	
	private String idConductor = null;
	private String idAutobus = null;
	private String idAutobusExt = null;
	private String idParadas = null;
	private String idVias = null;
	
	private Empleado empleado = null;
	private Autobus autobus = null;
	private AutobusExterno autobusExterno = null;
	

	private LinkedHashMap<Object, Object> hshConductores = null;
	private LinkedHashMap<Object, Object> hshAutobuses = null;
	private LinkedHashMap<Object, Object> hshAutobusesExt = null;
	private LinkedHashMap<Object, Object> hshParadas = null;
	private LinkedHashMap<Object, Object> hshVias = null;

	public static final int BUSCA_AUTOBUS_DESCRIPCION = 1;
	public static final int BUSCA_EMPLEADO_DESCRIPCION = 2;
	public static final int BUSCA_PARADAS_DESCRIPCION = 3;
	public static final int BUSCA_VIAS = 4;
	public static final int BUSCA_AUTOBUS_EXTERNO = 5;
	
	public static final int BUSCA_EMPLEADO_POR_CLAVE = 6;
	public static final int BUSCA_AUTOBUS_NUMECONOMICO = 7;
	public static final int BUSCA_AUTOBUEXT_NUMECONOMICO = 8;
	
	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		switch (getOpcion().intValue()) {
		case BUSCA_AUTOBUS_DESCRIPCION:
			cargaCatalogoAutobus();
			break;

		case BUSCA_EMPLEADO_DESCRIPCION:
			cargaCatalogoEmpleado();
			break;
		case BUSCA_PARADAS_DESCRIPCION:
			cargaCatalogoParadas();
			break;
		case BUSCA_VIAS:
			cargaCatalogovia();
			break;
		case BUSCA_AUTOBUS_EXTERNO:
			cargaAutobusExterno();
			break;
		case BUSCA_EMPLEADO_POR_CLAVE:
			buscaEmpleadoClave();
			break;
		default:
		case BUSCA_AUTOBUS_NUMECONOMICO:
			buscaAutobusNumEconomico();
			break;
		case BUSCA_AUTOBUEXT_NUMECONOMICO:
			buscaAutobusExtNumEconomico();
			break;
		}
		return this;
	}

	private void cargaCatalogoAutobus() {
		try {
			setHshAutobuses(new LinkedHashMap<>());
			AutobusDAO dao = new AutobusDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.setIds(getIdAutobus());
			dao.buscaAutobusesXId();
			armaHash(dao.getLstAutobus());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}

	private void cargaCatalogoEmpleado() {
		try {
			setHshConductores(new LinkedHashMap<>());
			EmpleadoDAO dao = new EmpleadoDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.setIds(getIdConductor());
			dao.buscaConductoresXId();
			armaHash(dao.getLstEmpleado());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}

	private void cargaCatalogoParadas() {
		try {
			setHshParadas(new LinkedHashMap<>());
			ParadaDAO dao = new ParadaDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.setIds(getIdParadas());
			dao.buscaParadasXId();
			armaHash(dao.getLstParadas());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}
	private void cargaCatalogovia(){
		try {
			setHshVias(new LinkedHashMap<>());
			ViaDAO dao = new ViaDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.setDescripciones(getIdVias());
			List<Via> vias = dao.buscaViasActivasDescripcion();
			armaHash(vias);
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	private void cargaAutobusExterno(){ 
		try {
			setHshAutobusesExt(new LinkedHashMap<>());
			AutobusExternoDAO dao = new AutobusExternoDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.setIds(getIdAutobusExt());
			dao.buscaAutobusesXId();
			armaHash(dao.getLstAutobusExt());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	private void buscaEmpleadoClave()
	{
		try {
			EmpleadoDAO dao = new EmpleadoDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.buscaConductorPorClave(getIdConductor(), getMarcaId());
			setEmpleado(dao.getEmpleado());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	private void buscaAutobusNumEconomico()
	{
		try {
			AutobusDAO dao = new AutobusDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.buscaAutobusesXClave(getIdAutobus(),getMarcaId());
			setAutobus(dao.getAutobus());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	private void buscaAutobusExtNumEconomico(){
		try {
			AutobusExternoDAO dao = new AutobusExternoDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.buscaBusExternoXNumEconomico(getIdAutobus(), getMarcaId());
			setAutobusExterno(dao.getAutobus());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	private void armaHash(List<?> lista) {
		Iterator<?> it = lista.iterator();
		while (it.hasNext()) {
			Object obj = (Object) it.next();
			if (obj instanceof Empleado) {
				Empleado empleado = (Empleado) obj;
				getHshConductores().put(empleado.getCveEmpleado(), empleado);
			}
			if (obj instanceof Autobus) {
				Autobus bus = (Autobus) obj;
				getHshAutobuses().put(bus.getNumeroEconomico(), bus);
			}
			if (obj instanceof AutobusExterno) {
				AutobusExterno bus = (AutobusExterno) obj;
				getHshAutobusesExt().put(bus.getNumeroEconomico(), bus);
			}
			if (obj instanceof Parada) {
				Parada parada = (Parada) obj;
				getHshParadas().put(parada.getCveParada(), parada);
			}
			if(obj instanceof Via){
				Via via = (Via)obj;
				getHshVias().put(via.getNombVia(), via);
			}
		}
	}

	public String getIdAutobus() {
		return idAutobus;
	}

	public void setIdAutobus(String idAutobus) {
		this.idAutobus = idAutobus;
	}

	public String getIdConductor() {
		return idConductor;
	}

	public void setIdConductor(String idConductor) {
		this.idConductor = idConductor;
	}

	public LinkedHashMap<Object, Object> getHshConductores() {
		return hshConductores;
	}

	public void setHshConductores(LinkedHashMap<Object, Object> hshConductores) {
		this.hshConductores = hshConductores;
	}

	public LinkedHashMap<Object, Object> getHshAutobuses() {
		return hshAutobuses;
	}

	public void setHshAutobuses(LinkedHashMap<Object, Object> hshAutobuses) {
		this.hshAutobuses = hshAutobuses;
	}

	public LinkedHashMap<Object, Object> getHshParadas() {
		return hshParadas;
	}

	public void setHshParadas(LinkedHashMap<Object, Object> hshParadas) {
		this.hshParadas = hshParadas;
	}

	public String getIdParadas() {
		return idParadas;
	}

	public void setIdParadas(String idParadas) {
		this.idParadas = idParadas;
	}

	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public String getIdVias() {
		return idVias;
	}

	public void setIdVias(String idVias) {
		this.idVias = idVias;
	}

	public LinkedHashMap<Object, Object> getHshVias() {
		return hshVias;
	}

	public void setHshVias(LinkedHashMap<Object, Object> hshVias) {
		this.hshVias = hshVias;
	}

	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	public Autobus getAutobus() {
		return autobus;
	}

	public void setAutobus(Autobus autobus) {
		this.autobus = autobus;
	}

	public AutobusExterno getAutobusExterno() {
		return autobusExterno;
	}

	public void setAutobusExterno(AutobusExterno autobusExterno) {
		this.autobusExterno = autobusExterno;
	}

	public Integer getMarcaId() {
		return marcaId;
	}

	public void setMarcaId(Integer marcaId) {
		this.marcaId = marcaId;
	}

	public String getIdAutobusExt() {
		return idAutobusExt;
	}

	public void setIdAutobusExt(String idAutobusExt) {
		this.idAutobusExt = idAutobusExt;
	}

	public LinkedHashMap<Object, Object> getHshAutobusesExt() {
		return hshAutobusesExt;
	}

	public void setHshAutobusesExt(LinkedHashMap<Object, Object> hshAutobusesExt) {
		this.hshAutobusesExt = hshAutobusesExt;
	}
}
