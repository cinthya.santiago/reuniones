package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.util.List;

import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.TramoMarca;
import recaudacion.bean.entidades.dao.TramosMacaDAO;
import recaudacion.interacion.InteraccionBD;

public class TxMttoTramos extends InteraccionBD {
	private static final long serialVersionUID = 1L;
	private Integer opcion = null;
	private Integer idTramo = null;

	private TramoMarca rol = null;

	public static final int BUSCA_TRAMOS_MARCA = 1;
	public static final int GUARDA_TRAMOS_MARCA = 2;

	private List<TramoMarca> lstTramoMarca = null;
	private Integer idMarca = null;

	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		switch (getOpcion().intValue()) {
		case BUSCA_TRAMOS_MARCA:
			buscaRolMarcaTramo();
			break;
		case GUARDA_TRAMOS_MARCA:
			guardaRolMarcaTramo();
			break;
		default:
			break;
		}
		return this;
	}

	private void buscaRolMarcaTramo() throws Exception {

		try {
			TramosMacaDAO dao = new TramosMacaDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			List<TramoMarca> tramosMarca = dao.buscaTramoMarca(getIdMarca());
			setLstTramoMarca(tramosMarca);
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}

	private void guardaRolMarcaTramo() throws Exception {
		try {
			TramosMacaDAO dao = new TramosMacaDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.setTramoRol(getRol());
			setIdTramo(dao.guardaTramoMarca());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}

	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public TramoMarca getRol() {
		return rol;
	}

	public void setRol(TramoMarca rol) {
		this.rol = rol;
	}

	public List<TramoMarca> getLstTramoMarca() {
		return lstTramoMarca;
	}

	public void setLstTramoMarca(List<TramoMarca> lstTramoMarca) {
		this.lstTramoMarca = lstTramoMarca;
	}

	public Integer getIdMarca() {
		return idMarca;
	}

	public void setIdMarca(Integer idMarca) {
		this.idMarca = idMarca;
	}

	public Integer getIdTramo() {
		return idTramo;
	}

	public void setIdTramo(Integer idTramo) {
		this.idTramo = idTramo;
	}

}
