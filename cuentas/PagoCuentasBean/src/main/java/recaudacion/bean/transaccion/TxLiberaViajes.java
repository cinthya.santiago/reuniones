package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.TarjetaCuenta;
import recaudacion.bean.entidades.ccuentas.TarjetaViaje;
import recaudacion.bean.entidades.dao.TarjetaCuentaDAO;
import recaudacion.bean.entidades.dao.TarjetaViajeDAO;
import recaudacion.bean.entidades.dao.ViajeDAO;
import recaudacion.interacion.InteraccionBD;

public class TxLiberaViajes extends InteraccionBD {

	private static final long serialVersionUID = 1L;

	private Integer opcion = null;
	public static final int ACTUALIZA_VIAJE = 1;
	public static final int VERIFICA_CUENTA_EXISTENTE = 2;
	public static final int GUARDA_TARJETA_CUENTA = 3;
	public static final int GUARDA_TARJETA_VIAJE = 4;
	public static final int ACTUALIZA_TARJETA_CUENTA = 5;

	private Integer idConductor = null;
	private Integer idTarjeta = null;
	private Date fechaEmision = null;

	private TarjetaCuenta tarjetaCuenta = null;
	private Vector<Object> lstViajes = null;
	private List<TarjetaViaje> lstTarjetaViaje = null;
	

	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		switch (getOpcion().intValue()) {
		case ACTUALIZA_VIAJE:
			liberaViajes();
			break;
		case VERIFICA_CUENTA_EXISTENTE:
			buscaTarjetaCuentaDia();
			break;

		case GUARDA_TARJETA_CUENTA:
			guardaTarjetaCuenta();
			break;
		case GUARDA_TARJETA_VIAJE:
			guardaTarjetaViaje();
			break;
		case ACTUALIZA_TARJETA_CUENTA:
			actualizaTarjeta();
			break;
		default:
			break;
		}
		return this;
	}

	private void liberaViajes() throws Exception {
			ViajeDAO dao = new ViajeDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.setViajes(getLstViajes());
			dao.guardaViajes();
	}

	private void buscaTarjetaCuentaDia() {
			TarjetaCuentaDAO dao = new TarjetaCuentaDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.consultaTarjetCuentaDia(getIdConductor(), getFechaEmision());
			setTarjetaCuenta(dao.getTarjetaCuenta());
	}

	private void guardaTarjetaCuenta() {
		try {
			TarjetaCuentaDAO dao = new TarjetaCuentaDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.setTarjetaCuenta(getTarjetaCuenta());
			dao.insertaTarjetaCuenta();
			setIdTarjeta(dao.getIdTarjeta());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	private void actualizaTarjeta() 
	{
		try {
			TarjetaCuentaDAO dao = new TarjetaCuentaDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.guarda(getTarjetaCuenta());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	private void guardaTarjetaViaje()
	{
		try {
			TarjetaViajeDAO dao = new TarjetaViajeDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.setLsttarjetaViajes(getLstTarjetaViaje());
			dao.guardaTarjetas();
			
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public Vector<Object> getLstViajes() {
		return lstViajes;
	}

	public void setLstViajes(Vector<Object> lstViajes) {
		this.lstViajes = lstViajes;
	}

	public Integer getIdConductor() {
		return idConductor;
	}

	public void setIdConductor(Integer idConductor) {
		this.idConductor = idConductor;
	}

	public Date getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public TarjetaCuenta getTarjetaCuenta() {
		return tarjetaCuenta;
	}

	public void setTarjetaCuenta(TarjetaCuenta tarjetaCuenta) {
		this.tarjetaCuenta = tarjetaCuenta;
	}

	public Integer getIdTarjeta() {
		return idTarjeta;
	}

	public void setIdTarjeta(Integer idTarjeta) {
		this.idTarjeta = idTarjeta;
	}

	public List<TarjetaViaje> getLstTarjetaViaje() {
		return lstTarjetaViaje;
	}

	public void setLstTarjetaViaje(List<TarjetaViaje> lstTarjetaViaje) {
		this.lstTarjetaViaje = lstTarjetaViaje;
	}
}
