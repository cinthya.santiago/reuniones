package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.Adeudo;
import recaudacion.bean.entidades.ccuentas.DepositoEnCamino;
import recaudacion.bean.entidades.ccuentas.DetalleAdeudo;
import recaudacion.bean.entidades.ccuentas.Recibo;
import recaudacion.bean.entidades.ccuentas.TarjetaCuenta;
import recaudacion.bean.entidades.ccuentas.TarjetaViaje;
import recaudacion.bean.entidades.ccuentas.Viajes;
import recaudacion.bean.entidades.dao.AdeudoDAO;
import recaudacion.bean.entidades.dao.DepositoEnCaminoDAO;
import recaudacion.bean.entidades.dao.DetalleAdeudoDAO;
import recaudacion.bean.entidades.dao.ReciboDAO;
import recaudacion.bean.entidades.dao.RegistroCombustibleDAO;
import recaudacion.bean.entidades.dao.TarjetaCuentaDAO;
import recaudacion.bean.entidades.dao.TarjetaViajeDAO;
import recaudacion.bean.entidades.dao.ViajeDAO;
import recaudacion.interacion.InteraccionBD;

public class TxRecaudaCuentas extends InteraccionBD{

	private static final long serialVersionUID = 1L;

	private Integer opcion = null;
	private Integer conductorId = null;
	private String ids = null;
	private Integer estatusViaje = null;
	private Integer tarjetaId = null;
	
	private List<TarjetaCuenta> lstCuentas = null;
	private List<Adeudo> lstAdeudos = null;
	private List<DetalleAdeudo> lstDetalleAdeudos = null;
	private List<DepositoEnCamino> lstDepositos = null;
	private List<?> lstViajesCta = null;
	private List<Viajes> lstViajes = null;
	
	private TarjetaCuenta tarjetaCuenta = null;
	private List<TarjetaViaje> lstTarjetaViajes = null;
	private Vector<Object> lstDepositosEnCamino = null;
	private Vector<Object> lstcombustible = null;
	
	private DetalleAdeudo detalleAdeudo = null;
	
	private Recibo recibo = null;
	
	public static final int OBTEN_CUENTAS_CONDUCTOR = 1;
	public static final int OBTEN_DEPOSITOS_CONDUCTOR = 2;
	public static final int OBTEN_ADEUDOS_CONDUCTOR = 3;
	
	public static final int ACTUALIZA_TARJETA_CUENTA = 4;
	public static final int ACTUALIZA_TARJETA_VIAJE = 5;
	public static final int ACTUALIZA_DEPOSITOS_EN_CAMINO = 6;
	public static final int ACTUALIZA_REGISTRO_COMBUSTIBLE = 7;
	public static final int ACTUALIZA_ESTATUS_VIAJES = 8;
	public static final int ACTUALIZA_DETALLE_ADEUDO = 9;
	public static final int REGISTRA_RECIBO = 10;
	public static final int RECUPERA_VIAJES = 11;
	
	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		switch (getOpcion().intValue()) {
		
		case OBTEN_CUENTAS_CONDUCTOR:
			obtenTarjetasCuentasPorConductor();
			
			break;
			
		case OBTEN_ADEUDOS_CONDUCTOR:
			obtenDetallesAdeudosPorConductor();
			break;

		case OBTEN_DEPOSITOS_CONDUCTOR:
			obtenDepositosPorConductor();
			break;
			
		case ACTUALIZA_TARJETA_CUENTA:
			actualizaTarjetaCuenta();
			break;
			
		case ACTUALIZA_TARJETA_VIAJE:
			actualizaTarjetaViajes();
			break;
			
		case ACTUALIZA_DEPOSITOS_EN_CAMINO:
			actualizaDepositos();
			break;
			
		case ACTUALIZA_REGISTRO_COMBUSTIBLE:
			actualizaCombustible();
			break;
			
		case ACTUALIZA_ESTATUS_VIAJES:
			actualizaEstatusViajes();
			break;
			
		case ACTUALIZA_DETALLE_ADEUDO:
			actualizaDetalleAdeudo();
			break;
			
		case REGISTRA_RECIBO:
			registraRecibo();
			break;
			
		case RECUPERA_VIAJES:
			recuperaViajesPorId();
			break;
		default:
			break;
		}
		return this;
	}

	private void obtenTarjetasCuentasPorConductor() {
		try {
			TarjetaCuentaDAO dao = new TarjetaCuentaDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.recuperaTarjetasPorConductor(getConductorId());
			setLstCuentas(dao.getLstTarjetas());
			
			String ids = recuperaIdsTarjetas(dao.getLstTarjetas());
			TarjetaViajeDAO daoT = new TarjetaViajeDAO();
			daoT.setConexion(getConexion());
			daoT.setDb(getDb());
			daoT.recuperaTarjetaViajesPorCta(ids);
			
			setLstViajesCta(daoT.getLsttarjetaViajes());
			
			
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	private String recuperaIdsTarjetas(List<TarjetaCuenta> lstTarjetas) {
		String ids= "";
		String coma = ",";
		Iterator<?> it = lstTarjetas.iterator();
		while(it.hasNext()){ 
			TarjetaCuenta tarjeta = (TarjetaCuenta)it.next();
			ids += "" + tarjeta.getIdTarjetaCuenta();
			if(it.hasNext()){
				ids += coma;
			}
		}
		return ids;
	}

	private void obtenDetallesAdeudosPorConductor(){
		try {
			AdeudoDAO dao = new AdeudoDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.consultaAdeudoConductor(getConductorId());
			setLstAdeudos(dao.getLstAdeudo());
			
			DetalleAdeudoDAO daoD = new DetalleAdeudoDAO();
			
			String ids = recuperaIds(dao.getLstAdeudo());
			if(!ids.equals("")){
				daoD.setConexion(getConexion());
				daoD.setDb(getDb());
				daoD.consultaDetalleAdeudo(ids);
				setLstDetalleAdeudos(daoD.getLstDetalles());
			}
			
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	private String recuperaIds(List<Adeudo> lstAdeudos) {
		String ids= "";
		String coma = ",";
		Iterator<?> it = lstAdeudos.iterator();
		while(it.hasNext()){ 
			Adeudo adeudo = (Adeudo)it.next();
			ids = "" + adeudo.getAdeudoId();
			if(it.hasNext()){
				ids += coma;
			}
		}
		return ids;
	}

	private void obtenDepositosPorConductor(){
		try {
			DepositoEnCaminoDAO dao = new DepositoEnCaminoDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.buscaDepositoTarjetaConducto(getConductorId(), getTarjetaId());
			setLstDepositos(dao.getLstDepositos());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	private void actualizaTarjetaCuenta(){
		try {
			TarjetaCuentaDAO dao = new TarjetaCuentaDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.guarda(getTarjetaCuenta());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	private void actualizaTarjetaViajes(){ 
		try {
			TarjetaViajeDAO dao = new TarjetaViajeDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.setLsttarjetaViajes(getLstTarjetaViajes());
			dao.guardaTarjetas();
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	private void actualizaDepositos(){
		try {
			DepositoEnCaminoDAO dao = new DepositoEnCaminoDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.guardaDepositoPorLista(getLstDepositosEnCamino());
			
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	
	private void actualizaCombustible() {
		try {
			RegistroCombustibleDAO dao = new RegistroCombustibleDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.guardaRegistroCombustiblePorLista(getLstcombustible());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	private void actualizaEstatusViajes()
	{
		try {
			ViajeDAO dao = new ViajeDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.setIdViajes(getIds());
			dao.setEstatusViaje(getEstatusViaje());
			dao.actualizaEstatusViajes();
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	
	private void actualizaDetalleAdeudo() {
		try {
			DetalleAdeudoDAO dao = new DetalleAdeudoDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.actualizaDetalleAdeudo(getDetalleAdeudo());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	private void registraRecibo(){ 
		try {
			ReciboDAO dao = new ReciboDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.guarda(getRecibo());
			
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	private void recuperaViajesPorId(){
		try {
			ViajeDAO dao = new ViajeDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.consultaViajesPorId(getIds());
			setLstViajes(dao.getLstViajes());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public List<TarjetaCuenta> getLstCuentas() {
		return lstCuentas;
	}

	public void setLstCuentas(List<TarjetaCuenta> lstCuentas) {
		this.lstCuentas = lstCuentas;
	}

	public List<DepositoEnCamino> getLstDepositos() {
		return lstDepositos;
	}

	public void setLstDepositos(List<DepositoEnCamino> lstDepositos) {
		this.lstDepositos = lstDepositos;
	}

	public Integer getConductorId() {
		return conductorId;
	}

	public void setConductorId(Integer conductorId) {
		this.conductorId = conductorId;
	}

	public List<DetalleAdeudo> getLstDetalleAdeudos() {
		return lstDetalleAdeudos;
	}

	public void setLstDetalleAdeudos(List<DetalleAdeudo> lstDetalleAdeudos) {
		this.lstDetalleAdeudos = lstDetalleAdeudos;
	}

	public TarjetaCuenta getTarjetaCuenta() {
		return tarjetaCuenta;
	}

	public void setTarjetaCuenta(TarjetaCuenta tarjetaCuenta) {
		this.tarjetaCuenta = tarjetaCuenta;
	}

	public List<Adeudo> getLstAdeudos() {
		return lstAdeudos;
	}

	public void setLstAdeudos(List<Adeudo> lstAdeudos) {
		this.lstAdeudos = lstAdeudos;
	}

	public List<?> getLstViajesCta() {
		return lstViajesCta;
	}

	public void setLstViajesCta(List<?> lstViajesCta) {
		this.lstViajesCta = lstViajesCta;
	}

	public List<TarjetaViaje> getLstTarjetaViajes() {
		return lstTarjetaViajes;
	}

	public void setLstTarjetaViajes(List<TarjetaViaje> lstTarjetaViajes) {
		this.lstTarjetaViajes = lstTarjetaViajes;
	}

	public Vector<Object> getLstDepositosEnCamino() {
		return lstDepositosEnCamino;
	}

	public void setLstDepositosEnCamino(Vector<Object> lstDepositosEnCamino) {
		this.lstDepositosEnCamino = lstDepositosEnCamino;
	}

	public Vector<Object> getLstcombustible() {
		return lstcombustible;
	}

	public void setLstcombustible(Vector<Object> lstcombustible) {
		this.lstcombustible = lstcombustible;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public Integer getEstatusViaje() {
		return estatusViaje;
	}

	public void setEstatusViaje(Integer estatusViaje) {
		this.estatusViaje = estatusViaje;
	}

	public DetalleAdeudo getDetalleAdeudo() {
		return detalleAdeudo;
	}

	public void setDetalleAdeudo(DetalleAdeudo detalleAdeudo) {
		this.detalleAdeudo = detalleAdeudo;
	}

	public Integer getTarjetaId() {
		return tarjetaId;
	}

	public void setTarjetaId(Integer tarjetaId) {
		this.tarjetaId = tarjetaId;
	}

	public Recibo getRecibo() {
		return recibo;
	}

	public void setRecibo(Recibo recibo) {
		this.recibo = recibo;
	}

	public List<Viajes> getLstViajes() {
		return lstViajes;
	}

	public void setLstViajes(List<Viajes> lstViajes) {
		this.lstViajes = lstViajes;
	}
}
