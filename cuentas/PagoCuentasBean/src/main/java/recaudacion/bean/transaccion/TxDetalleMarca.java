package recaudacion.bean.transaccion;

import java.io.Serializable;

import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.DetalleMarca;
import recaudacion.bean.entidades.dao.DetalleMarcaDAO;
import recaudacion.interacion.InteraccionBD;

public class TxDetalleMarca extends InteraccionBD
{
	private static final long serialVersionUID = 1L;
	
	private Integer opcion = null;
	private Integer marcaId = null;
	private Integer marcaRolId = null;
	
	private DetalleMarca detalleMarca = null;
	
	public static final int BUSCA_DETALLE_MARCA_PORID = 1;
	public static final int GUARDA_DETALLE_MARCA = 2;
	
	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		switch (getOpcion().intValue()) {
		case BUSCA_DETALLE_MARCA_PORID:
			buscaDetalleMarcaPorIdActivas();
			break;

		case GUARDA_DETALLE_MARCA:
			guardaDetalleMarca();
			break;
		default:
			break;
		}
		return this;
	}

	private void buscaDetalleMarcaPorIdActivas(){
		try {
			DetalleMarcaDAO dao = new DetalleMarcaDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			setDetalleMarca(dao.recuperaDetalleMarcaPorId(getMarcaId(), getMarcaRolId()));
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	private void guardaDetalleMarca(){ 
		try {
			DetalleMarcaDAO dao = new DetalleMarcaDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.guarda(getDetalleMarca());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public Integer getMarcaId() {
		return marcaId;
	}

	public void setMarcaId(Integer marcaId) {
		this.marcaId = marcaId;
	}

	public Integer getMarcaRolId() {
		return marcaRolId;
	}

	public void setMarcaRolId(Integer marcaRolId) {
		this.marcaRolId = marcaRolId;
	}

	public DetalleMarca getDetalleMarca() {
		return detalleMarca;
	}

	public void setDetalleMarca(DetalleMarca detalleMarca) {
		this.detalleMarca = detalleMarca;
	}

}
