package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.util.List;

import recaudacion.bean.entidades.dao.ViaDAO;
import recaudacion.bean.entidades.general.Via;
import recaudacion.interacion.InteraccionBD;

public class TxBuscaVias extends InteraccionBD {

	private static final long serialVersionUID = 1L;

	private Integer opcion = null;

	public static final int BUSCA_VIAS_ACTIVAS = 1;

	private List<Via> lstvias = null;

	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		switch (getOpcion().intValue()) {
		case BUSCA_VIAS_ACTIVAS:
			buscaViasActivas();
			break;

		default:
			break;
		}
		return this;
	}
	
	private void buscaViasActivas()
	{
		ViaDAO dao = new ViaDAO();
		dao.setDb(getDb());
		dao.setConexion(getConexion());
		setLstvias(dao.buscaViasActivas());
	}

	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public List<Via> getLstvias() {
		return lstvias;
	}

	public void setLstvias(List<Via> lstvias) {
		this.lstvias = lstvias;
	}

}
