package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.util.List;

import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.RegistroCombustible;
import recaudacion.bean.entidades.dao.RegistroCombustibleDAO;
import recaudacion.interacion.InteraccionBD;

public class TxRegistroCombustible extends InteraccionBD {

	private static final long serialVersionUID = 1L;

	public static final int BUSCA_REGISTRO_COMBUSTIBLE_CONDUCTOR = 1;
	public static final int GUARDA_REGISTRO_COMBUSTIBLE = 2;

	private Integer opcion = null;
	private Integer conductorId = null;
	
	private List<RegistroCombustible> lstCombustible = null;
	
	private RegistroCombustible registroCombustible = null;

	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		switch (getOpcion().intValue()) 
		{
			case BUSCA_REGISTRO_COMBUSTIBLE_CONDUCTOR:
				buscaCombustiblePorConductor();
				break;
			case GUARDA_REGISTRO_COMBUSTIBLE:
				guardaRegistroCombustible();
				break;

			default:
				break;
		}
		return this;
	}

	private void buscaCombustiblePorConductor()
	{
		try {
			RegistroCombustibleDAO dao = new RegistroCombustibleDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			setLstCombustible(dao.buscaCombustibleConductor(getConductorId()));
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	private void guardaRegistroCombustible() throws Exception {
		RegistroCombustibleDAO dao = new RegistroCombustibleDAO();
		dao.setConexion(getConexion());
		dao.setDb(getDb());
		RegistroCombustible regCombus = (RegistroCombustible) dao.guarda(getRegistroCombustible());
		setRegistroCombustible(regCombus);
	}

	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public RegistroCombustible getRegistroCombustible() {
		return registroCombustible;
	}

	public void setRegistroCombustible(RegistroCombustible registroCombustible) {
		this.registroCombustible = registroCombustible;
	}

	public Integer getConductorId() {
		return conductorId;
	}

	public void setConductorId(Integer conductorId) {
		this.conductorId = conductorId;
	}

	public List<RegistroCombustible> getLstCombustible() {
		return lstCombustible;
	}

	public void setLstCombustible(List<RegistroCombustible> lstCombustible) {
		this.lstCombustible = lstCombustible;
	}

}
