package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.TarjetaCuenta;
import recaudacion.bean.entidades.dao.ParadaDAO;
import recaudacion.bean.entidades.dao.TarjetaCuentaDAO;
import recaudacion.bean.entidades.general.Parada;
import recaudacion.interacion.InteraccionBD;

public class TxTarjetaCuentas extends InteraccionBD {
	private static final long serialVersionUID = 1L;

	private Integer opcion = null;
	private Integer idconductor = null;
	private Integer totalTarjetas = null;
	
	
	private List<TarjetaCuenta> lstTarjetas = null;
	private List<Parada> lstParadas = null;

	public static final int BUSCA_TARJETA_POR_CONDUCTOR = 1;
	public static final int BUSCA_PARADAS_ACTIVAS = 2;
	public static final int BUSCA_TOTAL_TARJETAS_CONDUCTOR = 3;

	@Override
	public Serializable ejecutaTransaccion() throws Exception {

		switch (getOpcion().intValue()) {
		case BUSCA_TARJETA_POR_CONDUCTOR:
			buscaTarjetaPorconductor();
			break;

		case BUSCA_PARADAS_ACTIVAS:
			buscaParadasActivas();
			break;

		case BUSCA_TOTAL_TARJETAS_CONDUCTOR:
			consultaTotalTarjetasConductor();
			break;
		default:
			break;
		}
		return this;
	}

	private void buscaTarjetaPorconductor() {
		try {
			TarjetaCuentaDAO dao = new TarjetaCuentaDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.recuperaTarjetasPorConductor(getIdconductor());
			setLstTarjetas(dao.getLstTarjetas());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	private void buscaParadasActivas() {
		try {
			setLstParadas(new ArrayList<>());
			ParadaDAO dao = new ParadaDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.buscaParadasActivas();
			setLstParadas(dao.getLstParadas());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}
	
	private void consultaTotalTarjetasConductor(){ 
		try {
			TarjetaCuentaDAO dao = new TarjetaCuentaDAO();
			setTotalTarjetas(dao.consultaTotalCuentasConductor(getIdconductor()));
			
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);;
			throw e;
		}
	}

	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public Integer getIdconductor() {
		return idconductor;
	}

	public void setIdconductor(Integer idconductor) {
		this.idconductor = idconductor;
	}

	public List<TarjetaCuenta> getLstTarjetas() {
		return lstTarjetas;
	}

	public void setLstTarjetas(List<TarjetaCuenta> lstTarjetas) {
		this.lstTarjetas = lstTarjetas;
	}

	public List<Parada> getLstParadas() {
		return lstParadas;
	}

	public void setLstParadas(List<Parada> lstParadas) {
		this.lstParadas = lstParadas;
	}

	public Integer getTotalTarjetas() {
		return totalTarjetas;
	}

	public void setTotalTarjetas(Integer totalTarjetas) {
		this.totalTarjetas = totalTarjetas;
	}
}
