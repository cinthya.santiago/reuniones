package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.stream.Collectors;

import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.DetalleRol;
import recaudacion.bean.entidades.ccuentas.Viajes;
import recaudacion.bean.entidades.dao.DetalleRolDAO;
import recaudacion.bean.entidades.dao.ViajeDAO;
import recaudacion.interacion.InteraccionBD;

public class TxConsultaViajes extends InteraccionBD {

	private static final long serialVersionUID = 1L;

	private Integer opcion = null;
	private Integer idMarca = null;
	private Integer idMarcaRol = null;
	private Integer idConductor = null;
	private Integer idRol = null;
	private Date fecha = null;
	
	private Date fechaInicial = null;
	private Date fechaFinal = null;

	private Vector<Object> viajes = null;
	private List<Viajes> lstViajes = null;
	private List<DetalleRol> lstDetalleRol = null;
	
	private Boolean existeOfertaGenerada = null;

	public static final int BUSCA_VIAJES = 1;
	public static final int ACTUALIZA_RECURSOS = 2;
	public static final int BUSCA_VIAJES_POR_CONDUCTOR = 3;
	public static final int BUSCA_VIAJES_POR_PERIODO_ROL = 4;
	public static final int ELIMINA_VIAJES = 5;
	public static final int CONSULTA_OFERTA_X_MARCA_ROL = 6;

	@Override
	public Serializable ejecutaTransaccion() throws Exception {

		switch (getOpcion().intValue()) {
		case BUSCA_VIAJES:
			buscaViajes();
			break;

		case ACTUALIZA_RECURSOS:
			actualizaRecursos();
			break;
		
		case BUSCA_VIAJES_POR_CONDUCTOR:
			buscaViajesPorConductor();
			break;
		
		case BUSCA_VIAJES_POR_PERIODO_ROL:
			buscaViajesPeriodoConductor();
			break;
		
		case ELIMINA_VIAJES:
			eliminaViajes();
			break;
		case CONSULTA_OFERTA_X_MARCA_ROL:
			consultaOfertaMarcarol();
			break;
		default:
			break;
		}
		return this;
	}

	private void consultaOfertaMarcarol() throws SQLException {
		ViajeDAO dao = new ViajeDAO();
		dao.setDb(getDb());
		dao.setConexion(getConexion());
		existeOfertaGenerada = dao.consultaOfertaDisponible(getIdMarcaRol(),getFechaInicial(),getFechaFinal());
	}

	private void eliminaViajes() {
		try {
			ViajeDAO dao = new ViajeDAO();
			dao.setDb(getDb());
			dao.setConexion(getConexion());
			dao.eliminaViajes(getIdMarcaRol(),getFechaInicial(),getFechaFinal());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	private void buscaViajes() {
		try {
			setViajes(new Vector<Object>());
			setLstDetalleRol(new ArrayList<DetalleRol>());
			
			ViajeDAO daoV = new ViajeDAO();
			daoV.setDb(getDb());
			daoV.setConexion(getConexion());
			List<Viajes> resultado = daoV.consultaViajesPorMarcaMarcaRol(getIdMarca(), getIdMarcaRol(), getFecha());
			setLstViajes(resultado);
			
			Set<Integer> roles = resultado.stream().map(v-> v.getRol().getRolId()).collect(Collectors.toSet());
			
			
			DetalleRolDAO dao = new DetalleRolDAO();
			dao.setDb(getDb());
			for(Integer rolId : roles) {
				getLstDetalleRol().addAll(dao.buscaDetalleRol(rolId));
			}
			
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	private void actualizaRecursos() {
		try {
			ViajeDAO dao = new ViajeDAO();
			dao.setDb(getDb());
			dao.setConexion(getConexion());
			dao.setViajes(getViajes());
			dao.actualizaRecursosViajes();
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	private void buscaViajesPorConductor() {
		try {
			ViajeDAO dao = new ViajeDAO();
			dao.setDb(getDb());
			dao.setConexion(getConexion());
			dao.buscaViajesPorConductor(getIdConductor(), getFecha());
			setLstViajes(dao.getLstViajes());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	private void buscaViajesPeriodoConductor() {
		try {
			ViajeDAO dao = new ViajeDAO();
			dao.setDb(getDb());
			dao.setConexion(getConexion());
			dao.buscaViajesPorRolPeriodo(getIdRol(), getFechaInicial(), getFechaFinal());
			setLstViajes(dao.getLstViajes());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public Vector<Object> getViajes() {
		return viajes;
	}

	public void setViajes(Vector<Object> viajes) {
		this.viajes = viajes;
	}

	public Integer getIdMarca() {
		return idMarca;
	}

	public void setIdMarca(Integer idMarca) {
		this.idMarca = idMarca;
	}

	public Integer getIdMarcaRol() {
		return idMarcaRol;
	}

	public void setIdMarcaRol(Integer idMarcaRol) {
		this.idMarcaRol = idMarcaRol;
	}

	public List<Viajes> getLstViajes() {
		return lstViajes;
	}

	public void setLstViajes(List<Viajes> lstViajes) {
		this.lstViajes = lstViajes;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Integer getIdConductor() {
		return idConductor;
	}

	public void setIdConductor(Integer idConductor) {
		this.idConductor = idConductor;
	}

	public Integer getIdRol() {
		return idRol;
	}

	public void setIdRol(Integer idRol) {
		this.idRol = idRol;
	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public List<DetalleRol> getLstDetalleRol() {
		return lstDetalleRol;
	}

	public void setLstDetalleRol(List<DetalleRol> lstDetalleRol) {
		this.lstDetalleRol = lstDetalleRol;
	}

	public Boolean getExisteOfertaGenerada() {
		return existeOfertaGenerada;
	}

	public void setExisteOfertaGenerada(Boolean existeOfertaGenerada) {
		this.existeOfertaGenerada = existeOfertaGenerada;
	}
}