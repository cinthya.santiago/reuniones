package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.util.Date;

import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.Rol;
import recaudacion.bean.entidades.ccuentas.Viajes;
import recaudacion.bean.entidades.dao.RolDAO;
import recaudacion.bean.entidades.dao.ViajeDAO;
import recaudacion.interacion.InteraccionBD;

public class TxViajesExtra extends InteraccionBD {

	private static final long serialVersionUID = 1L;

	private Integer opcion = null;
	
	private Integer idViaje = null;
	
	private Integer idMarca = null;
	private Integer idMarcaRol = null;
	private Date fecha = null;
	
	private Viajes  viaje = null;
	private Rol rol = null;

	public static final int GUARDA_VIAJE_EXTRA = 1;
	public static final int BUSCA_ROL = 2;

	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		switch (getOpcion().intValue()) {
		case GUARDA_VIAJE_EXTRA:
			guardaViajeExtra();
			break;
		case BUSCA_ROL:
			buscaRol();
			break;

		default:
			break;
		}
		return this;
	}

	private void guardaViajeExtra(){
		try {
			ViajeDAO dao = new ViajeDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.setViaje(getViaje());
			dao.actualizaViaje();
			setIdViaje(dao.getIdViaje());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	private void buscaRol()
	{
		try {
			RolDAO dao = new RolDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			setRol(dao.buscaRolId(getIdMarca(), getIdMarcaRol(), getFecha()));
		} catch (Exception e) {
		}
	}
	
	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public Viajes getViaje() {
		return viaje;
	}

	public void setViaje(Viajes viaje) {
		this.viaje = viaje;
	}

	public Integer getIdViaje() {
		return idViaje;
	}

	public void setIdViaje(Integer idViaje) {
		this.idViaje = idViaje;
	}

	public Integer getIdMarca() {
		return idMarca;
	}

	public void setIdMarca(Integer idMarca) {
		this.idMarca = idMarca;
	}

	public Integer getIdMarcaRol() {
		return idMarcaRol;
	}

	public void setIdMarcaRol(Integer idMarcaRol) {
		this.idMarcaRol = idMarcaRol;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

}
