package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import recaudacion.bean.entidades.ccuentas.RptAdicionales;
import recaudacion.bean.entidades.dao.DepositoEnCaminoDAO;
import recaudacion.bean.entidades.dao.RegistroCombustibleDAO;
import recaudacion.bean.entidades.dao.ViajeRutaDAO;
import recaudacion.interacion.InteraccionBD;

public class TxRptsAdicionales extends InteraccionBD {

	private static final long serialVersionUID = 1L;

	private Integer opcion = null;

	public static final int TOTALES_VENTA_COMBUSTIBLE = 1;
	public static final int TOTALES_DEPOSITOS_CAMINO = 2;
	public static final int TOTALES_VIAJES_RUTA = 3;

	private Date fechaInicio = null;
	private Date fechaFinal = null;
	private Integer usuarioId = null;

	private List<RptAdicionales> ltsCombustible = null;
	private List<RptAdicionales> lstDepositos = null;
	private List<RptAdicionales> lstViajesRuta = null;

	@Override
	public Serializable ejecutaTransaccion() throws Exception {

		switch (getOpcion().intValue()) {
		case TOTALES_VENTA_COMBUSTIBLE:
			recuperaTotalesVentaCombustible();
			break;

		case TOTALES_DEPOSITOS_CAMINO:
			recuperaTotalesDepositos();
			break;

		case TOTALES_VIAJES_RUTA:
			recuperaTotalesViajesRuta();
			break;

		default:
			break;
		}
		return this;
	}

	private void recuperaTotalesVentaCombustible() throws Exception {
		RegistroCombustibleDAO dao = new RegistroCombustibleDAO();
		dao.setConexion(getConexion());
		dao.setDb(getDb());
		List<RptAdicionales> ventaCombust = dao.buscaTotalesVentaCombustible(getFechaInicio(), getFechaFinal(), getUsuarioId());
		setLtsCombustible(ventaCombust);
	}

	private void recuperaTotalesDepositos() throws Exception {
		DepositoEnCaminoDAO dao = new DepositoEnCaminoDAO();
		dao.setConexion(getConexion());
		dao.setDb(getDb());
		List<RptAdicionales> totales = dao.buscaTotalesDepositos(getFechaInicio(), getFechaFinal(), getUsuarioId());
		setLstDepositos(totales);
	}

	

	private void recuperaTotalesViajesRuta() throws Exception {
		ViajeRutaDAO dao = new ViajeRutaDAO();
		dao.setConexion(getConexion());
		dao.setDb(getDb());
		
		List<RptAdicionales> totales = dao.buscaTotalesViajesRuta(getFechaInicio(), getFechaFinal(), getUsuarioId());
		setLstViajesRuta(totales);
	}


	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	public List<RptAdicionales> getLtsCombustible() {
		return ltsCombustible;
	}

	public void setLtsCombustible(List<RptAdicionales> ltsCombustible) {
		this.ltsCombustible = ltsCombustible;
	}

	public List<RptAdicionales> getLstDepositos() {
		return lstDepositos;
	}

	public void setLstDepositos(List<RptAdicionales> lstDepositos) {
		this.lstDepositos = lstDepositos;
	}

	public List<RptAdicionales> getLstViajesRuta() {
		return lstViajesRuta;
	}

	public void setLstViajesRuta(List<RptAdicionales> lstViajesRuta) {
		this.lstViajesRuta = lstViajesRuta;
	}

	
}
