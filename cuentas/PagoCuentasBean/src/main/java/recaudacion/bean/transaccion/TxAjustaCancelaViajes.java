package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.util.Vector;

import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.TarjetaAjusteCuenta;
import recaudacion.bean.entidades.ccuentas.TarjetaCuenta;
import recaudacion.bean.entidades.ccuentas.TarjetaViaje;
import recaudacion.bean.entidades.dao.TarjetaAjusteCuentaDAO;
import recaudacion.bean.entidades.dao.TarjetaCuentaDAO;
import recaudacion.bean.entidades.dao.TarjetaViajeDAO;
import recaudacion.interacion.InteraccionBD;

public class TxAjustaCancelaViajes extends InteraccionBD {

	private static final long serialVersionUID = 1L;
	
	private Integer opcion = null;
	private Integer idViaje = null;
	private Integer idCuenta = null;

	private TarjetaViaje tarjetaviaje = null;
	private TarjetaCuenta tarjetaCuenta = null;
	private TarjetaAjusteCuenta tarjetaAjusteCuenta = null;

	public static final int CONSULTA_TARJETA_VIAJE_XID = 1;
	public static final int CONSULTA_TARJETA_CUENTA_XID = 2;
	public static final int ACTUALIZA_TARJETA_VIAJE = 3;
	public static final int ACTUALIZA_TARJETA_CUENTA = 4;
	public static final int CREA_TARJETA_AJUSTA_CUENTA = 5;

	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		switch (getOpcion().intValue()) {
		case CONSULTA_TARJETA_VIAJE_XID:
			consultaTarjetaViajeXId();
			break;
		case CONSULTA_TARJETA_CUENTA_XID:
			consultaTarjetaCuentaXIdViaje();
			break;
		case ACTUALIZA_TARJETA_VIAJE:
			actualizaTarjetaViaje();
			break;
		case ACTUALIZA_TARJETA_CUENTA:
			actualizaTarjetaCuenta();
			break;
		case CREA_TARJETA_AJUSTA_CUENTA:
			guardaTrajetaAjusteCuenta();
			break;
		default:
			break;
		}
		return this;
	}

	private void consultaTarjetaViajeXId() {
		try {
			TarjetaViajeDAO dao = new TarjetaViajeDAO();
			dao.setDb(getDb());
			dao.setConexion(getConexion());
			dao.recuperaTarjetaViajeXId(getIdViaje());
			setTarjetaviaje(dao.getTarjetaviaje());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	private void consultaTarjetaCuentaXIdViaje() {
		try {
			TarjetaCuentaDAO dao = new TarjetaCuentaDAO();
			dao.setDb(getDb());
			dao.setConexion(getConexion());
			dao.recuperaTarjetaCuentaXIdViaje(getIdCuenta());
			setTarjetaCuenta(dao.getTarjetaCuenta());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	private void actualizaTarjetaViaje() {
		try {
			Vector<TarjetaViaje> vTarjeta = new Vector<>();
			vTarjeta.add(getTarjetaviaje());
			
			TarjetaViajeDAO dao = new TarjetaViajeDAO();
			dao.setDb(getDb());
			dao.setConexion(getConexion());
			dao.setLsttarjetaViajes(vTarjeta);
			dao.guardaTarjetas();
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	private void actualizaTarjetaCuenta() {
		try {
			TarjetaCuentaDAO dao = new TarjetaCuentaDAO();
			dao.setDb(getDb());
			dao.setConexion(getConexion());
			dao.guarda(getTarjetaCuenta());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	private void guardaTrajetaAjusteCuenta() {
		try {
			TarjetaAjusteCuentaDAO dao = new TarjetaAjusteCuentaDAO();
			dao.setDb(getDb());
			dao.setConexion(getConexion());
			dao.guarda(getTarjetaAjusteCuenta());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public Integer getIdViaje() {
		return idViaje;
	}

	public void setIdViaje(Integer idViaje) {
		this.idViaje = idViaje;
	}

	public Integer getIdCuenta() {
		return idCuenta;
	}

	public void setIdCuenta(Integer idCuenta) {
		this.idCuenta = idCuenta;
	}

	public TarjetaViaje getTarjetaviaje() {
		return tarjetaviaje;
	}

	public void setTarjetaviaje(TarjetaViaje tarjetaviaje) {
		this.tarjetaviaje = tarjetaviaje;
	}

	public TarjetaCuenta getTarjetaCuenta() {
		return tarjetaCuenta;
	}

	public void setTarjetaCuenta(TarjetaCuenta tarjetaCuenta) {
		this.tarjetaCuenta = tarjetaCuenta;
	}

	public TarjetaAjusteCuenta getTarjetaAjusteCuenta() {
		return tarjetaAjusteCuenta;
	}

	public void setTarjetaAjusteCuenta(TarjetaAjusteCuenta tarjetaAjusteCuenta) {
		this.tarjetaAjusteCuenta = tarjetaAjusteCuenta;
	}
}
