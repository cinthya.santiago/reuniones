package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.util.List;

import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.dao.TipoAutobusDAO;
import recaudacion.bean.entidades.general.TipoAutobus;
import recaudacion.interacion.InteraccionBD;

public class TxBuscaTipoAutobus extends InteraccionBD
{

	private static final long serialVersionUID = 1L;

	private Integer opcion =  null;
	
	private List<TipoAutobus> lstTipoAutobus = null;
	
	public static final int BUSCA_TIPO_AUTOBUS = 1;
	
	@Override
	public Serializable ejecutaTransaccion() throws Exception
	{
		switch (getOpcion().intValue()) {
		case BUSCA_TIPO_AUTOBUS:
			buscaTipoAutobus();
			break;

		default:
			break;
		}
		return this;
	}

	private void buscaTipoAutobus()
	{
		try {
			TipoAutobusDAO dao = new TipoAutobusDAO();
			dao.setDb(getDb());
			dao.setConexion(getConexion());
			setLstTipoAutobus(dao.buscaTipoAutobus());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public List<TipoAutobus> getLstTipoAutobus() {
		return lstTipoAutobus;
	}

	public void setLstTipoAutobus(List<TipoAutobus> lstTipoAutobus) {
		this.lstTipoAutobus = lstTipoAutobus;
	}
}
