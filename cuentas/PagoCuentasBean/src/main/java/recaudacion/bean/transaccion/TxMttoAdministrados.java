package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.util.List;

import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.Asociados;
import recaudacion.bean.entidades.dao.AsociadosDAO;
import recaudacion.interacion.InteraccionBD;

public class TxMttoAdministrados extends InteraccionBD {
	private static final long serialVersionUID = 1L;

	private Integer opcion = null;
	private String descAdmin = null;
	private List<Asociados> lstAdministrados = null;
	private Asociados administrado = null;

	public static final int BUSCA_TODOS_ADMINISTRADOS = 1;
	public static final int BUSCA_ADMINISTRADOS = 2;
	public static final int GUARDA_ASOCIADOS = 3;

	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		switch (getOpcion().intValue()) 
		{
			case BUSCA_TODOS_ADMINISTRADOS:
				buscaTodosAdministrados();
				break;
				
			case BUSCA_ADMINISTRADOS:
				buscaAdministrados();
				break;
				
			case GUARDA_ASOCIADOS:
				guardaAsociados();
				break;

			default:
			break;
		}
		return this;
	}

	private void buscaTodosAdministrados()
	{ 
		try {
			AsociadosDAO dao = new AsociadosDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			setLstAdministrados(dao.buscaTodosAsociados());
			
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	private void buscaAdministrados()
	{
		try {
			AsociadosDAO dao = new AsociadosDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.setDescripcion(getDescAdmin());
			setLstAdministrados(dao.buscaAsociadosActivos());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	private void guardaAsociados(){
		try {
			AsociadosDAO dao = new AsociadosDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.guardaAsociados(getAdministrado());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public String getDescAdmin() {
		return descAdmin;
	}

	public void setDescAdmin(String descAdmin) {
		this.descAdmin = descAdmin;
	}

	public List<Asociados> getLstAdministrados() {
		return lstAdministrados;
	}

	public void setLstAdministrados(List<Asociados> lstAdministrados) {
		this.lstAdministrados = lstAdministrados;
	}

	public Asociados getAdministrado() {
		return administrado;
	}

	public void setAdministrado(Asociados administrado) {
		this.administrado = administrado;
	}
}
