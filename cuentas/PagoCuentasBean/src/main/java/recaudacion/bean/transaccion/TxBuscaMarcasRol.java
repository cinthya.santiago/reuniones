package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.util.List;

import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.MarcaRol;
import recaudacion.bean.entidades.dao.MarcaRolDAO;
import recaudacion.interacion.InteraccionBD;

public class TxBuscaMarcasRol extends InteraccionBD 
{
	private static final long serialVersionUID = 1L;

	private Integer opcion = null;
	
	private String descripcion = null;
	
	private List<MarcaRol> lstMarcaRol = null;
	private Integer idMarca = null;
	private MarcaRol marcarol = null;
	
	public static final int BUSCA_MARCAS_ROL= 1;
	public static final int BUSCA_MARCAROL_DESCRIPCION = 2;
	public static final int BUSCA_MARCAROL_POR_MARCA = 3;
	public static final int RECUPERA_MARCAROL_DESCRIPCION = 4;
	
	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		switch (getOpcion().intValue()) {
		case BUSCA_MARCAS_ROL:
			buscaMarcasRol();
			break;

		case BUSCA_MARCAROL_DESCRIPCION:
			buscaMarcaRolDescripcion();
			break;
			
		case BUSCA_MARCAROL_POR_MARCA:
			buscaMarcaRolPorMarca();
			break;
			
		case RECUPERA_MARCAROL_DESCRIPCION:
			recuoeraMarcaRolPorDescripcion();
			break;
			
		default:
			break;
		}
		return this;
	}
	
	private void buscaMarcasRol()
	{
		try {
			MarcaRolDAO dao = new MarcaRolDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			setLstMarcaRol(dao.buscaMarcaRol());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	private void buscaMarcaRolDescripcion()
	{
		try {
			MarcaRolDAO dao = new MarcaRolDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.setDescripciones(getDescripcion());
			setLstMarcaRol(dao.buscaMarcasDescripcion());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	private void buscaMarcaRolPorMarca()
	{
		try {
			MarcaRolDAO dao = new MarcaRolDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			List<MarcaRol> marcasRoles = dao.buscaMarcaRolPorMarca(getIdMarca());
			setLstMarcaRol(marcasRoles);
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	private void recuoeraMarcaRolPorDescripcion(){
		try {
			MarcaRolDAO dao = new MarcaRolDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			setMarcarol(dao.recuperaMarcaRolPorDescripcion(getDescripcion()));
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	public Integer getOpcion() {
		return opcion;
	}
	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public List<MarcaRol> getLstMarcaRol() {
		return lstMarcaRol;
	}

	public void setLstMarcaRol(List<MarcaRol> lstMarcaRol) {
		this.lstMarcaRol = lstMarcaRol;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getIdMarca() {
		return idMarca;
	}

	public void setIdMarca(Integer idMarca) {
		this.idMarca = idMarca;
	}

	public MarcaRol getMarcarol() {
		return marcarol;
	}

	public void setMarcarol(MarcaRol marcarol) {
		this.marcarol = marcarol;
	}
}
