package recaudacion.bean.transaccion;

import java.io.Serializable;

import recaudacion.bean.entidades.dao.ParadaDAO;
import recaudacion.bean.entidades.general.Parada;
import recaudacion.interacion.InteraccionBD;

public class TxBuscaParada extends InteraccionBD{

	private static final long serialVersionUID = 1L;

	private Integer opcion = null;
	
	private String cveParada = null;
	
	private Parada parada = null;
	
	public static final int BUSCA_PARADA_XCVE = 1;
	
	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		switch (getOpcion().intValue()) {
		case BUSCA_PARADA_XCVE:
			buscaParadaXCve();

		default:
			break;
		}
		return this;
	}
	
	private void buscaParadaXCve() throws Exception{
		ParadaDAO dao = new ParadaDAO();
		dao.setDb(getDb());
		dao.setConexion(getConexion());
		parada = dao.buscaParadaXCve(getCveParada());
	}
	

	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public Parada getParada() {
		return parada;
	}

	public void setParada(Parada parada) {
		this.parada = parada;
	}

	public String getCveParada() {
		return cveParada;
	}

	public void setCveParada(String cveParada) {
		this.cveParada = cveParada;
	}

}
