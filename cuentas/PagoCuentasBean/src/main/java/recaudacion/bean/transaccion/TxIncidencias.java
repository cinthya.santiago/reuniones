package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import recaudacion.bean.entidades.ccuentas.RegistroIncidencias;
import recaudacion.bean.entidades.dao.IncidenciasDAO;
import recaudacion.interacion.InteraccionBD;

public class TxIncidencias extends InteraccionBD {
	private static final long serialVersionUID = 1L;

	private Integer opcion = null;
	private Integer idMarcaRol = null;
	private Integer idConductor = null;
	private Date fecha = null;
	private String idsConductores = null;

	private List<RegistroIncidencias> lstIncidencias = null;
	private RegistroIncidencias incidencia = null;

	public static final int BUSCA_INCIDENCIAS = 1;
	public static final int GUARDA_INCIDENCIAS = 2;
	public static final int BUSCA_INCIDENCIA_X_CONDUCTOR = 3;
	public static final int BUSCA_INCIDENCIAS_GRUPO_CONDUCTOR = 4;

	@Override
	public Serializable ejecutaTransaccion() throws Exception {

		switch (getOpcion().intValue()) {
		case BUSCA_INCIDENCIAS:
			buscaIncidenciasPorMarca();
			break;

		case GUARDA_INCIDENCIAS:
			guardaIncidencia();
			break;

		case BUSCA_INCIDENCIA_X_CONDUCTOR:
			buscaIncidenciaPorConductor();
			break;

		case BUSCA_INCIDENCIAS_GRUPO_CONDUCTOR:
			buscaIncidenciasGrupoConductor();
			break;
		default:
			break;
		}
		return this;
	}

	private void buscaIncidenciasPorMarca() {
		IncidenciasDAO dao = new IncidenciasDAO();
		dao.setConexion(getConexion());
		dao.setDb(getDb());
		dao.consultaIncienciasPorMarca(getIdMarcaRol(), getFecha());
		setLstIncidencias(dao.getLstIncidencias());
	}

	private void guardaIncidencia() throws Exception {
		IncidenciasDAO dao = new IncidenciasDAO();
		dao.setConexion(getConexion());
		dao.setDb(getDb());
		dao.guarda(getIncidencia());
	}

	private void buscaIncidenciaPorConductor() {
		IncidenciasDAO dao = new IncidenciasDAO();
		dao.setConexion(getConexion());
		dao.setDb(getDb());
		dao.buscaIncidenciaConductor(getIdConductor());
		setLstIncidencias(dao.getLstIncidencias());

	}

	private void buscaIncidenciasGrupoConductor() throws Exception{ 
		IncidenciasDAO dao = new IncidenciasDAO();
		dao.setConexion(getConexion());
		dao.setDb(getDb());
		setLstIncidencias(dao.consultaIncidenciasGrupoConductor(getIdsConductores()));
		System.out.println();
	}

	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public Integer getIdMarcaRol() {
		return idMarcaRol;
	}

	public void setIdMarcaRol(Integer idMarcaRol) {
		this.idMarcaRol = idMarcaRol;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public List<RegistroIncidencias> getLstIncidencias() {
		return lstIncidencias;
	}

	public void setLstIncidencias(List<RegistroIncidencias> lstIncidencias) {
		this.lstIncidencias = lstIncidencias;
	}

	public RegistroIncidencias getIncidencia() {
		return incidencia;
	}

	public void setIncidencia(RegistroIncidencias incidencia) {
		this.incidencia = incidencia;
	}

	public Integer getIdConductor() {
		return idConductor;
	}

	public void setIdConductor(Integer idConductor) {
		this.idConductor = idConductor;
	}

	public String getIdsConductores() {
		return idsConductores;
	}

	public void setIdsConductores(String idsConductores) {
		this.idsConductores = idsConductores;
	}
}
