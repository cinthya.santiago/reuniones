package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.util.List;

import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.Adeudo;
import recaudacion.bean.entidades.ccuentas.DetalleAdeudo;
import recaudacion.bean.entidades.dao.AdeudoDAO;
import recaudacion.bean.entidades.dao.DetalleAdeudoDAO;
import recaudacion.interacion.InteraccionBD;

public class TxAportacionAdeudo extends InteraccionBD {

	private static final long serialVersionUID = 1L;

	private Integer opcion = null;

	public static final int BUSCA_ADEUDOS_CONDUCTOR = 1;
	public static final int BUSCA_DETALLE_ADEUDO_CONDUCTOR = 2;
	public static final int GUARDA_ADEUDO_CONDUCTOR = 3;
	public static final int GUARDA_DETALLE_CONDUCTOR = 4;

	private Integer idConductor = null;
	private String idAdeudos = null;
	
	private Adeudo Adeudo = null;
	private List<Adeudo> lstAdeudos = null;
	private List<DetalleAdeudo> lstDetalle = null;
	
	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		switch (getOpcion().intValue()) {
			case BUSCA_ADEUDOS_CONDUCTOR:
				buscaAdeudosConductor();
				break;
			case BUSCA_DETALLE_ADEUDO_CONDUCTOR:
				buscaDetalleAdeudoConductor();
				break;
			case GUARDA_ADEUDO_CONDUCTOR:
				guardaAdeudoConductor();
				break;
			case GUARDA_DETALLE_CONDUCTOR:
				guardaDetalleConductor();
				break;
			default:
				break;
		}
		return this;
	}
	
	private void buscaAdeudosConductor(){
		try {
			AdeudoDAO dao = new AdeudoDAO();
			dao.setDb(getDb());
			dao.setConexion(getConexion());
			
			dao.consultaAdeudoConductor(getIdConductor());
			setLstAdeudos(dao.getLstAdeudo());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	private void buscaDetalleAdeudoConductor(){
		try {
			DetalleAdeudoDAO dao = new DetalleAdeudoDAO();
			dao.setDb(getDb());
			dao.setConexion(getConexion());
			dao.consultaDetalleAdeudo(getIdAdeudos());
			setLstDetalle(dao.getLstDetalles());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	private void guardaAdeudoConductor(){
		try {
			AdeudoDAO dao = new AdeudoDAO();
			dao.setDb(getDb());
			dao.setConexion(getConexion());
			setAdeudo(dao.guardaAdeudo(getAdeudo()));
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	private void guardaDetalleConductor(){
		try {
			DetalleAdeudoDAO dao = new DetalleAdeudoDAO();
			dao.setDb(getDb());
			dao.setConexion(getConexion());
			dao.guardaLstDetalleAdeudo( getLstDetalle());
			
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public Adeudo getAdeudo() {
		return Adeudo;
	}

	public void setAdeudo(Adeudo adeudo) {
		Adeudo = adeudo;
	}

	public List<DetalleAdeudo> getLstDetalle() {
		return lstDetalle;
	}

	public void setLstDetalle(List<DetalleAdeudo> lstDetalle) {
		this.lstDetalle = lstDetalle;
	}

	public Integer getIdConductor() {
		return idConductor;
	}

	public void setIdConductor(Integer idConductor) {
		this.idConductor = idConductor;
	}

	public List<Adeudo> getLstAdeudos() {
		return lstAdeudos;
	}

	public void setLstAdeudos(List<Adeudo> lstAdeudos) {
		this.lstAdeudos = lstAdeudos;
	}

	public String getIdAdeudos() {
		return idAdeudos;
	}

	public void setIdAdeudos(String idAdeudos) {
		this.idAdeudos = idAdeudos;
	}

}
