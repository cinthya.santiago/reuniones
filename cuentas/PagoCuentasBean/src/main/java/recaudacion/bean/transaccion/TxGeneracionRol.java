package recaudacion.bean.transaccion;

import java.io.Serializable;
import java.util.List;
import java.util.Vector;

import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.DetalleRol;
import recaudacion.bean.entidades.ccuentas.Rol;
import recaudacion.bean.entidades.dao.DetalleRolDAO;
import recaudacion.bean.entidades.dao.RolDAO;
import recaudacion.bean.entidades.dao.ViajeDAO;
import recaudacion.interacion.InteraccionBD;

public class TxGeneracionRol extends InteraccionBD
{
	private static final long serialVersionUID = 1L;

	private Integer opcion = null;
	private Integer rolId = null;
	
	private Rol rol =  null;
	
	private List<DetalleRol> detalles = null;
	private Vector<Object> viajes = null;

	public static final int GUARDA_GENERACION_ROL = 1;
	public static final int GUARDA_DETALLE_ROL = 2;
	public static final int GUARDA_GENERACION_VIAJES = 3;
	
	
	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		switch (getOpcion().intValue()) {
		case GUARDA_GENERACION_ROL:
			guardaRol();
			break;

		case GUARDA_DETALLE_ROL:
			guardaDetalleRol();
			break;
			
		case GUARDA_GENERACION_VIAJES:
			guardaGeneracionViajes();
			break;
		default:
			break;
		}
		return this;
	}

	private void guardaGeneracionViajes() 
	{
		try {
			ViajeDAO dao = new ViajeDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.setViajes(getViajes());
			dao.guardaViajes();
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	private void guardaRol() {
		try {
			RolDAO dao = new RolDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.setRol(getRol());
			dao.guardaRol();
			setRolId(dao.getRolId());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}

	private void guardaDetalleRol(){
		try {
			DetalleRolDAO dao = new DetalleRolDAO();
			dao.setConexion(getConexion());
			dao.setDb(getDb());
			dao.setLstDetalles(getDetalles());
			dao.guardaRol();
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	public Integer getOpcion() {
		return opcion;
	}

	public void setOpcion(Integer opcion) {
		this.opcion = opcion;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public Integer getRolId() {
		return rolId;
	}

	public void setRolId(Integer rolId) {
		this.rolId = rolId;
	}

	public List<DetalleRol> getDetalles() {
		return detalles;
	}

	public void setDetalles(List<DetalleRol> detalles) {
		this.detalles = detalles;
	}

	public Vector<Object> getViajes() {
		return viajes;
	}

	public void setViajes(Vector<Object> viajes) {
		this.viajes = viajes;
	}
	
}
