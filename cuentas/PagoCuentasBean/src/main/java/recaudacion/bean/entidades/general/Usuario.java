package recaudacion.bean.entidades.general;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="USUARIO",schema="GENERAL")
public class Usuario implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="USUARIO_ID",insertable=false,updatable= false)
	private Integer idUsuario;
	
	@Column(name="CVEUSUARIO")
	private String cveUsuario;
	
	@Column(name="NOMBRE")
	private String nombre;
	
	@Column(name="PATERNO")
	private String paterno;
	
	@Column(name="MATERNO")
	private String materno;
	
	@ManyToOne
	@JoinColumn(name="EMPLEADO_ID")
	private Empleado empleado;
	
	public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getCveUsuario() {
		return cveUsuario;
	}
	public void setCveUsuario(String cveUsuario) {
		this.cveUsuario = cveUsuario;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPaterno() {
		return paterno;
	}
	public void setPaterno(String paterno) {
		this.paterno = paterno;
	}
	public String getMaterno() {
		return materno;
	}
	public void setMaterno(String materno) {
		this.materno = materno;
	}
	public Empleado getEmpleado() {
		return empleado;
	}
	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}
}
