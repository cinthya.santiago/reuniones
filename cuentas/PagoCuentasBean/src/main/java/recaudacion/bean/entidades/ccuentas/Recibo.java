package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import recaudacion.bean.entidades.general.Marca;


@Entity
@Table(name="RECIBO", schema="CCUENTAS")
public class Recibo implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator ="RECIBOID")
	@SequenceGenerator(name="RECIBOID", sequenceName="CCUENTAS.RECIBO_SEQ", allocationSize = 1)
	@Column(name = "RECIBO_ID")
	private Integer reciboId ;
	
	@Column(name="USUARIO_ID")
	private Integer usuarioId;
	
	@ManyToOne
	@JoinColumn(name="TARJETA_CUENTA_ID")
	private TarjetaCuenta tarjetaCuenta ;
	
	@Column(name="IMPORTE_RECAUDADO")
	private Double importeRecaudado;
	
	@Column(name="ROL_ID")
	private Integer rolId;
	
	@ManyToOne
	@JoinColumn(name="MARCA_ID")
	private Marca marca;
	
	@Column(name="FECHA_RECAUDA")
	private Date fechaRecauda;
	
	@Column(name="ESTATUS")
	private Integer estatus;

	public Integer getReciboId() {
		return reciboId;
	}

	public void setReciboId(Integer reciboId) {
		this.reciboId = reciboId;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	public TarjetaCuenta getTarjetaCuenta() {
		return tarjetaCuenta;
	}

	public void setTarjetaCuenta(TarjetaCuenta tarjetaCuenta) {
		this.tarjetaCuenta = tarjetaCuenta;
	}

	public Double getImporteRecaudado() {
		return importeRecaudado;
	}

	public void setImporteRecaudado(Double importeRecaudado) {
		this.importeRecaudado = importeRecaudado;
	}

	public Integer getRolId() {
		return rolId;
	}

	public void setRolId(Integer rolId) {
		this.rolId = rolId;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public Date getFechaRecauda() {
		return fechaRecauda;
	}

	public void setFechaRecauda(Date fechaRecauda) {
		this.fechaRecauda = fechaRecauda;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}
	
}
