package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import recaudacion.bean.entidades.general.Marca;
import recaudacion.bean.entidades.general.Parada;
@Entity
@Table(name="MARCA_PARADA", schema="CCUENTAS")
public class MarcaParada implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@EmbeddedId
	@ManyToOne
	@JoinColumn(name="MARCA_ID")
	private Marca marca;
	
	@ManyToOne
	@JoinColumn(name="PARADA_ID")
	private Parada parada = null;

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public Parada getParada() {
		return parada;
	}

	public void setParada(Parada parada) {
		this.parada = parada;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((marca == null) ? 0 : marca.hashCode());
		result = prime * result + ((parada == null) ? 0 : parada.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MarcaParada other = (MarcaParada) obj;
		if (marca == null) {
			if (other.marca != null)
				return false;
		} else if (!marca.equals(other.marca))
			return false;
		if (parada == null) {
			if (other.parada != null)
				return false;
		} else if (!parada.equals(other.parada))
			return false;
		return true;
	}

	
	
}
