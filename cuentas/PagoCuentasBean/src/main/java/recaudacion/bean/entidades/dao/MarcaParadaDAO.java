package recaudacion.bean.entidades.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.MarcaParada;
import recaudacion.bean.entidades.general.Marca;
import recaudacion.bean.entidades.general.Parada;
import recaudacion.bean.entidades.general.Usuario;

public class MarcaParadaDAO extends DAO{
	
	
	public List<MarcaParada>  buscaTodasParadasMarca(){
		try {
			String hql = getQueryBuscaMarcaParada();
			Query<MarcaParada> query = getDb().createQuery(hql,MarcaParada.class);
			return query.list();
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}

	private String getQueryBuscaMarcaParada() {
		return "selec mp from MarcaParada mp";
	}
	
	
	public List<MarcaParada> buscaMarcaParadas() throws Exception{ 
		
		List<MarcaParada> marcaParadas = new ArrayList<MarcaParada>();
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getConexion().prepareStatement(queryBuscaMarcaParadas());
			rs = ps.executeQuery();
			
			while(rs.next()){ 
				Marca marca = new Marca();
				marca.setId(new Integer(rs.getInt("MARCA_ID")));
				marca.setCveMarca(rs.getString("CVEMARCA"));
				marca.setDescMarca(rs.getString("NOMBMARCA"));
				marca.setStatus(new Integer(rs.getInt("STATUSMARCA")));
				        
				Parada parada = new Parada();
				parada.setIdParada(new Integer(rs.getInt("PARADA_ID")));
				parada.setCveParada(rs.getString("CVEPARADA"));
				parada.setDescParada(rs.getString("DESCPARADA"));
				parada.setStatus(new Integer(rs.getInt("STATUSPARADA")));
				parada.setFecHorAct(rs.getDate("FECHORACT"));
				                         
				Usuario usuario = new Usuario();
				usuario.setIdUsuario(new Integer(rs.getInt("USUARIO_ID")));
				usuario.setCveUsuario(rs.getString("CVEUSUARIO"));
				usuario.setNombre(rs.getString("NOMBRE"));
				usuario.setPaterno(rs.getString("PATERNO"));
				usuario.setMaterno(rs.getString("MATERNO"));
				parada.setUsuario(usuario);
				
				MarcaParada mp = new MarcaParada();
				mp.setMarca(marca);
				mp.setParada(parada);
				marcaParadas.add(mp);
			}
			return marcaParadas;
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}
	
	private String queryBuscaMarcaParadas(){ 
		String sql = "SELECT M.MARCA_ID, M.CVEMARCA, M.NOMBMARCA, M.STATUS AS STATUSMARCA,"
				+ "			 P.PARADA_ID, P.CVEPARADA, P.DESCPARADA, P.STATUS AS STATUSPARADA, P.FECHORACT,"
				+ "          U.USUARIO_ID, U.CVEUSUARIO, U.NOMBRE, U.PATERNO, U.MATERNO "
				+ "	FROM CCUENTAS.MARCA_PARADA MP, GENERAL.MARCA M, CCUENTAS.PARADA P, GENERAL.USUARIO U"
				+ "	WHERE MP.MARCA_ID = M.MARCA_ID"
				+ "	AND MP.PARADA_ID = P.PARADA_ID"
				+ "	AND P.USUARIO_ID = U.USUARIO_ID";
		return sql;
	}
}
