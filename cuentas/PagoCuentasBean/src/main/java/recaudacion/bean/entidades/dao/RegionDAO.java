package recaudacion.bean.entidades.dao;

import java.util.List;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.general.Region;

public class RegionDAO extends DAO {
	private List<Region> lstRegion = null;

	public List<Region> buscaRegionesActivas() {
		try {

			String hql = queryBuscaRegionesActivas();

			Query<Region> query = getDb().createQuery(hql,Region.class);
			setLstRegion(query.list());

		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
		return getLstRegion();
	}

	private String queryBuscaRegionesActivas() {
		return "  select r from recaudacion.bean.entidades.general.Region r where r.status = 1 order by r.descRegion  ";
	}

	public List<Region> getLstRegion() {
		return lstRegion;
	}

	public void setLstRegion(List<Region> lstRegion) {
		this.lstRegion = lstRegion;
	}
}
