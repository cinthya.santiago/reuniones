package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="DETALLE_MARCA", schema="CCUENTAS")
public class DetalleMarca implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	protected DetalleMarcaKey key = null;
	
	@Column(name="VALOR_FIANZA")
	private Double valorFianza;
	
	@Column(name="FECHA_FIANZA")
	private Date fechaFianza;
	
	@Column(name="FACTOR_KMS")
	private Double factorKms;
	
	@Column(name="VALOR_CUENTA")
	private Double valorCuenta;
	
	@Column(name="CUENTAS_PERMITIDAS")
	private Integer cuentasPermitidas;
	
	@Column(name="FECHORACT")
	private Date fecHorAct;
	
	@Column(name="USUARIO_ID")
	private Integer usuarioId;
	
	@Column(name="ESTATUS")
	private Integer estatus;


	@Column(name="MINS_CANCELACION")
	private Integer minsCancelacion;
	
	public DetalleMarcaKey getKey() {
		return key;
	}

	public void setKey(DetalleMarcaKey key) {
		this.key = key;
	}

	public Double getValorFianza() {
		return valorFianza;
	}

	public void setValorFianza(Double valorFianza) {
		this.valorFianza = valorFianza;
	}

	public Date getFechaFianza() {
		return fechaFianza;
	}

	public void setFechaFianza(Date fechaFianza) {
		this.fechaFianza = fechaFianza;
	}

	public Double getFactorKms() {
		return factorKms;
	}

	public void setFactorKms(Double factorKms) {
		this.factorKms = factorKms;
	}

	public Double getValorCuenta() {
		return valorCuenta;
	}

	public void setValorCuenta(Double valorCuenta) {
		this.valorCuenta = valorCuenta;
	}

	public Integer getCuentasPermitidas() {
		return cuentasPermitidas;
	}

	public void setCuentasPermitidas(Integer cuentasPermitidas) {
		this.cuentasPermitidas = cuentasPermitidas;
	}

	public Date getFecHorAct() {
		return fecHorAct;
	}

	public void setFecHorAct(Date fecHorAct) {
		this.fecHorAct = fecHorAct;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public Integer getMinsCancelacion() {
		return minsCancelacion;
	}

	public void setMinsCancelacion(Integer minsCancelacion) {
		this.minsCancelacion = minsCancelacion;
	}
	
	
}
