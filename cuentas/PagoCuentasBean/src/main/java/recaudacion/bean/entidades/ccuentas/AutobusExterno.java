package recaudacion.bean.entidades.ccuentas;

import java.util.Date;
import java.util.Vector;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import asu.bean.entidades.dto.ObjetoGenerico;
import recaudacion.bean.entidades.general.Marca;
import recaudacion.bean.entidades.general.Region;
import recaudacion.bean.entidades.general.Zona;

@Entity
@Table(name = "AUTOBUS_EXTERNO", schema = "CCUENTAS")
public class AutobusExterno extends ObjetoGenerico {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AUTOBUSEXT_ID")
	@SequenceGenerator(name = "AUTOBUSEXT_ID", sequenceName = "CCUENTAS.AUTOBUS_EXTERNO_SEQ", allocationSize = 1)
	@Column(name = "AUTOBUSEXT_ID")
	private Integer idAutobus;

	@Column(name = "NUM_ECONOMICO_ID")
	private String numeroEconomico;

	@ManyToOne
	@JoinColumn(name="REGION_ID")
	private Region region;

	@ManyToOne
	@JoinColumn(name="MARCA_ID")
	private Marca marca;

	@ManyToOne
	@JoinColumn(name="ZONA_ID")
	private Zona zona;

	@ManyToOne
	@JoinColumn(name="ASOCIADO_ID")
	private Asociados asociado;
	
	@Column(name = "MODELO")
	private Integer modelo;
	
	@Column(name = "TIPOAUTOBUS_ID")
	private Integer idTipoAutobus;
	
	@Column(name = "PLACAS")
	private String placas;

	@Column(name = "ASIENTOS")
	private Integer asientos;

	@Column(name = "TARJETA_IAVE")
	private String tarjetaIave;
	
	@Column(name = "FECHORACT")
	private Date fecHorAct;
	
	@Column(name = "USUARIO_ID")
	private Integer idUsuario;

	@Column(name = "ESTATUS")
	private Integer estatus;
	
	@Column(name="TIPO_VEHICULO")
	private String tipoVehiculo;

	public Integer getIdAutobus() {
		return idAutobus;
	}

	public void setIdAutobus(Integer idAutobus) {
		this.idAutobus = idAutobus;
	}

	public String getNumeroEconomico() {
		return numeroEconomico;
	}

	public void setNumeroEconomico(String numeroEconomico) {
		this.numeroEconomico = numeroEconomico;
	}

	@Override
	public Integer getId() {
		return getIdAutobus();
	}

	@Override
	public void setId(Integer id) {
		setIdAutobus(id);
	}

	@Override
	public void setClave(String clave) {
		this.setNumeroEconomico(clave);
	}

	@Override
	public String getClave() {
		return getNumeroEconomico();
	}

	@Override
	public String getDescripcion() {
		return this.getPlacas();
	}

	@Override
	public void setDescripcion(String descripcion) {
		this.setPlacas(descripcion);
	}

	@Override
	public void getQueryPorClave() {
		String query = "Select c from recaudacion.bean.entidades.ccuentas.AutobusExterno c where numeroEconomico like ? and estatus = 1 ";
		if(getMarca().getId() != null){
			query +=  " and  c.marca.id = ? ";
		}
		query += "order by idAutobus ";
		setQueryGenerico(query);
		setParametros(new Vector<Object>());
		getParametros().add("%"+getNumeroEconomico().trim()+"%");
		if(getMarca().getId() != null){
			getParametros().add(getMarca().getId());
		}
	}

	public void getQueryNumeroEconomicoZona() {

	}

	@Override
	public void getQueryPorDescripcion() {
		String query = "Select c from recaudacion.bean.entidades.ccuentas.AutobusExterno c where upper(numeroEconomico) like ? and estatus = 1 ";
		if(getMarca().getId()!=null){
			query +=  "  and c.marca.id = ? ";
		}
		query += "order by idAutobus ";
		setQueryGenerico(query);
		setParametros(new Vector<Object>());
		getParametros().add("%"+getPlacas().trim()+"%");
		if(getMarca().getId() != null){
			getParametros().add(getMarca().getId());
		}

	}

	@Override
	public void getQueryPorId() {
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void seEstatus(Integer status) {
		this.estatus = status;
	}

	public String getPlacas() {
		return placas;
	}

	public void setPlacas(String placas) {
		this.placas = placas;
	}

	public Integer getModelo() {
		return modelo;
	}

	public void setModelo(Integer modelo) {
		this.modelo = modelo;
	}

	public Integer getIdTipoAutobus() {
		return idTipoAutobus;
	}

	public void setIdTipoAutobus(Integer idTipoAutobus) {
		this.idTipoAutobus = idTipoAutobus;
	}

	public Integer getAsientos() {
		return asientos;
	}

	public void setAsientos(Integer asientos) {
		this.asientos = asientos;
	}

	public String getTarjetaIave() {
		return tarjetaIave;
	}

	public void setTarjetaIave(String tarjetaIave) {
		this.tarjetaIave = tarjetaIave;
	}

	public Date getFecHorAct() {
		return fecHorAct;
	}

	public void setFecHorAct(Date fecHorAct) {
		this.fecHorAct = fecHorAct;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public Zona getZona() {
		return zona;
	}

	public void setZona(Zona zona) {
		this.zona = zona;
	}

	public Asociados getAsociado() {
		return asociado;
	}

	public void setAsociado(Asociados asociado) {
		this.asociado = asociado;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public String getTipoVehiculo() {
		return tipoVehiculo;
	}

	public void setTipoVehiculo(String tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}
}
