package recaudacion.bean.entidades.general;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "MARCA_CLASESERVICIO", schema = "GENERAL")
public class MarcaClaseServicio implements Serializable{

	private static final long serialVersionUID = 1L;

	@Column(name = "MARCA_ID")
	private Marca marca = null;
	
	@Column(name ="CLASESERVICIO_ID")
	private Integer claseServicioId = null;

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public Integer getClaseServicioId() {
		return claseServicioId;
	}

	public void setClaseServicioId(Integer claseServicioId) {
		this.claseServicioId = claseServicioId;
	}
}
