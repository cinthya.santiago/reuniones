package recaudacion.bean.entidades.general;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "REGION_MARCA", schema = "GENERAL")
public class RegionMarca implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@OneToOne
	@JoinColumn(name = "REGION_ID", nullable = false)
	private Region region;

	@Id
	@OneToOne
	@JoinColumn(name = "MARCA_ID", nullable = false)
	private Marca marca;

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region regionId) {
		this.region = regionId;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marcaId) {
		this.marca = marcaId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((marca == null) ? 0 : marca.hashCode());
		result = prime * result + ((region == null) ? 0 : region.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegionMarca other = (RegionMarca) obj;
		if (marca == null) {
			if (other.marca != null)
				return false;
		} else if (!marca.equals(other.marca))
			return false;
		if (region == null) {
			if (other.region != null)
				return false;
		} else if (!region.equals(other.region))
			return false;
		return true;
	}
	
	
}
