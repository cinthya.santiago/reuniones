package recaudacion.bean.entidades.general;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MARCA", schema = "GENERAL")
public class Marca implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "MARCA_ID")
	public Integer id = null;

	@Column(name = "NOMBMARCA")
	public String descMarca = null;

	@Column(name = "CVEMARCA")
	public String cveMarca = null;

	@Column(name = "STATUS")
	public Integer status = null;

	public String getCveMarca() {
		return cveMarca;
	}

	public void setCveMarca(String cveMarca) {
		this.cveMarca = cveMarca;
	}

	public String getDescMarca() {
		return descMarca;
	}

	public void setDescMarca(String descMarca) {
		this.descMarca = descMarca;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}
}