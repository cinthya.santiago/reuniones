package recaudacion.bean.entidades.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.List;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.RegistroIncidencias;
import recaudacion.bean.entidades.general.Empleado;

public class IncidenciasDAO extends DAO
{
	private List<RegistroIncidencias> lstIncidencias = null;
	private RegistroIncidencias incidencia = null;

	
	public void consultaIncienciasPorMarca(Integer idMarcaRol, Date fecha)
	{
		try {
			String hql = queryBuscaViajesMarcarol();
			Query<RegistroIncidencias> query = getDb().createQuery(hql,RegistroIncidencias.class);
			query.setParameter(0, idMarcaRol);
			query.setParameter(1, fecha);
			setLstIncidencias(query.list());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}
	
	private String queryBuscaViajesMarcarol()
	{
		return "select x from recaudacion.bean.entidades.ccuentas.RegistroIncidencias x where x.marcaRol.rolId = ? and trunc(x.fechaIngreso) =  trunc( ? ) order by x.fechaIngreso";
	}

	public List<RegistroIncidencias> getLstIncidencias() {
		return lstIncidencias;
	}

	public void setLstIncidencias(List<RegistroIncidencias> lstIncidencias) {
		this.lstIncidencias = lstIncidencias;
	}

	public void buscaIncidenciaConductor(Integer idConductor) {
		try {
			String hql = queryBuscaIncidenciaConductor();
			Query<RegistroIncidencias> query = getDb().createQuery(hql,RegistroIncidencias.class);
			query.setParameter(0, idConductor);
			setLstIncidencias(query.list());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}
	
	private String queryBuscaIncidenciaConductor() {
		return "select x from recaudacion.bean.entidades.ccuentas.RegistroIncidencias x where x.estatus = 2 and x.conductor.idEmpleado = ? ";
	}

	public List<RegistroIncidencias> consultaIncidenciasGrupoConductor(String idsConductores) throws Exception {
		try {

			PreparedStatement ps = null;
			String sql = queryConsultaTarjetasGpoConductores(idsConductores);

			ps = getConexion().prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				RegistroIncidencias ri = new RegistroIncidencias();
				Empleado em = new Empleado();

				ri.setIncidenciaId(rs.getInt("REGISTROINCIDENCIAS_ID"));
				em.setIdEmpleado(rs.getInt("CONDUCTOR_ID"));
				em.setCveEmpleado(rs.getString("CVEEMPLEADO"));
				ri.setConductor(em);
				
				getLstIncidencias().add(ri);
			}
			return getLstIncidencias();
		
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	
	public String queryConsultaTarjetasGpoConductores(String ids) {
		return "SELECT COUNT(RI.REGISTROINCIDENCIAS_ID) as REGISTROINCIDENCIAS_ID, RI.CONDUCTOR_ID, E.CVEEMPLEADO"
				+ " FROM CCUENTAS.REGISTRO_INCIDENCIAS RI, GENERAL.EMPLEADO E"
				+ " WHERE RI.CONDUCTOR_ID = E.EMPLEADO_ID(+)"
				+ " AND RI.ESTATUS = 2"
				+ " AND E.CVEEMPLEADO IN("+ids+")"
				+ " GROUP BY CONDUCTOR_ID, CVEEMPLEADO ";
	}
	
	public RegistroIncidencias getIncidencia() {
		return incidencia;
	}

	public void setIncidencia(RegistroIncidencias incidencia) {
		this.incidencia = incidencia;
	}
}
