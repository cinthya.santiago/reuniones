package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import recaudacion.bean.entidades.general.Marca;
import recaudacion.bean.entidades.general.Usuario;

@Entity
@Table(name="MARCA_ROL", schema = "CCUENTAS")
public class MarcaRol implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="MARCAROL_ID")
	@SequenceGenerator(name="MARCAROL_ID", sequenceName="CCUENTAS.MARCA_ROL_SEQ", allocationSize=1)
	@Column(name="MARCAROL_ID")
	private Integer rolId;
	
	@Column(name="DESC_ROL")
	private String descripcionRol;

	@ManyToOne
	@JoinColumn(name="MARCA_ID")
	private Marca marca;
	
	@Column(name="FECHORACT")
	private Date fecHorAct;
	
	@ManyToOne
	@JoinColumn(name="USUARIO_ID")
	private Usuario usuario;
	
	@Column(name="ESTATUS")
	private Integer estatus;
	
	public Integer getRolId() {
		return rolId;
	}
	public void setRolId(Integer rolId) {
		this.rolId = rolId;
	}
	public String getDescripcionRol() {
		return descripcionRol;
	}
	public void setDescripcionRol(String descripcionRol) {
		this.descripcionRol = descripcionRol;
	}
	public Date getFecHorAct() {
		return fecHorAct;
	}
	public void setFecHorAct(Date fecHorAct) {
		this.fecHorAct = fecHorAct;
	}
	public Marca getMarca() {
		return marca;
	}
	public void setMarca(Marca marca) {
		this.marca = marca;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Integer getEstatus() {
		return estatus;
	}
	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}
	
	public MarcaRol() {	}
	
	public MarcaRol(Integer rolId, String descripcionRol) {
		super();
		this.rolId = rolId;
		this.descripcionRol = descripcionRol;
	}
	
	
}
