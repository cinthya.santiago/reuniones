package recaudacion.bean.entidades.general;

import java.util.Vector;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import asu.bean.entidades.dto.ObjetoGenerico;

@Entity
@Table(name = "EMPLEADO", schema = "GENERAL")
public class Empleado  extends ObjetoGenerico{
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "EMPLEADO_ID")
	private Integer idEmpleado = 0;

	@Column(name = "CVEEMPLEADO")
	private String cveEmpleado = "";

	@Column(name = "NOMBRE")
	private String nombre = "";

	@Column(name = "PATERNO")
	private String paterno = "";

	@Column(name = "MATERNO")
	private String materno = "";

	@Column(name = "FUNCION_ID")
	private String funcion;

	@Column(name="STATUS")
	private Integer status;
	
	@Column(name="REGION_ID")
	private Integer regionId;
	
	@Column(name="MARCA_ID")
	private Integer marcaId;

	@Column(name="ZONA_ID")
	private Integer zonaId;
	public Integer getIdEmpleado() {
		return idEmpleado;
	}

	public void setIdEmpleado(Integer idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public String getCveEmpleado() {
		return cveEmpleado;
	}

	public void setCveEmpleado(String cveEmpleado) {
		this.cveEmpleado = cveEmpleado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPaterno() {
		return paterno;
	}

	public void setPaterno(String paterno) {
		this.paterno = paterno;
	}

	public String getMaterno() {
		return materno;
	}

	public void setMaterno(String materno) {
		this.materno = materno;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getFuncion() {
		return funcion;
	}

	public void setFuncion(String funcion) {
		this.funcion = funcion;
	}

	@Override
	public Integer getId() {
		return this.getIdEmpleado();
	}

	@Override
	public void setId(Integer id) {
		this.setIdEmpleado(id);
	}

	@Override
	public void setClave(String clave) {
		this.setCveEmpleado(clave);
	}

	@Override
	public String getClave() {
		return this.getCveEmpleado();
	}

	@Override
	public String getDescripcion() {
		String nombre = this.getNombre();
		String aPaterno = this.getPaterno();
		String aMaterno = this.getMaterno();
		
		return nombre+" "+ aPaterno+" "+ aMaterno;
	}

	@Override
	public void setDescripcion(String newDescripcion) {
		setNombre(newDescripcion);
	}

	@Override
	public void getQueryPorId() {
		
	}

	@Override
	public void getQueryPorClave() {
		String query = "Select c from recaudacion.bean.entidades.general.Empleado c where cveEmpleado like ? and status = 1 and funcion = 'CONDUCTOR' ";
		if(getMarcaId()!= null){
			query += " and marcaId = ? ";
		}
		setQueryGenerico(query);
		setParametros(new Vector<Object>());
		getParametros().add("%"+getCveEmpleado().trim()+"%");
		if(getMarcaId()!= null){
			getParametros().add(getMarcaId());
		}
	}

	@Override
	public void getQueryPorDescripcion() {
		String query = "Select c from recaudacion.bean.entidades.general.Empleado c where nombre like ? or paterno like ? or materno like ? and funcion = 'CONDUCTOR'";
		if(getMarcaId()!= null){
			query += " and marcaId = ? ";
		}
		
		setQueryGenerico(query);
		setParametros(new Vector<Object>());
		getParametros().add("%"+getDescripcion().trim()+"%");
		getParametros().add("%"+getDescripcion().trim()+"%");
		getParametros().add("%"+getDescripcion().trim()+"%");
		if(getMarcaId()!= null){
			getParametros().add(getMarcaId());
		}
	}

	public Integer getMarcaId() {
		return marcaId;
	}

	public void setMarcaId(Integer marcaId) {
		this.marcaId = marcaId;
	}

	public Integer getRegionId() {
		return regionId;
	}

	public void setRegionId(Integer regionId) {
		this.regionId = regionId;
	}

	public Integer getZonaId() {
		return zonaId;
	}

	public void setZonaId(Integer zonaId) {
		this.zonaId = zonaId;
	}
}
