package recaudacion.bean.entidades.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.general.Marca;

public class MarcaDAO extends DAO {
	
	private List<Marca> lstMarca = null;
	private Integer regionId = null;
	private String descripcion = null;
	
	public List<Marca> buscaMarcasRegionActivas() {
		try {

			String hql = getQueryBuscaMarcaRegionActiva();

			Query<Marca> query = getDb().createQuery(hql,Marca.class);
			query.setParameter(0, getRegionId());
			return query.list();

		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	public String getQueryBuscaMarcaRegionActiva() {
		return "select x.marca from recaudacion.bean.entidades.general.RegionMarca x  where x.marca.status = 1  and x.region.id = ? order by x.marca.descMarca asc";
	}
	
	public List<Marca> buscaMarcasDescripcion() {
		try {

			String hql = getQueryBuscaMarcaDescripcion();
			Query<Marca> query = getDb().createQuery(hql,Marca.class);
			return query.list();
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	public String getQueryBuscaMarcaDescripcion() {
		return "select x from recaudacion.bean.entidades.general.Marca x  where x.descMarca in ("+ getDescripcion() +") and x.status = 1";
	}
	
	public Marca recuperaMarcaXDescripcion(String descripcion){
		try {
			String hql = getQueryBuscaMarcaXdescripcion(descripcion);
			
			Query<Marca> query = getDb().createQuery(hql,Marca.class);
			Marca marca =  (Marca)query.uniqueResult();
			return marca;
			
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}
	
	private String getQueryBuscaMarcaXdescripcion(String descripcion) {
		return "select x from recaudacion.bean.entidades.general.Marca x  where x.descMarca = '" + descripcion +"'  and x.status = 1";
	}
	
	public List<Marca> buscaMarcas() throws Exception 
	{
		
		List<Marca> marcas = new ArrayList<Marca>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			 ps = getConexion().prepareStatement(queryMarcaClaseServicio());
			rs  = ps.executeQuery();
			while(rs.next()){
				Marca marca = new Marca();
				marca.setId(new Integer(rs.getInt("MARCA_ID")));
				marca.setDescMarca(rs.getString("NOMBMARCA"));
				marca.setCveMarca(rs.getString("CVEMARCA"));
				marcas.add(marca);
			}
			return marcas;
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje("El sistema no pudo recuperar las marcas");
			throw e;
		}
	}
	
	private String queryMarcaClaseServicio(){
		return "SELECT DISTINCT(M.NOMBMARCA), M.MARCA_ID, M.CVEMARCA FROM GENERAL.MARCA M, GENERAL.MARCA_CLASESERVICIO MC  "
				+ "	WHERE M.MARCA_ID =  MC.MARCA_ID  "
				+ "	AND M.STATUS = 1  "
				+ "	AND MC.CLASESERVICIO_ID > 6	 "
				+ " ORDER BY M.NOMBMARCA  ";
	}


	public List<Marca> getLstMarca() {
		return lstMarca;
	}

	public void setLstMarca(List<Marca> lstMarca) {
		this.lstMarca = lstMarca;
	}

	public Integer getRegionId() {
		return regionId;
	}

	public void setRegionId(Integer regionId) {
		this.regionId = regionId;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
