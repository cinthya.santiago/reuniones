package recaudacion.bean.entidades.dao;

import java.util.Iterator;
import java.util.List;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.TarjetaViaje;

public class TarjetaViajeDAO  extends DAO{

	private List<TarjetaViaje> lsttarjetaViajes = null;
	private TarjetaViaje tarjetaviaje = null;
	
	public void guardaTarjetas() {
		try {
			Iterator<?> it = getLsttarjetaViajes().iterator();
			while(it.hasNext()){
				TarjetaViaje tarjeta = (TarjetaViaje)it.next();
				guarda(tarjeta);
			}
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
	}
	
	public void recuperaTarjetaViajeXId(Integer idViaje){
		try {
			String hql = queryRecuperaTarjetaViajeXId();
			Query<TarjetaViaje> query = getDb().createQuery(hql,TarjetaViaje.class);
			query.setParameter(0, idViaje);
			setTarjetaviaje((TarjetaViaje) query.uniqueResult());
			
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	private String queryRecuperaTarjetaViajeXId()
	{
		return "select x from recaudacion.bean.entidades.ccuentas.TarjetaViaje x where x.viajeId = ?";
	}
	
	public void recuperaTarjetaViajesPorCta(String ids){
		try {
			String hql = queryRecuperaTarjetasViajesPorCtas(ids);
			Query<TarjetaViaje> query = getDb().createQuery(hql,TarjetaViaje.class);
			setLsttarjetaViajes(query.list());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}
	
	private String queryRecuperaTarjetasViajesPorCtas(String ids){
		return "select x from TarjetaViaje x where x.tarjetaCuentaId in ("+ids +") ";
	}
	
	public List<TarjetaViaje> getLsttarjetaViajes() {
		return lsttarjetaViajes;
	}

	public void setLsttarjetaViajes(List<TarjetaViaje> lsttarjetaViajes) {
		this.lsttarjetaViajes = lsttarjetaViajes;
	}

	public TarjetaViaje getTarjetaviaje() {
		return tarjetaviaje;
	}

	public void setTarjetaviaje(TarjetaViaje tarjetaviaje) {
		this.tarjetaviaje = tarjetaviaje;
	}
}
