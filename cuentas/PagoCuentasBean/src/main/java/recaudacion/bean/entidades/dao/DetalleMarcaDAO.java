package recaudacion.bean.entidades.dao;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.DetalleMarca;

public class DetalleMarcaDAO extends DAO 
{
	public DetalleMarca recuperaDetalleMarcaPorId(Integer marcaId, Integer marcaRolId)
	{
		DetalleMarca detalle = null;
		try {

			String hql = getQueryBuscaDetalleMarcaXIdActivas();

			Query<DetalleMarca> query = getDb().createQuery(hql,DetalleMarca.class);
			query.setParameter(0, marcaId);
			query.setParameter(1, marcaRolId);
			detalle = (DetalleMarca)query.uniqueResult();
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
		return detalle;
	}
	
	private String getQueryBuscaDetalleMarcaXIdActivas(){
		return "select x from recaudacion.bean.entidades.ccuentas.DetalleMarca x where x.key.marca.id = ? and x.key.marcaRol.rolId = ? ";
	}
	
	
}
