package recaudacion.bean.entidades.general;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TIPO_AUTOBUS", schema = "GENERAL")
public class TipoAutobus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "TIPOAUTOBUS_ID")
	private Integer id;

	@Column(name = "DESCTIPOAUTOBUS")
	private String desctipoBus;

	@Column(name = "STATUS")
	private Integer estatus;

	@Column(name = "CVE_TIPOAUTO")
	private String cveTipoAuto;

	@Column(name = "CAPACIDADLTS")
	private Integer capacidadLts;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDesctipoBus() {
		return desctipoBus;
	}

	public void setDesctipoBus(String desctipoBus) {
		this.desctipoBus = desctipoBus;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public String getCveTipoAuto() {
		return cveTipoAuto;
	}

	public void setCveTipoAuto(String cveTipoAuto) {
		this.cveTipoAuto = cveTipoAuto;
	}

	public Integer getCapacidadLts() {
		return capacidadLts;
	}

	public void setCapacidadLts(Integer capacidadLts) {
		this.capacidadLts = capacidadLts;
	}

}
