package recaudacion.bean.entidades.general;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ZONA", schema = "GENERAL")
public class Zona implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ZONA_ID")
	public Integer id = null;

	@Column(name = "NOMBZONA")
	public String descZona = null;

	@Column(name = "STATUS")
	public Integer status = null;

	public String getDescZona() {
		return descZona;
	}

	public void setDescZona(String descZona) {
		this.descZona = descZona;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
