package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import recaudacion.bean.entidades.general.Empleado;
import recaudacion.bean.entidades.general.Parada;


@Entity
@Table(schema ="CCUENTAS", name="DEPOSITO_EN_CAMINO")
public class DepositoEnCamino implements Serializable{

	private static final long serialVersionUID = 1L;
	public static final int DEPOSITO_ACTIVO = 1;
	public static final int DEPOSITO_LIQUIDADO = 2;
	

	@Id	
	@SequenceGenerator(name="DEPOSITOCAMINOID", sequenceName="CCUENTAS.DEPOSITO_EN_CAMINO_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DEPOSITOCAMINOID")
	@Column(name="DEPOSITOCAMINO_ID")
	private Integer depositoCaminoId;
	
	@ManyToOne
	@JoinColumn(name="CONDUCTOR_ID")
	private Empleado conductor;
	
	@Column(name="IMPORTE_DEPOSITO")
	private Double importeDeposito;
	
	@Column(name="FECHA_DEPOSITO")
	private Date fechaDeposito;
	
	@Column(name="USUARIO_RECAUDA")
	private Integer usuarioRecuada;
	
	@Column(name="ESTATUS")
	private Integer estatus;
	
	@Column(name="FECHORACT")
	private Date fecHorAct = null;
	
	@Column(name="USUARIO_ID")
	private Integer usuarioId;

	@ManyToOne
	@JoinColumn(name="TARJETA_CUENTA_ID")
	private TarjetaCuenta tarjetaCuenta;
	
	@ManyToOne
	@JoinColumn(name="PARADA_ID")
	private Parada parada ;
	
	@Transient
	private String cveUsuario;
	
	public Integer getDepositoCaminoId() {
		return depositoCaminoId;
	}
	

	public void setDepositoCaminoId(Integer depositoCaminoId) {
		this.depositoCaminoId = depositoCaminoId;
	}

	public Empleado getConductor() {
		return conductor;
	}

	public void setConductor(Empleado conductor) {
		this.conductor = conductor;
	}

	public Double getImporteDeposito() {
		return importeDeposito;
	}

	public void setImporteDeposito(Double importeDeposito) {
		this.importeDeposito = importeDeposito;
	}

	public Date getFechaDeposito() {
		return fechaDeposito;
	}

	public void setFechaDeposito(Date fechaDeposito) {
		this.fechaDeposito = fechaDeposito;
	}

	public Integer getUsuarioRecuada() {
		return usuarioRecuada;
	}

	public void setUsuarioRecuada(Integer usuarioRecuada) {
		this.usuarioRecuada = usuarioRecuada;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public Date getFecHorAct() {
		return fecHorAct;
	}

	public void setFecHorAct(Date fecHorAct) {
		this.fecHorAct = fecHorAct;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}


	public TarjetaCuenta getTarjetaCuenta() {
		return tarjetaCuenta;
	}


	public void setTarjetaCuenta(TarjetaCuenta tarjetaCuenta) {
		this.tarjetaCuenta = tarjetaCuenta;
	}
	public Parada getParada() {
		return parada;
	}
	public void setParada(Parada parada) {
		this.parada = parada;
	}


	public String getCveUsuario() {
		return cveUsuario;
	}


	public void setCveUsuario(String cveUsuario) {
		this.cveUsuario = cveUsuario;
	}
}