package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import recaudacion.bean.entidades.general.Parada;

@Entity
@Table(name="DETALLE_ROL", schema="CCUENTAS")
public class DetalleRol implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="DETALLEROLID", sequenceName="CCUENTAS.DETALLE_ROL_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DETALLEROLID")
	@Column(name="DETALLEROL_ID")
	private Integer detalleRolId;
	
	@Column(name="ROL_ID")
	private Integer rolId;
	
	@Column(name="CUADRO")
	private Integer cuadro;
	
	@Column(name="SECUENCIA")
	private String secuencia;
	
	@Column(name="HORA_SALIDA")
	private Time horaSalida;
	
	@ManyToOne
	@JoinColumn(name="ORIGEN_ID")
	private Parada origen;
	
	@ManyToOne
	@JoinColumn(name="DESTINO_ID")
	private Parada destino;
	
	@Column(name="KMS")
	private Integer kms;
	
	@Column(name="VIA_D")
	private Integer viaId;
	
	@Column(name="L")
	private Integer lunes;
	
	@Column(name="M")
	private Integer martes;
	
	@Column(name="W")
	private Integer miercoles;
	
	@Column(name="J")
	private Integer jueves;
		
	@Column(name="V")
	private Integer viernes;
	
	@Column(name="S")
	private Integer sabado;
	
	@Column(name="D")
	private Integer domingo;
	
	@Column(name="V_L")
	private Double valLunes;
	
	@Column(name="V_M")
	private Double valMartes;
	
	@Column(name="V_W")
	private Double valMiercoles;
	
	@Column(name="V_J")
	private Double valJueves;
	
	@Column(name="V_V")
	private Double valViernes;
	
	@Column(name="V_S")
	private Double valSabado;
	
	@Column(name="V_D")
	private Double valDomingo;
	
	@Column(name="USUARIO_ID")
	private Integer usuarioId;
	
	@Column(name="FECHORACT")
	private Date fecHorAct;

	public Integer getDetalleRolId() {
		return detalleRolId;
	}

	public void setDetalleRolId(Integer detalleRolId) {
		this.detalleRolId = detalleRolId;
	}

	public Integer getRolId() {
		return rolId;
	}

	public void setRolId(Integer rolId) {
		this.rolId = rolId;
	}

	public Integer getCuadro() {
		return cuadro;
	}

	public void setCuadro(Integer cuadro) {
		this.cuadro = cuadro;
	}

	public String getSecuencia() {
		return secuencia;
	}

	public void setSecuencia(String secuencia) {
		this.secuencia = secuencia;
	}

	public Time getHoraSalida() {
		return horaSalida;
	}

	public void setHoraSalida(Time horaSalida) {
		this.horaSalida = horaSalida;
	}

	public Integer getKms() {
		return kms;
	}

	public void setKms(Integer kms) {
		this.kms = kms;
	}

	public Integer getViaId() {
		return viaId;
	}

	public void setViaId(Integer viaId) {
		this.viaId = viaId;
	}

	public Integer getLunes() {
		return lunes;
	}

	public void setLunes(Integer lunes) {
		this.lunes = lunes;
	}

	public Integer getMartes() {
		return martes;
	}

	public void setMartes(Integer martes) {
		this.martes = martes;
	}

	public Integer getMiercoles() {
		return miercoles;
	}

	public void setMiercoles(Integer miercoles) {
		this.miercoles = miercoles;
	}

	public Integer getJueves() {
		return jueves;
	}

	public void setJueves(Integer jueves) {
		this.jueves = jueves;
	}

	public Integer getViernes() {
		return viernes;
	}

	public void setViernes(Integer viernes) {
		this.viernes = viernes;
	}

	public Integer getSabado() {
		return sabado;
	}

	public void setSabado(Integer sabado) {
		this.sabado = sabado;
	}

	public Integer getDomingo() {
		return domingo;
	}

	public void setDomingo(Integer domingo) {
		this.domingo = domingo;
	}

	public Double getValLunes() {
		return valLunes;
	}

	public void setValLunes(Double valLunes) {
		this.valLunes = valLunes;
	}

	public Double getValMartes() {
		return valMartes;
	}

	public void setValMartes(Double valMartes) {
		this.valMartes = valMartes;
	}

	public Double getValMiercoles() {
		return valMiercoles;
	}

	public void setValMiercoles(Double valMiercoles) {
		this.valMiercoles = valMiercoles;
	}

	public Double getValJueves() {
		return valJueves;
	}

	public void setValJueves(Double valJueves) {
		this.valJueves = valJueves;
	}

	public Double getValViernes() {
		return valViernes;
	}

	public void setValViernes(Double valViernes) {
		this.valViernes = valViernes;
	}

	public Double getValSabado() {
		return valSabado;
	}

	public void setValSabado(Double valSabado) {
		this.valSabado = valSabado;
	}

	public Double getValDomingo() {
		return valDomingo;
	}

	public void setValDomingo(Double valDomingo) {
		this.valDomingo = valDomingo;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	public Date getFecHorAct() {
		return fecHorAct;
	}

	public void setFecHorAct(Date fecHorAct) {
		this.fecHorAct = fecHorAct;
	}

	public Parada getOrigen() {
		return origen;
	}

	public void setOrigen(Parada origen) {
		this.origen = origen;
	}

	public Parada getDestino() {
		return destino;
	}

	public void setDestino(Parada destino) {
		this.destino = destino;
	}
	
	
}
