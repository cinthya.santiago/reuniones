package recaudacion.bean.entidades.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.AutobusExterno;

public class AutobusExternoDAO extends DAO {
	private AutobusExterno autobus = null;

	private List<AutobusExterno> lstAutobusExt = null;
	private String ids = null;
	private Integer idRegion = null;

	public void buscaAutobusesXId() {
		setLstAutobusExt(new ArrayList<AutobusExterno>());

		try {

			String hql = getQueryBuscaAutobusesXIds();
			Query<AutobusExterno> query = getDb().createQuery(hql,AutobusExterno.class);
			setLstAutobusExt(query.list());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	private String getQueryBuscaAutobusesXIds() {
		return "select e from recaudacion.bean.entidades.ccuentas.AutobusExterno e where numeroEconomico in ("
				+ getIds() + ") ";
	}

	public List<AutobusExterno> buscaBusExterno() {
		List<AutobusExterno> autobusEx = null;
		try {
			String hql = getQueryBuscaBusExterno();
			Query<AutobusExterno> query = getDb().createQuery(hql,AutobusExterno.class);
			autobusEx = query.list();
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
		return autobusEx;
	}

	private String getQueryBuscaBusExterno() {
		String sql = "select e from recaudacion.bean.entidades.ccuentas.AutobusExterno e ";
		if (getIdRegion() != null) {
			sql += "    where e.region.id = " + getIdRegion();
		}
		sql += "    order by e.idAutobus";

		return sql;
	}

	public void buscaBusExternoXNumEconomico(String clave, Integer marcaId) {
		try {
			String hql = getQueryBuscaBusNumEconomico(marcaId);
			Query<AutobusExterno> query = getDb().createQuery(hql,AutobusExterno.class);
			query.setParameter(0, clave);
			if (marcaId != null) {
				query.setParameter(1, marcaId);
			}
			setAutobus((AutobusExterno) query.uniqueResult());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	private String getQueryBuscaBusNumEconomico(Integer marcaId) {
		String sql = "select e from recaudacion.bean.entidades.ccuentas.AutobusExterno e where e.numeroEconomico like ? ";
		if (marcaId != null) {
			sql += "and e.marca.id = ? ";
		}
		return sql;
	}

	public void guardaAutobusExterno() {

		try {
			getDb().saveOrUpdate(getAutobus());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	public AutobusExterno getAutobus() {
		return autobus;
	}

	public void setAutobus(AutobusExterno autobus) {
		this.autobus = autobus;
	}

	public List<AutobusExterno> getLstAutobusExt() {
		return lstAutobusExt;
	}

	public void setLstAutobusExt(List<AutobusExterno> lstAutobusExt) {
		this.lstAutobusExt = lstAutobusExt;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public Integer getIdRegion() {
		return idRegion;
	}

	public void setIdRegion(Integer idRegion) {
		this.idRegion = idRegion;
	}
}