package recaudacion.bean.entidades.dao;

import java.util.List;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.IslaCombustible;

public class IslaCombustibleDAO extends DAO{

	public List<IslaCombustible> buscaIslasCombustible() {
		try {
			String hql = "select e from IslaCombustible e";
			Query<IslaCombustible> query = getDb().createQuery(hql,IslaCombustible.class);
			return query.list();
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}
}
