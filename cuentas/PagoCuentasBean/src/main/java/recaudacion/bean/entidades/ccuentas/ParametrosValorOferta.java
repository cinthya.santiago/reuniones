package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;
import java.util.Vector;

public class ParametrosValorOferta implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private String marca;
	private String tramoMarca;
	private String numCuadros;
	private String numVueltas;
	private String tipoOferta;
	private String descTipoOferta;
	private Vector<Object> lstHorarios = null;
	private Vector<ValorOferta> valorOferta;
	
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getNumCuadros() {
		return numCuadros;
	}
	public void setNumCuadros(String numCuadros) {
		this.numCuadros = numCuadros;
	}
	public String getNumVueltas() {
		return numVueltas;
	}
	public void setNumVueltas(String numVueltas) {
		this.numVueltas = numVueltas;
	}
	public String getTipoOferta() {
		return tipoOferta;
	}
	public void setTipoOferta(String tipoOferta) {
		this.tipoOferta = tipoOferta;
	}
	public String getDescTipoOferta() {
		return descTipoOferta;
	}
	public void setDescTipoOferta(String descTipoOferta) {
		this.descTipoOferta = descTipoOferta;
	}
	public String getTramoMarca() {
		return tramoMarca;
	}
	public void setTramoMarca(String tramoMarca) {
		this.tramoMarca = tramoMarca;
	}
	public Vector<ValorOferta> getValorOferta() {
		return valorOferta;
	}
	public void setValorOferta(Vector<ValorOferta> valorOferta) {
		this.valorOferta = valorOferta;
	}
	public Vector<Object> getLstHorarios() {
		return lstHorarios;
	}
	public void setLstHorarios(Vector<Object> lstHorarios) {
		this.lstHorarios = lstHorarios;
	}
}
