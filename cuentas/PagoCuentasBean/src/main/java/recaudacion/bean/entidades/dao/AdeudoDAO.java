package recaudacion.bean.entidades.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.Adeudo;

public class AdeudoDAO extends DAO{

	private List<Adeudo> lstAdeudo = null;
	
	public void consultaAdeudoConductor(Integer idConductor){
		setLstAdeudo(new ArrayList<Adeudo>());
		try {
			String hql = queryBuscaAdeudoConductor();
			Query<Adeudo> query = getDb().createQuery(hql,Adeudo.class);
			query.setParameter(0, idConductor);
			setLstAdeudo(query.list());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		} 
	}

	private String queryBuscaAdeudoConductor() {
		return "select x from recaudacion.bean.entidades.ccuentas.Adeudo x where x.conductor.idEmpleado = ? ";
	}

	public Adeudo guardaAdeudo(Adeudo adeudo){

		try {
			getDb().saveOrUpdate(adeudo);
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
		return adeudo;
	}
	
	public List<Adeudo> getLstAdeudo() {
		return lstAdeudo;
	}


	public void setLstAdeudo(List<Adeudo> lstAdeudo) {
		this.lstAdeudo = lstAdeudo;
	}
}
