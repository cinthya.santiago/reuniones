package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import recaudacion.bean.entidades.general.Autobus;
import recaudacion.bean.entidades.general.Empleado;

@Entity
@Table(name="REGISTRO_ODOMETRO", schema="CCUENTAS")
public class RegistroOdometro implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="REGISTROODOMETRO_ID", sequenceName="CCUENTAS.REGISTRO_ODOMETRO_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator ="REGISTROODOMETRO_ID")
	@Column(name = "REGISTROODOMETRO_ID")
	private Integer registroOdometroId;
	
	@OneToOne
	@JoinColumn(name="AUTOBUS_ID")
	private Autobus autobus;

	@OneToOne
	@JoinColumn(name="AUTOBUSEXT_ID")
	private AutobusExterno autobusExterno;
	
	@Column(name = "KILOMETROS")
	private Integer kilometros;

	@OneToOne
	@JoinColumn(name="CONDUCTOR_ID")
	private Empleado conductor;
	
	@Column(name = "FECHORACT")
	private Date fecHorAct;
	
	@Column(name = "USUARIO_ID")
	private Integer usuarioId;
	
	public Integer getRegistroOdometroId() {
		return registroOdometroId;
	}

	public void setRegistroOdometroId(Integer registroOdometroId) {
		this.registroOdometroId = registroOdometroId;
	}

	public Empleado getConductor() {
		return conductor;
	}

	public void setConductor(Empleado conductor) {
		this.conductor = conductor;
	}

	public Autobus getAutobus() {
		return autobus;
	}

	public void setAutobus(Autobus autobus) {
		this.autobus = autobus;
	}

	public AutobusExterno getAutobusExterno() {
		return autobusExterno;
	}

	public void setAutobusExterno(AutobusExterno autobusExterno) {
		this.autobusExterno = autobusExterno;
	}

	public Integer getKilometros() {
		return kilometros;
	}

	public void setKilometros(Integer kilometros) {
		this.kilometros = kilometros;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	public Date getFecHorAct() {
		return fecHorAct;
	}

	public void setFecHorAct(Date fecHorAct) {
		this.fecHorAct = fecHorAct;
	}
}
