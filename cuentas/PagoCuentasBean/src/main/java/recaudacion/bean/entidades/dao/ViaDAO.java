package recaudacion.bean.entidades.dao;

import java.util.List;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.general.Via;

public class ViaDAO extends DAO 
{
	private List<Via> lstVias = null;
	
	private String descripciones = null;
	
	public List<Via> buscaViasActivas() {
		try {

			String hql = getQueryBuscaViasActivas();
			Query<Via> query = getDb().createQuery(hql,Via.class);
			return query.list();
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	
	public List<Via> buscaViasActivasDescripcion() {
		try {

			String hql = getQueryBuscaViasActivas();
			Query<Via> query = getDb().createQuery(hql,Via.class);
			return query.list();
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}
	
	public String getQueryBuscaViasActivas() {
		String sql = "select x from recaudacion.bean.entidades.general.Via x  where x.status = 1  and x.autorizado = 1 ";
		
		if(getDescripciones() != null) {
			sql += " and x.nombVia in ("+getDescripciones()+") ";
		}
		
		sql	+= "order by x.nombVia asc";
		
		return sql;
	}

	public List<Via> getLstVias() {
		return lstVias;
	}

	public void setLstVias(List<Via> lstVias) {
		this.lstVias = lstVias;
	}

	public String getDescripciones() {
		return descripciones;
	}

	public void setDescripciones(String descripciones) {
		this.descripciones = descripciones;
	}
}
