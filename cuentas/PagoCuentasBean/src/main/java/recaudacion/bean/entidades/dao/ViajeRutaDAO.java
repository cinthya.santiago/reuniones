package recaudacion.bean.entidades.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.RptAdicionales;
import recaudacion.bean.entidades.ccuentas.ViajeRuta;

public class ViajeRutaDAO extends DAO {

	private ViajeRuta viajeRuta = null;

	public List<ViajeRuta> buscaViajesRuta(Date fechaIni, Date fechaFin) {
		try {

			String hql = getQueryViajesRuta();
			Query<ViajeRuta> query = getDb().createQuery(hql,ViajeRuta.class);
			query.setParameter(0, fechaIni);
			query.setParameter(1, fechaFin);
			return query.list();

		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	public String getQueryViajesRuta() {
		return "select e from recaudacion.bean.entidades.ccuentas.ViajeRuta e where e.fechaAlta between ? and ?";
	}

	public List<ViajeRuta> buscaViajesRutaConductor(Integer idConductor){
		try {
			String hql = queryBuscaViajesRutaConductor();
			Query<ViajeRuta> query = getDb().createQuery(hql,ViajeRuta.class);
			query.setParameter(0, idConductor);
			query.setParameter(1, idConductor);
			return query.list();
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}
	private String queryBuscaViajesRutaConductor() {
		return "select x from ViajeRuta x  where (x.conductor1.idEmpleado = ? or x.conductor2.idEmpleado = ? ) and x.status = 1 ";
	}


	public void eliminaViajeRuta(ViajeRuta viajeRuta) {
		try {

			getDb().delete(viajeRuta);

		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}
	
	public List<RptAdicionales> buscaTotalesViajesRuta(Date fecInicial, Date FecFinal, Integer recaudadorId) throws Exception {
		List<RptAdicionales> totales = new ArrayList<RptAdicionales>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getConexion().prepareStatement(queryTotalesViajesRuta(recaudadorId));
			ps.setTimestamp(1, new Timestamp(fecInicial.getTime()));
			ps.setTimestamp(2, new Timestamp(FecFinal.getTime()));
			if(recaudadorId != null){ 
				ps.setObject(3, recaudadorId);
			}
			rs = ps.executeQuery();
			while(rs.next()){ 
				RptAdicionales rpt = new RptAdicionales();
				rpt.setFolio(new Integer(rs.getInt("VIAJERUTA_ID")));
				rpt.setCantidad(rs.getObject("ORIGEN_DESTINO"));
				rpt.setImporte(new Double(rs.getDouble("IMPORTE_SERVICIO")));
				totales.add(rpt);
			}
			return totales;
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}
	
	

	private String queryTotalesViajesRuta(Integer recaudadorId) {

		String sql = "SELECT VIAJERUTA_ID, (ORIGEN ||' - '||DESTINO) AS ORIGEN_DESTINO, IMPORTE_SERVICIO"
				+ "		FROM CCUENTAS.VIAJE_RUTA" + "	"
				+ "   WHERE FECHA_ALTA BETWEEN  ?  AND  ?  ";
		if (recaudadorId != null) {
			sql += "		AND USUARIO_ALTA = ?  ";
		}
		return sql;
	
	}

	public ViajeRuta getViajeRuta() {
		return viajeRuta;
	}

	public void setViajeRuta(ViajeRuta viajeRuta) {
		this.viajeRuta = viajeRuta;
	}
}