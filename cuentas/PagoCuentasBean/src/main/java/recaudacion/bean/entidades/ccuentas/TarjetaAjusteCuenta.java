package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="TARJETA_AJUSTECUENTAS", schema="CCUENTAS")
public class TarjetaAjusteCuenta implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator ="TARJETAAJUSTECUENTA_ID")
	@SequenceGenerator(name="TARJETAAJUSTECUENTA_ID", sequenceName="CCUENTAS.TARJETA_AJUSTECUENTAS_SEQ", allocationSize = 1)
	@Column(name="TARJETAAJUSTECUENTA_ID")
	private Integer tarjetaAjusteCuentaId;
	
	@Column(name="TARJETACUENTA_ID")
	private Integer tarjetaCuentaId;
	
	@Column(name="VIAJE_ID")
	private Integer viajeId;

	@Column(name="FECHA_AJUSTE")
	private Date fechaAjuste;
	
	@Column(name="MOTIVOAJUSTE")
	private String motivoAjuste;
	
	@Column(name="COSTO_PROGRAMADO")
	private Double costoProgramado;

	@Column(name="COSTO_AJUSTE")
	private Double costoAjuste;

	@Column(name="ESTATUS_VIAJE")
	private Integer estatusViaje;
	
	@Column(name="FECHORACT")
	private Date fecHorAct;
	
	@Column(name="USUARIO_ID")
	private Integer usuarioId;

	@Column(name="KILOMETRO_PROGRAMADO")
	private Integer kmsProgramados;
	
	public Integer getTarjetaAjusteCuentaId() {
		return tarjetaAjusteCuentaId;
	}

	public void setTarjetaAjusteCuentaId(Integer tarjetaAjusteCuentaId) {
		this.tarjetaAjusteCuentaId = tarjetaAjusteCuentaId;
	}

	public Integer getTarjetaCuentaId() {
		return tarjetaCuentaId;
	}

	public void setTarjetaCuentaId(Integer tarjetaCuentaId) {
		this.tarjetaCuentaId = tarjetaCuentaId;
	}

	public Integer getViajeId() {
		return viajeId;
	}

	public void setViajeId(Integer viajeId) {
		this.viajeId = viajeId;
	}

	public Date getFechaAjuste() {
		return fechaAjuste;
	}

	public void setFechaAjuste(Date fechaAjuste) {
		this.fechaAjuste = fechaAjuste;
	}

	public String getMotivoAjuste() {
		return motivoAjuste;
	}

	public void setMotivoAjuste(String motivoAjuste) {
		this.motivoAjuste = motivoAjuste;
	}

	public Double getCostoProgramado() {
		return costoProgramado;
	}

	public void setCostoProgramado(Double costoProgramado) {
		this.costoProgramado = costoProgramado;
	}

	public Double getCostoAjuste() {
		return costoAjuste;
	}

	public void setCostoAjuste(Double costoAjuste) {
		this.costoAjuste = costoAjuste;
	}

	public Integer getEstatusViaje() {
		return estatusViaje;
	}

	public void setEstatusViaje(Integer estatusViaje) {
		this.estatusViaje = estatusViaje;
	}

	public Date getFecHorAct() {
		return fecHorAct;
	}

	public void setFecHorAct(Date fecHorAct) {
		this.fecHorAct = fecHorAct;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	public Integer getKmsProgramados() {
		return kmsProgramados;
	}

	public void setKmsProgramados(Integer kmsProgramados) {
		this.kmsProgramados = kmsProgramados;
	}
}
