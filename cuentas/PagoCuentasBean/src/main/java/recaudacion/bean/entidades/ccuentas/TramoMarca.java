package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "TRAMOS_MARCA", schema = "CCUENTAS")
public class TramoMarca implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "TRAMOSMARCA_ID")
	private Integer tramoMarcaId;
	
	@EmbeddedId
	protected TramoMarcaKey key;

	@Column(name = "KMS_RECORRIDOS")
	private Integer kmsRecorridos;
	
	@Column(name = "FECHORACT")
	private Date fecHorAct;

	@Column(name = "USUARIO_ID")
	private Integer usuarioId;

	@Column(name = "ESTATUS")
	private Integer estatus;

	@Column(name = "CON_PISTA")
	private Integer conPista;

	@Column(name = "COSTO")
	private Double costo;

	public Integer getKmsRecorridos() {
		return kmsRecorridos;
	}

	@Transient
	private Date tRecorrido;
	
	public void setKmsRecorridos(Integer kmsRecorridos) {
		this.kmsRecorridos = kmsRecorridos;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	public Date getFecHorAct() {
		return fecHorAct;
	}

	public void setFecHorAct(Date fecHorAct) {
		this.fecHorAct = fecHorAct;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public Integer getConPista() {
		return conPista;
	}

	public void setConPista(Integer conPista) {
		this.conPista = conPista;
	}

	public Double getCosto() {
		return costo;
	}

	public void setCosto(Double costo) {
		this.costo = costo;
	}

	public TramoMarcaKey getKey() {
		return key;
	}

	public void setKey(TramoMarcaKey key) {
		this.key = key;
	}

	public Integer getTramoMarcaId() {
		return tramoMarcaId;
	}

	public void setTramoMarcaId(Integer tramoMarcaId) {
		this.tramoMarcaId = tramoMarcaId;
	}

	public Date gettRecorrido() {
		return tRecorrido;
	}

	public void settRecorrido(Date tRecorrido) {
		this.tRecorrido = tRecorrido;
	}
}
