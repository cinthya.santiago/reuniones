package recaudacion.bean.entidades.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.Viajes;

public class ViajeDAO extends DAO {
	private Vector<Object> viajes = null;
	private List<Viajes> lstViajes = null;

	private Viajes viaje = null;
	private Integer idViaje = null;

	private String idViajes = null;
	private Integer estatusViaje = null;
	
	public void consultaViajesPorMarcaRol(Integer idRol, Date fecha) {
		setLstViajes(new ArrayList<Viajes>());
		try {
			String hql = queryBuscaViajesMarcarol();
			Query<Viajes> query = getDb().createQuery(hql,Viajes.class);
			query.setParameter(0, idRol);
			query.setParameter(1, fecha);
			setLstViajes(query.list());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	private String queryBuscaViajesMarcarol() {
		return "select x from recaudacion.bean.entidades.ccuentas.Viajes x where x.rol.rolId = ? and x.fecha =  ? order by x.viajeId";
	}

	public List<Viajes> consultaViajesPorMarcaMarcaRol(Integer marcaId, Integer marcaRolId, Date fecha) {
		setLstViajes(new ArrayList<Viajes>());
		try {
			String hql = queryBuscaViajesMarcaMarcaRol();
			Query<Viajes> query = getDb().createQuery(hql,Viajes.class);
			query.setParameter("marcaId", marcaId);
			query.setParameter("marcaRolId", marcaRolId);
			query.setParameter("fecha", fecha);
			return query.list();
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	private String queryBuscaViajesMarcaMarcaRol() {
		return "select x from recaudacion.bean.entidades.ccuentas.Viajes x where x.marca.id = :marcaId and x.marcarol.rolId = :marcaRolId  and x.fecha =  :fecha order by x.viajeId";
	}

	public void guardaViajes() throws Exception {
		try {
			Iterator<?> it = getViajes().iterator();
			while (it.hasNext()) {
				Viajes viaje = (Viajes) it.next();
				guarda(viaje);
			}
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}
	

	public void actualizaViaje() {

		try {
			getDb().saveOrUpdate(getViaje());
			setIdViaje(getViaje().getViajeId());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		} 
	}

	public void actualizaRecursosViajes() throws Exception {
		PreparedStatement ps = null;
		try {
			Iterator<Object> it = getViajes().iterator();
			while (it.hasNext()) {
				@SuppressWarnings("unchecked")
				Vector<Object> obj = (Vector<Object>) it.next();
				Viajes viaje = (Viajes) obj.get(0);

				String query = getQueryActualizaRecursos(viaje);

				ps = getConexion().prepareStatement(query);
				ps.setTimestamp(1, new Timestamp(viaje.getFecHorAct().getTime()));
				ps.setObject(2, viaje.getUsuarioId());
				ps.setObject(3, viaje.getConductor().getIdEmpleado());
				if (viaje.getAutobus() != null) {
					ps.setObject(4, viaje.getAutobus().getIdAutobus());
				}
				if (viaje.getAutobusExterno() != null) {
					ps.setObject(4, viaje.getAutobusExterno().getIdAutobus());
				}
				ps.setObject(5, viaje.getViajeId());
				ps.executeUpdate();
			}
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}


	private String getQueryActualizaRecursos(Viajes viaje) {
		String sql = "UPDATE CCUENTAS.VIAJES SET FECHORAT = ?, USUARIO_ID = ?, CONDUCTOR_ID = ?  ";
		if (viaje.getAutobus() != null) {
			sql += " ,  AUTOBUS_ID = ? ";
		}
		if (viaje.getAutobusExterno() != null) {
			sql += " ,  AUTOBUSEXT_ID = ?  ";
		}
		sql += "  WHERE  VIAJES_ID = ? AND STAUS_VIAJE = 1 ";
		return sql;
	}

	public void buscaViajesPorConductor(Integer idConductor, Date fecha) {
		setLstViajes(new ArrayList<Viajes>());
		try {
			String hql = queryBuscaViajesPorConductor();
			Query<Viajes> query = getDb().createQuery(hql,Viajes.class);
			query.setParameter(0, idConductor);
			query.setParameter(1, fecha);
			setLstViajes(query.list());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	private String queryBuscaViajesPorConductor() {
		return "  select v from recaudacion.bean.entidades.ccuentas.Viajes v where v.conductor.idEmpleado = ? and v.fecha = ?  ";
	}

	public void buscaViajesPorRolPeriodo(Integer rolId, Date fechaInicial, Date fechaFinal) {
		try {
			String hql = queryBuscaViajesRolPeriodo();

			Query<Viajes> query = getDb().createQuery(hql,Viajes.class);
			query.setParameter(0, rolId);
			query.setParameter(1, fechaInicial);
			query.setParameter(2, fechaFinal);

			setLstViajes(query.list());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		} 
	}

	private String queryBuscaViajesRolPeriodo() {
		return "select v from recaudacion.bean.entidades.ccuentas.Viajes v where v.rol.rolId = ? and v.fecha BETWEEN ?  and  ?  ";
	}

	public void eliminaViajes(Integer marcaRolId, Date fecInicial,Date fecFinal) throws SQLException {

		
		String query = "DELETE FROM CCUENTAS.VIAJES WHERE MARCAROL_ID = ? AND FECHA BETWEEN ? AND ?";
		try {
			PreparedStatement ps = getConexion().prepareStatement(query);
			
			ps.setInt(1, marcaRolId);
			ps.setTimestamp(2, new Timestamp(fecInicial.getTime()));
			ps.setTimestamp(3, new Timestamp(fecFinal.getTime()));
			
			ps.executeUpdate();
			
		} catch (SQLException e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
		
		
	
}

	public void actualizaEstatusViajes() throws Exception {
		try {
			String query = getQueryActualizaEstatus();

			PreparedStatement ps = getConexion().prepareStatement(query);
			ps.executeUpdate();
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	private String getQueryActualizaEstatus() {
		return "UPDATE CCUENTAS.VIAJES SET STAUS_VIAJE = "+getEstatusViaje()+"  WHERE VIAJES_ID IN ("+getIdViajes()+")";
	}

	public void consultaViajesPorId(String ids) {
		setLstViajes(new ArrayList<Viajes>());
		try {
			String hql = queryconsultaViajesPorId(ids);
			Query<Viajes> query = getDb().createQuery(hql,Viajes.class);
			setLstViajes(query.list());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		} 
	}
	
	/**
	 * Realiza consulta de Oferta existente en viajes
	 * @param MarcaRolId 
	 * @param fecInical 
	 * @param fecfinal
	 * @return boolean - Si existen viajes True, Si no existen False 
	 * @throws SQLException 
	 * 
	 */
	public boolean consultaOfertaDisponible(Integer marcaRolId, Date fecInicial,Date fecFinal) throws SQLException 
	{
		
		String query = "SELECT * FROM CCUENTAS.VIAJES WHERE MARCAROL_ID = ? AND FECHA BETWEEN ? AND ?";
		try {
			PreparedStatement ps = getConexion().prepareStatement(query);
			
			ps.setInt(1, marcaRolId);
			ps.setTimestamp(2, new Timestamp(fecInicial.getTime()));
			ps.setTimestamp(3, new Timestamp(fecFinal.getTime()));
			
			ResultSet rs = ps.executeQuery();
			return rs.next();
			
		} catch (SQLException e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}
	
	
	private String queryconsultaViajesPorId(String ids) {
		return "select x from recaudacion.bean.entidades.ccuentas.Viajes x where x.viajeId in ("+ids+")";
	}

	public Vector<Object> getViajes() {
		return viajes;
	}

	public void setViajes(Vector<Object> viajes) {
		this.viajes = viajes;
	}

	public List<Viajes> getLstViajes() {
		return lstViajes;
	}

	public void setLstViajes(List<Viajes> lstViajes) {
		this.lstViajes = lstViajes;
	}

	public Viajes getViaje() {
		return viaje;
	}

	public void setViaje(Viajes viaje) {
		this.viaje = viaje;
	}

	public Integer getIdViaje() {
		return idViaje;
	}

	public void setIdViaje(Integer idViaje) {
		this.idViaje = idViaje;
	}

	public String getIdViajes() {
		return idViajes;
	}

	public void setIdViajes(String idViajes) {
		this.idViajes = idViajes;
	}

	public Integer getEstatusViaje() {
		return estatusViaje;
	}

	public void setEstatusViaje(Integer estatusViaje) {
		this.estatusViaje = estatusViaje;
	}
}
