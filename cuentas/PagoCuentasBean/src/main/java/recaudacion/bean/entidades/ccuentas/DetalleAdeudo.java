package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(schema="CCUENTAS", name="DETALLE_ADEUDO")
public class DetalleAdeudo implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id	
	@SequenceGenerator(name="DETALLEADEUDOID", sequenceName="CCUENTAS.DETALLE_ADEUDO_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DETALLEADEUDOID")
	@Column(name="DETALLEADEUDO_ID")
	private Integer detalleAdeudoId;
	
	@ManyToOne
	@JoinColumn(name="ADEUDO_ID")
	private Adeudo adeudo;
	
	@Column(name="NUMERO_PERIODO")
	private Integer numeroPeriodo;
	
	@Column(name="IMPORTE_PERIODO")
	private Double importePeriodo;
	
	@Column(name="FECHA_PERIODO")
	private Date fechaPeriodo;
	
	@Column(name="FECHA_PAGADO")
	private Date fechaPagado;
	
	@Column(name="IMPORTE_PAGADO")
	private Double importePagado;
	
	@Column(name="ESTATUS")
	private Integer estatus;
	
	@Column(name="FECHORACT")
	private Date fecHorAct;
	
	@Column(name="USUARIO_ID")
	private Integer usuarioId;

	public Integer getDetalleAdeudoId() {
		return detalleAdeudoId;
	}

	public void setDetalleAdeudoId(Integer detalleAdeudoId) {
		this.detalleAdeudoId = detalleAdeudoId;
	}

	public Adeudo getAdeudo() {
		return adeudo;
	}

	public void setAdeudo(Adeudo adeudo) {
		this.adeudo = adeudo;
	}

	public Integer getNumeroPeriodo() {
		return numeroPeriodo;
	}

	public void setNumeroPeriodo(Integer numeroPeriodo) {
		this.numeroPeriodo = numeroPeriodo;
	}

	public Double getImportePeriodo() {
		return importePeriodo;
	}

	public void setImportePeriodo(Double importePeriodo) {
		this.importePeriodo = importePeriodo;
	}

	public Date getFechaPeriodo() {
		return fechaPeriodo;
	}

	public void setFechaPeriodo(Date fechaPeriodo) {
		this.fechaPeriodo = fechaPeriodo;
	}

	public Date getFechaPagado() {
		return fechaPagado;
	}

	public void setFechaPagado(Date fechaPagado) {
		this.fechaPagado = fechaPagado;
	}

	public Double getImportePagado() {
		return importePagado;
	}

	public void setImportePagado(Double importePagado) {
		this.importePagado = importePagado;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public Date getFecHorAct() {
		return fecHorAct;
	}

	public void setFecHorAct(Date fecHorAct) {
		this.fecHorAct = fecHorAct;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}
}