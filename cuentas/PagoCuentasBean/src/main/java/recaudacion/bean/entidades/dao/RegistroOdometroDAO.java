package recaudacion.bean.entidades.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Date;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.RegistroOdometro;

public class RegistroOdometroDAO extends DAO {
	
	private RegistroOdometro odometro = null;
	
	private Integer kilometros = null;
	
	
	public void insertaOdometro() throws Exception {
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		
		Integer id = null;
		try {
			String query1 = "SELECT CCUENTAS.REGISTRO_ODOMETRO_SEQ.nextval from dual";
			ps2 = getConexion().prepareStatement(query1);
			ResultSet rs = ps2.executeQuery();
			while(rs.next()){
				id = new Integer(rs.getInt("NEXTVAL"));
			}
			
			String  query2 = getQueryOdometro();
			ps1 = getConexion().prepareStatement(query2);
			ps1.setObject(1, id);
			ps1.setObject(2, getOdometro().getAutobus()== null ? null : getOdometro().getAutobus().getIdAutobus());
			ps1.setObject(3, getOdometro().getAutobusExterno() == null ? null : getOdometro().getAutobusExterno().getIdAutobus());
			ps1.setObject(4, getOdometro().getKilometros());
			ps1.setObject(5, getOdometro().getConductor().getIdEmpleado());
			ps1.setTimestamp(6, new Timestamp(getOdometro().getFecHorAct().getTime()));
			ps1.setObject(7, getOdometro().getUsuarioId());
			ps1.execute();
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}
	
	private String getQueryOdometro() {
		return "	 INSERT  INTO CCUENTAS.REGISTRO_ODOMETRO "
				+ "  ( REGISTROODOMETRO_ID, AUTOBUS_ID, AUTOBUSEXT_ID, KILOMETROS, CONDUCTOR_ID, FECHORACT, USUARIO_ID ) "
				+ "  VALUES ( ?, ?, ?, ?, ?, ?, ? ) ";
	}

	

	public void buscaUltimoOdometro(Integer idEmpleado, Integer idAutobus, Date fecha) {
		try {
			String hql = getQueryBuscaUltimoOdometro();
			Query<Integer> query = getDb().createQuery(hql,Integer.class);
			query.setParameter(0, idEmpleado);
			query.setParameter(1, idAutobus);
			query.setParameter(2, idAutobus);
			setKilometros((Integer) query.uniqueResult());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}

	private String getQueryBuscaUltimoOdometro() {
		return "select  max(e.kilometros) from recaudacion.bean.entidades.ccuentas.RegistroOdometro e where e.conductor.idEmpleado = ?  and (e.autobus.idAutobus = ? or e.autobusExterno.idAutobus = ? ) order by e.fecHorAct ";
	}
	
	public RegistroOdometro buscaUltimoOdometroAutobus(Integer idEmpleado, Integer idAutobus) {
		RegistroOdometro odometro = null;
		try {
			PreparedStatement ps1 = null;
			ResultSet rs = null;
				String  sql = getQueryUltimoOdometro();
				ps1 = getConexion().prepareStatement(sql);
				ps1.setObject(1, idEmpleado);
				ps1.setObject(2, idAutobus);
				ps1.setObject(3, idAutobus);
				rs = ps1.executeQuery();
				while(rs.next()){
					odometro = new RegistroOdometro();
					odometro.setKilometros(new Integer(rs.getInt("MAXKILOMETROS")));
					odometro.setRegistroOdometroId(new Integer(rs.getInt("REGISTROODOMETRO_ID")));
				}

		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		} 
		return odometro;
	}

	private String getQueryUltimoOdometro() {
		return "	 SELECT MAX(REGISTRO_ODOMETRO.KILOMETROS) AS MAXKILOMETROS, REGISTRO_ODOMETRO.REGISTROODOMETRO_ID  FROM CCUENTAS.REGISTRO_ODOMETRO"
				+ "	 WHERE REGISTRO_ODOMETRO.CONDUCTOR_ID = ? "
				+ "	 AND (REGISTRO_ODOMETRO.AUTOBUS_ID = ? OR REGISTRO_ODOMETRO.AUTOBUSEXT_ID = ? )"
				+ "	 GROUP BY REGISTRO_ODOMETRO.REGISTROODOMETRO_ID, FECHORACT"
				+ "	 ORDER BY REGISTRO_ODOMETRO.FECHORACT";
	}
	

	public RegistroOdometro getOdometro() {
		return odometro;
	}

	public void setOdometro(RegistroOdometro odometro) {
		this.odometro = odometro;
	}

	public Integer getKilometros() {
		return kilometros;
	}

	public void setKilometros(Integer kilometros) {
		this.kilometros = kilometros;
	}
}
