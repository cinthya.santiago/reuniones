package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="TARJETA_VIAJE",schema="CCUENTAS")
public class TarjetaViaje implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TARJETA_VIAJE_ID")
	@SequenceGenerator(name="TARJETA_VIAJE_ID", sequenceName="CCUENTAS.TARJETA_VIAJE_SEQ", allocationSize=1)
	@Column(name="TARJETA_VIAJE_ID")
	private Integer idTarjetaViaje;
	
	@Column(name="TARJETA_CUENTA_ID")
	private Integer tarjetaCuentaId;
	
	@Column(name="VIAJE_ID")
	private Integer viajeId;
	
	@Column(name="FECHA")
	private Date fecha;
	
	@Column(name="HORA_SALIDA")
	private Date horaSalida;
	
	@Column(name="AUTOBUS_ID")
	private Integer autobusId;
	
	@Column(name="AUTOBUSEXT_ID")
	private Integer autobusExternoId;
	
	@Column(name="COSTO_VIAJE")
	private Double costoViaje;
	
	@Column(name="MANCUERNA")
	private Integer cuadro;
	
	@Column(name="SECUENCIA")
	private String secuencia;
	
	@Column(name="IMPORTE_CANCELA")
	private Double importeCancela;
	
	@Column(name="IMPORTE_AJUSTE")
	private Double importeAjuste;
	
	@Column(name="FECHA_EMISION")
	private Date fechaEmision;
	
	@Column(name="FECHA_CANCELA")
	private Date fechaCancela;
	
	@Column(name="FECHA_AJUSTA")
	private Date fechaAjusta;
	
	@Column(name="FECHA_RECAUDA")
	private Date fechaRecauda;
	
	@Column(name="ESTATUS")
	private Integer estatus;
	
	@Column(name="COSTO_PISTA")
	private Double costoPista;
	
	@Column(name="KILOMETROS")
	private Integer kilometros;

	public Integer getIdTarjetaViaje() {
		return idTarjetaViaje;
	}

	public void setIdTarjetaViaje(Integer idTarjetaViaje) {
		this.idTarjetaViaje = idTarjetaViaje;
	}

	public Integer getTarjetaCuentaId() {
		return tarjetaCuentaId;
	}

	public void setTarjetaCuentaId(Integer tarjetaCuentaId) {
		this.tarjetaCuentaId = tarjetaCuentaId;
	}

	public Integer getViajeId() {
		return viajeId;
	}

	public void setViajeId(Integer viajeId) {
		this.viajeId = viajeId;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Date getHoraSalida() {
		return horaSalida;
	}

	public void setHoraSalida(Date horaSalida) {
		this.horaSalida = horaSalida;
	}

	public Integer getAutobusId() {
		return autobusId;
	}

	public void setAutobusId(Integer autobusId) {
		this.autobusId = autobusId;
	}

	public Integer getAutobusExternoId() {
		return autobusExternoId;
	}

	public void setAutobusExternoId(Integer autobusExternoId) {
		this.autobusExternoId = autobusExternoId;
	}

	public Double getCostoViaje() {
		return costoViaje;
	}

	public void setCostoViaje(Double costoViaje) {
		this.costoViaje = costoViaje;
	}

	public Integer getCuadro() {
		return cuadro;
	}

	public void setCuadro(Integer cuadro) {
		this.cuadro = cuadro;
	}

	public String getSecuencia() {
		return secuencia;
	}

	public void setSecuencia(String secuencia) {
		this.secuencia = secuencia;
	}

	public Double getImporteCancela() {
		return importeCancela;
	}

	public void setImporteCancela(Double importeCancela) {
		this.importeCancela = importeCancela;
	}

	public Double getImporteAjuste() {
		return importeAjuste;
	}

	public void setImporteAjuste(Double importeAjuste) {
		this.importeAjuste = importeAjuste;
	}

	public Date getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public Date getFechaCancela() {
		return fechaCancela;
	}

	public void setFechaCancela(Date fechaCancela) {
		this.fechaCancela = fechaCancela;
	}

	public Date getFechaAjusta() {
		return fechaAjusta;
	}

	public void setFechaAjusta(Date fechaAjusta) {
		this.fechaAjusta = fechaAjusta;
	}

	public Date getFechaRecauda() {
		return fechaRecauda;
	}

	public void setFechaRecauda(Date fechaRecauda) {
		this.fechaRecauda = fechaRecauda;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public Double getCostoPista() {
		return costoPista;
	}

	public void setCostoPista(Double costoPista) {
		this.costoPista = costoPista;
	}

	public Integer getKilometros() {
		return kilometros;
	}

	public void setKilometros(Integer kilometros) {
		this.kilometros = kilometros;
	}
	
}
