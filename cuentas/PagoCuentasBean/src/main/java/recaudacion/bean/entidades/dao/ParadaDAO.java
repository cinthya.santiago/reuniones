package recaudacion.bean.entidades.dao;

import java.util.List;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.general.Parada;

public class ParadaDAO extends DAO{

	private List<Parada> lstParadas = null;
	private String ids = null;
	
	
	public void buscaParadasXId() {
		try {
			String hql = getQueryBuscaParadas();
			Query<Parada> query = getDb().createQuery(hql,Parada.class);
			setLstParadas(query.list());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	private String getQueryBuscaParadas() {
		return "Select c from recaudacion.bean.entidades.general.Parada c where cveParada in ("+getIds()+") ";
	}
	
	public Parada buscaParadaXCve(String cveParada)throws Exception {
		String consulta = getQueryParadaXCve();
		Query<Parada> query = getDb().createQuery(consulta,Parada.class);
		query.setParameter(0, cveParada);
		return query.uniqueResultOptional().get();
	}
	
	
	private String getQueryParadaXCve() 
	{
		return "Select c from recaudacion.bean.entidades.general.Parada c where cveParada = ?";
	}

	public void buscaParadasActivas() {
		try {
			String hql = getQueryBuscaParadasActivas();
			Query<Parada> query = getDb().createQuery(hql,Parada.class);
			setLstParadas(query.list());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}
	private String getQueryBuscaParadasActivas() {
		return "Select c from recaudacion.bean.entidades.general.Parada c where c.status = 1 ";
	}
	
	public void buscaParadas(){
		try {
			String hql = getQueryBuscaTodasParadas();
			Query<Parada> query= getDb().createQuery(hql,Parada.class);
			setLstParadas(query.list());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}
	
	private String getQueryBuscaTodasParadas(){ 
		return " Select c from recaudacion.bean.entidades.general.Parada c ";
	}
	
	
	
	public List<Parada> getLstParadas() {
		return lstParadas;
	}

	public void setLstParadas(List<Parada> lstParadas) {
		this.lstParadas = lstParadas;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}
}
