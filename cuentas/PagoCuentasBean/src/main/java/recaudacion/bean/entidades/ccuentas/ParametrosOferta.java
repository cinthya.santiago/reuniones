package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;
import java.util.Vector;

public class ParametrosOferta implements Serializable
{
	private static final long serialVersionUID = -8596339784661668769L;
	private MarcaRol marcaRol;
	private Integer numCuadros;
	private Integer numVueltas;
	private Integer tipoOferta;
	private String descTipoOferta;
	private Vector<Object> lstHorarios = null;
	
	public MarcaRol getMarcaRol() {
		return marcaRol;
	}
	public void setMarcaRol(MarcaRol marcaRol) {
		this.marcaRol = marcaRol;
	}
	public Integer getNumCuadros() {
		return numCuadros;
	}
	public void setNumCuadros(Integer numCuadros) {
		this.numCuadros = numCuadros;
	}
	public Integer getNumVueltas() {
		return numVueltas;
	}
	public void setNumVueltas(Integer numVueltas) {
		this.numVueltas = numVueltas;
	}
	public Integer getTipoOferta() {
		return tipoOferta;
	}
	public void setTipoOferta(Integer tipoOferta) {
		this.tipoOferta = tipoOferta;
	}
	public String getDescTipoOferta() {
		return descTipoOferta;
	}
	public void setDescTipoOferta(String descTipoOferta) {
		this.descTipoOferta = descTipoOferta;
	}
	
	public Vector<Object> getAbc()
	{
		Vector<Object> lstABC = new Vector<>();
		
		lstABC.add("");
		lstABC.add("A");
		lstABC.add("B");
		lstABC.add("C");
		lstABC.add("D");
		lstABC.add("E");
		lstABC.add("F");
		lstABC.add("G");
		lstABC.add("H");
		lstABC.add("I");
		lstABC.add("J");
		lstABC.add("K");
		lstABC.add("L");
		lstABC.add("M");
		lstABC.add("N");
		lstABC.add("O");
		lstABC.add("P");
		lstABC.add("Q");
		lstABC.add("R");
		lstABC.add("S");
		lstABC.add("T");
		lstABC.add("U");
		lstABC.add("V");
		lstABC.add("W");
		lstABC.add("X");
		lstABC.add("Y");
		lstABC.add("Z");
		return lstABC;
	}
	public Vector<Object> getLstHorarios() {
		return lstHorarios;
	}
	public void setLstHorarios(Vector<Object> lstHorarios) {
		this.lstHorarios = lstHorarios;
	}
}
