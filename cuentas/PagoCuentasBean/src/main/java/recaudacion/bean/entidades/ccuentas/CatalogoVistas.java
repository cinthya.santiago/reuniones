package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(schema = "CCUENTAS", name="CATALOGO_VISTAS_CUENTAS")
public class CatalogoVistas implements Serializable
{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator ="CATALOGOVISTASCUENTASID")
	@SequenceGenerator(name="CATALOGOVISTASCUENTASID", sequenceName="CCUENTAS.CATALOGO_VISTAS_SEQ", allocationSize = 1)
	@Column(name = "CATALOGO_VISTAS_CUENTAS_ID")
	private Integer catalogoVistaId;
	
	@Column(name="DESCVISTA")
	private String descVista;
	
	@Column(name="ESQUEMA")
	private String esquema;
     
	@Column(name="NOMBREVISTA")
	private String nombreVista;
	
	@Column(name="ESTATUS")
	private Integer estatus;

	@Transient
	private ArrayList<Object> camposVista = null;
	
	public Integer getCatalogoVistaId() {
		return catalogoVistaId;
	}

	public void setCatalogoVistaId(Integer catalogoVistaId) {
		this.catalogoVistaId = catalogoVistaId;
	}

	public String getDescVista() {
		return descVista;
	}

	public void setDescVista(String descVista) {
		this.descVista = descVista;
	}

	public String getEsquema() {
		return esquema;
	}

	public void setEsquema(String esquema) {
		this.esquema = esquema;
	}

	public String getNombreVista() {
		return nombreVista;
	}

	public void setNombreVista(String nombreVista) {
		this.nombreVista = nombreVista;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public ArrayList<Object> getCamposVista() {
		return camposVista;
	}

	public void setCamposVista(ArrayList<Object> camposVista) {
		this.camposVista = camposVista;
	}
}
