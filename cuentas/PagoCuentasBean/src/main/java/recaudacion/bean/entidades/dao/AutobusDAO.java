package recaudacion.bean.entidades.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.general.Autobus;

public class AutobusDAO extends DAO {

	private List<Autobus> lstAutobus = null;

	private String ids = null;
	
	private Autobus autobus = null;
	
	public void buscaAutobusesXId() {
		setLstAutobus(new ArrayList<Autobus>());

		try {

			String hql = getQueryBuscaAutobusesXIds();
			Query<Autobus> query = getDb().createQuery(hql,Autobus.class);
			setLstAutobus(query.list());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		} 
	}

	private String getQueryBuscaAutobusesXIds() {
		return "select e from recaudacion.bean.entidades.general.Autobus e where numeroEconomico in ("+getIds()+") ";
	}

	public void buscaAutobusesXClave(String clave, Integer marcaId) {
		try {

			String hql = getQueryBuscaAutobusesXClave(marcaId);
			Query<Autobus> query = getDb().createQuery(hql,Autobus.class);
			query.setParameter(0, clave);
			if(marcaId != null){
				query.setParameter(1, marcaId);
			}
			setAutobus((Autobus)query.uniqueResult());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		} 
	}

	private String getQueryBuscaAutobusesXClave(Integer marcaId) {
		String sql =  "select e from recaudacion.bean.entidades.general.Autobus e where numeroEconomico like ? ";
		if(marcaId != null){
			sql += "and marcaId = ? ";
		}
		return sql;
	}
	
	public List<Autobus> getLstAutobus() {
		return lstAutobus;
	}

	public void setLstAutobus(List<Autobus> lstAutobus) {
		this.lstAutobus = lstAutobus;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public Autobus getAutobus() {
		return autobus;
	}

	public void setAutobus(Autobus autobus) {
		this.autobus = autobus;
	}
}
