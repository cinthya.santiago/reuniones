package recaudacion.bean.entidades.dao;

import java.util.List;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.MarcaRol;

public class MarcaRolDAO extends DAO {
	List<MarcaRol> lstMarcaRol = null;
	private MarcaRol marcaRol = null;
	private String descripciones = null;

	public List<MarcaRol> buscaMarcaRol() {
		try {

			String hql = queryBuscaMarcaRol();

			Query<MarcaRol> query = getDb().createQuery(hql,MarcaRol.class);
			return query.list();

		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	private String queryBuscaMarcaRol() {
		return "select x from recaudacion.bean.entidades.ccuentas.MarcaRol x where  x.estatus = 1";
	}

	public List<MarcaRol> buscaMarcasDescripcion() {
		try {
			String hql = getQueryBuscaMarcaRolDescripcion();
			Query<MarcaRol> query = getDb().createQuery(hql,MarcaRol.class);
			return query.list();
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	public String getQueryBuscaMarcaRolDescripcion() {
		return "select x from recaudacion.bean.entidades.ccuentas.MarcaRol x  where x.descripcionRol in ("
				+ getDescripciones().trim() + ") and x.estatus = 1";
	}

	public MarcaRol recuperaMarcaRolPorDescripcion(String descripcion) {
		try {
			String hql = recuperaMarcaRolXDescripcion(descripcion);
			Query<MarcaRol> query = getDb().createQuery(hql,MarcaRol.class);
			MarcaRol rol = (MarcaRol)query.uniqueResult();
			return rol;
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}

	public String recuperaMarcaRolXDescripcion(String descripcion){
		return "select x from recaudacion.bean.entidades.ccuentas.MarcaRol x  where x.descripcionRol = '"+descripcion +"'" ;
	}

	public List<MarcaRol> buscaMarcaRolPorMarca(Integer idMarca)
	{
		try {
			String hql = queryBuscaRolMarcaPorMarca();
			Query<MarcaRol> query = getDb().createQuery(hql,MarcaRol.class);
			query.setParameter(0, idMarca);
			return query.list();
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}

	private String queryBuscaRolMarcaPorMarca() {
		return "select x from recaudacion.bean.entidades.ccuentas.MarcaRol x where x.marca.id = ? and x.estatus = 1";
	}

	
	public void eliminaRolMarca() {
		try {
			getDb().delete(getMarcaRol());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	public List<MarcaRol> getLstMarcaRol() {
		return lstMarcaRol;
	}

	public void setLstMarcaRol(List<MarcaRol> lstMarcaRol) {
		this.lstMarcaRol = lstMarcaRol;
	}

	public MarcaRol getMarcaRol() {
		return marcaRol;
	}

	public void setMarcaRol(MarcaRol marcaRol) {
		this.marcaRol = marcaRol;
	}

	public String getDescripciones() {
		return descripciones;
	}

	public void setDescripciones(String descripciones) {
		this.descripciones = descripciones;
	}
}
