package recaudacion.bean.entidades.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.Rol;

public class RolDAO extends DAO {
	private List<Rol> lstRol = null;
	private Rol rol = null;
	private Integer rolId = null;

	public void guardaRol() {
		try {
			getDb().saveOrUpdate(getRol());
			setRolId(getRol().getRolId());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}

	public Rol buscaRolId(Integer idMarca, Integer idMarcaRol, Date fecha) {
		try {

			String hql = queryBuscaRol();

			Query<Rol> query = getDb().createQuery(hql,Rol.class);
			query.setParameter(0, idMarca);
			query.setParameter(1, idMarcaRol);
			query.setParameter(2, fecha);
			setRol((Rol) query.uniqueResult());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
		return getRol();
	}

	private String queryBuscaRol() {
		return "select x from recaudacion.bean.entidades.ccuentas.Rol x where x.marcaId = ? and x.marcaRolId = ? and ? BETWEEN x.periodoInicial and x.periodoFinal ";
	}

	public Rol recuperaRolPorFecha(Integer idMarca, Integer idMarcaRolId, Date fechaIni, Date fechaFin) {
		try {
			String hql = queryBUscaRolFechas();
			Query<Rol> query = getDb().createQuery(hql,Rol.class);
			query.setParameter(0, idMarca);
			query.setParameter(1, idMarcaRolId);
			query.setParameter(2, fechaIni);
			query.setParameter(3, fechaFin);
			query.setParameter(4, fechaIni);
			query.setParameter(5, fechaFin);

			setRol((Rol) query.uniqueResult());

		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
		return getRol();
	}

	private String queryBUscaRolFechas() {
		return "select x from recaudacion.bean.entidades.ccuentas.Rol x where x.marcaId = ? and x.marcaRolId = ? "
				+ "  and (x.periodoInicial between ? and ? " + "	  or  x.periodoFinal between ? and ? )";
	}

	public List<Rol> getLstRol() {
		return lstRol;
	}

	public void setLstRol(List<Rol> lstRol) {
		this.lstRol = lstRol;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public Integer getRolId() {
		return rolId;
	}

	public void setRolId(Integer rolId) {
		this.rolId = rolId;
	}
}