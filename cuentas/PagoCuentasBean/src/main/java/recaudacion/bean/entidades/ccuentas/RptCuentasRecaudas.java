package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;

public class RptCuentasRecaudas implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private Integer cantidadBoletos = null;
	private Double totalBoletos = null;
	private Integer cantidadPistas = null;
	private Double totalPistas = null;
	private Double totalCuentas = null;
	private Integer cantidadDepositos = null;
	private Double totalDepositos = null;
	private Double totalAdeudos = null;
	private Double totalTarjetas = null;
	
	public Integer getCantidadBoletos() {
		return cantidadBoletos;
	}
	public void setCantidadBoletos(Integer cantidadBoletos) {
		this.cantidadBoletos = cantidadBoletos;
	}
	public Double getTotalBoletos() {
		return totalBoletos;
	}
	public void setTotalBoletos(Double totalBoletos) {
		this.totalBoletos = totalBoletos;
	}
	public Integer getCantidadPistas() {
		return cantidadPistas;
	}
	public void setCantidadPistas(Integer cantidadPistas) {
		this.cantidadPistas = cantidadPistas;
	}
	public Double getTotalPistas() {
		return totalPistas;
	}
	public void setTotalPistas(Double totalPistas) {
		this.totalPistas = totalPistas;
	}
	public Double getTotalCuentas() {
		return totalCuentas;
	}
	public void setTotalCuentas(Double totalCuentas) {
		this.totalCuentas = totalCuentas;
	}
	public Integer getCantidadDepositos() {
		return cantidadDepositos;
	}
	public void setCantidadDepositos(Integer cantidadDepositos) {
		this.cantidadDepositos = cantidadDepositos;
	}
	public Double getTotalDepositos() {
		return totalDepositos;
	}
	public void setTotalDepositos(Double totalDepositos) {
		this.totalDepositos = totalDepositos;
	}
	public Double getTotalAdeudos() {
		return totalAdeudos;
	}
	public void setTotalAdeudos(Double totalAdeudos) {
		this.totalAdeudos = totalAdeudos;
	}
	public Double getTotalTarjetas() {
		return totalTarjetas;
	}
	public void setTotalTarjetas(Double totalTarjetas) {
		this.totalTarjetas = totalTarjetas;
	}
}
