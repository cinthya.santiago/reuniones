package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;

public class CampoVista implements Serializable
{
	private static final long serialVersionUID = 1L;
	private int escala = 0;
	private int presicion = 0;
	private String nombCampo = "";
	private String typeClassName = "";

	private String andOr = "";
	private String clausula = "";
	private String clausulaEspanol = "";
	private Object valor = null;
	private Object valor2 = null;
	
	public int getEscala() {
		return escala;
	}
	public void setEscala(int escala) {
		this.escala = escala;
	}
	public int getPresicion() {
		return presicion;
	}
	public void setPresicion(int presicion) {
		this.presicion = presicion;
	}
	public String getNombCampo() {
		return nombCampo;
	}
	public void setNombCampo(String nombCampo) {
		this.nombCampo = nombCampo;
	}
	public String getTypeClassName() {
		return typeClassName;
	}
	public void setTypeClassName(String typeClassName) {
		this.typeClassName = typeClassName;
	}
	public String getAndOr() {
		return andOr;
	}
	public void setAndOr(String andOr) {
		this.andOr = andOr;
	}
	public String getClausula() {
		return clausula;
	}
	public void setClausula(String clausula) {
		this.clausula = clausula;
	}
	public String getClausulaEspanol() {
		return clausulaEspanol;
	}
	public void setClausulaEspanol(String clausulaEspanol) {
		this.clausulaEspanol = clausulaEspanol;
	}
	public Object getValor() {
		return valor;
	}
	public void setValor(Object valor) {
		this.valor = valor;
	}
	public Object getValor2() {
		return valor2;
	}
	public void setValor2(Object valor2) {
		this.valor2 = valor2;
	}
}
