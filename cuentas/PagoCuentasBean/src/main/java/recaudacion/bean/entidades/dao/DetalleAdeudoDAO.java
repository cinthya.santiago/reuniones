package recaudacion.bean.entidades.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.DetalleAdeudo;

public class DetalleAdeudoDAO extends DAO{

	private List<DetalleAdeudo> lstDetalles = null;
	
	public void guardaLstDetalleAdeudo(List<DetalleAdeudo> lstDetalle) throws Exception{

		try {
			Iterator<?> it = lstDetalle.iterator();
			while(it.hasNext()){
				DetalleAdeudo detalle = (DetalleAdeudo) it.next();
				guarda(detalle);
			}
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}
	
	public void consultaDetalleAdeudo(String idAdeudos){
		setLstDetalles(new ArrayList<>());
		try {
			String hql = queryBuscaDetalleAdeudo(idAdeudos);
			Query<DetalleAdeudo> query = getDb().createQuery(hql,DetalleAdeudo.class);
			setLstDetalles(query.list());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}
	
	public void actualizaDetalleAdeudo(DetalleAdeudo detalle){

		try {
			getDb().saveOrUpdate(detalle);
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	private String queryBuscaDetalleAdeudo(String idAdeudos) {
		return "select x from recaudacion.bean.entidades.ccuentas.DetalleAdeudo x where x.adeudo in ( "+idAdeudos+" )  and x.estatus = 1";
	}

	public List<DetalleAdeudo> getLstDetalles() {
		return lstDetalles;
	}

	public void setLstDetalles(List<DetalleAdeudo> lstDetalles) {
		this.lstDetalles = lstDetalles;
	}
}
