package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(schema="CCUENTAS", name="CONCEPTO_ADEUDO")
public class ConceptoAdeudo implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id	
	@SequenceGenerator(name="CONCEPTOADEUDOID", sequenceName="CCUENTAS.CONCEPTO_ADEUDO_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CONCEPTOADEUDOID")
	@Column(name="CONCEPTOADEUDO_ID")
	private Integer conceptoAdeudoId;
	
	@Column(name="DESCRIPCION_ADEUDO")
	private String descripcionAdeudo;
	
	@Column(name="STATUS")
	private Integer estatus;
	
	@Column(name="FECHORACT")
	private Date fecHorAct;
	
	@Column(name="USUARIO_ID")
	private Integer usuarioId;

	public Integer getConceptoAdeudoId() {
		return conceptoAdeudoId;
	}

	public void setConceptoAdeudoId(Integer conceptoAdeudoId) {
		this.conceptoAdeudoId = conceptoAdeudoId;
	}

	public String getDescripcionAdeudo() {
		return descripcionAdeudo;
	}

	public void setDescripcionAdeudo(String descripcionAdeudo) {
		this.descripcionAdeudo = descripcionAdeudo;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public Date getFecHorAct() {
		return fecHorAct;
	}

	public void setFecHorAct(Date fecHorAct) {
		this.fecHorAct = fecHorAct;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}
}