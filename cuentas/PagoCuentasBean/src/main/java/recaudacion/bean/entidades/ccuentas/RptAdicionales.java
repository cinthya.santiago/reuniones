package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;
import java.util.Date;

public class RptAdicionales implements Serializable
{
	
	private static final long serialVersionUID = 1L;
	
	private Integer folio = null;
	private Object cantidad = null;
	private Double importe = null;
	private Date fecVenta = null;
	
	public Integer getFolio() {
		return folio;
	}
	public void setFolio(Integer folio) {
		this.folio = folio;
	}
	public Object getCantidad() {
		return cantidad;
	}
	public void setCantidad(Object cantidad) {
		this.cantidad = cantidad;
	}
	public Double getImporte() {
		return importe;
	}
	public void setImporte(Double importe) {
		this.importe = importe;
	}
	public Date getFecVenta() {
		return fecVenta;
	}
	public void setFecVenta(Date fecVenta) {
		this.fecVenta = fecVenta;
	}
}
