package recaudacion.bean.entidades.dao;

import java.util.List;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.general.Zona;

public class ZonaDAO extends DAO {
	private List<Zona> lstZona = null;
	private Integer marcaId = null;

	public List<Zona> buscaZonaMarcaActivas() {
		try {

			String hql = getQueryBuscaZonaMarcaActivas();
			Query<Zona> query = getDb().createQuery(hql,Zona.class);
			query.setParameter(0, getMarcaId());
			setLstZona(query.list());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		} 
		return getLstZona();
	}

	public String getQueryBuscaZonaMarcaActivas() {
		return "select x.zona from recaudacion.bean.entidades.general.ZonaMarca x  where x.zona.status = 1  and x.marcaId.id = ? order by x.zona.descZona asc";
	}

	public List<Zona> getLstZona() {
		return lstZona;
	}

	public void setLstZona(List<Zona> lstZona) {
		this.lstZona = lstZona;
	}

	public Integer getMarcaId() {
		return marcaId;
	}

	public void setMarcaId(Integer marcaId) {
		this.marcaId = marcaId;
	}
}
