package recaudacion.bean.entidades.dao;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.general.Usuario;

public class UsuarioDAO extends DAO{

	public Usuario buscaUsuarioPorClave(String clave) {
		Usuario usuario = null;
		try {
			String hql = getQueryBuscUsuarioXClave();
			Query<Usuario> query = getDb().createQuery(hql,Usuario.class);
			query.setParameter(0, clave);
			usuario = (Usuario) query.uniqueResult();
			return usuario ;
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}
	
	private String getQueryBuscUsuarioXClave() {
		return "Select c from recaudacion.bean.entidades.general.Usuario c where c.empleado.cveEmpleado like ? ";
	}
}
