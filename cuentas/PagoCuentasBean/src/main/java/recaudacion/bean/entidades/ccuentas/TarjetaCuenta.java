package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import recaudacion.bean.entidades.general.Autobus;
import recaudacion.bean.entidades.general.Empleado;


@Entity
@Table(name="TARJETA_CUENTA", schema="CCUENTAS")
public class TarjetaCuenta implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator ="TARJETA_CUENTA_ID")
	@SequenceGenerator(name="TARJETA_CUENTA_ID", sequenceName="CCUENTAS.TARJETA_CUENTA_SEQ", allocationSize = 1)
	@Column(name = "TARJETA_CUENTA_ID")
	private Integer idTarjetaCuenta = 0;

	@Column(name = "ROL_ID")
	private Integer idRol;
	
	@ManyToOne
	@JoinColumn(name="CONDUCTOR_ID")
	private Empleado conductor;
	
	@ManyToOne
	@JoinColumn(name="AUTOBUS_ID")
	private Autobus autobus;
	
	@ManyToOne
	@JoinColumn(name="AUTOBUSEXT_ID")
	private AutobusExterno autobusExterno;
	
	@Column(name = "REGION_ID")
	private Integer regionId;
	
	@Column(name = "MARCA_ID")
	private Integer marcaId;
	
	@Column(name = "ZONA_ID")
	private Integer zonaId;
	
	@Column(name = "IMPORTE_CUENTA")
	private Double importeCuenta;
	
	@Column(name = "IMPORTE_CANCELA")
	private Double importeCancela;
	
	@Column(name = "IMPORTE_AJUSTA")
	private Double importeAjusta;
	
	@Column(name = "VALOR_TARJETA")
	private Double valorTarjeta;
	
	@Column(name = "INGRESO_MANUAL")
	private Double ingresoManual;
	
	@Column(name = "FECHA_EMISION")
	private Date fechaEmision;
	
	@Column(name = "FECHA_CANCELA")
	private Date fechaCancela;
	
	@Column(name = "FECHA_AJUSTA")
	private Date fechaAjusta;
	
	@Column(name = "FECHA_RECAUDA")
	private Date fechaRecauda;
	
	@Column(name = "ESTATUS")
	private Integer estatus;
	
	@Column(name = "USUARIOCREA_ID")
	private Integer usuarioCreaId;
	
	@Column(name = "USUARIOCANCELA_ID")
	private Integer usuarioCancelaId;
	
	@Column(name = "USUARIOAJUSTA_ID")
	private Integer usuarioAjustaId;
	
	@Column(name = "USUARIORECAUDA_ID")
	private Integer usuarioRecaudaId;
	
	@Column(name = "TOTAL_PISTAS")
	private Double totalPistas;

	@Column(name = "CANTIDAD_BOLETOS")
	private Integer numeroBoletos;
	
	@Column(name = "COSTO_LAVADO")
	private Double costoLavado;
	
	@Column(name = "IMPORTE_RECAUDADO")
	private Double importeRecaudado;
	
	@Column(name = "IMPORTE_DEPOSITO")
	private Double importeDeposito;
	
	
	@Transient
	private Set<TarjetaViaje> tarjetasViaje;
	
	public Set<TarjetaViaje> getTarjetasViaje() {
		return tarjetasViaje;
	}

	public void setTarjetasViaje(Set<TarjetaViaje> viajes) {
		this.tarjetasViaje = viajes;
	}


	public Integer getIdTarjetaCuenta() {
		return idTarjetaCuenta;
	}

	public void setIdTarjetaCuenta(Integer idTarjetaCuenta) {
		this.idTarjetaCuenta = idTarjetaCuenta;
	}

	public Integer getIdRol() {
		return idRol;
	}

	public void setIdRol(Integer idRol) {
		this.idRol = idRol;
	}

	public Integer getRegionId() {
		return regionId;
	}

	public void setRegionId(Integer regionId) {
		this.regionId = regionId;
	}

	public Integer getMarcaId() {
		return marcaId;
	}

	public void setMarcaId(Integer marcaId) {
		this.marcaId = marcaId;
	}

	public Integer getZonaId() {
		return zonaId;
	}

	public void setZonaId(Integer zonaId) {
		this.zonaId = zonaId;
	}

	public Double getImporteCuenta() {
		return importeCuenta;
	}

	public void setImporteCuenta(Double importeCuenta) {
		this.importeCuenta = importeCuenta;
	}

	public Double getImporteCancela() {
		return importeCancela;
	}

	public void setImporteCancela(Double importeCancela) {
		this.importeCancela = importeCancela;
	}

	public Double getImporteAjusta() {
		return importeAjusta;
	}

	public void setImporteAjusta(Double importeAjusta) {
		this.importeAjusta = importeAjusta;
	}

	public Double getValorTarjeta() {
		return valorTarjeta;
	}

	public void setValorTarjeta(Double valorTarjeta) {
		this.valorTarjeta = valorTarjeta;
	}

	public Double getIngresoManual() {
		return ingresoManual;
	}

	public void setIngresoManual(Double ingresoManual) {
		this.ingresoManual = ingresoManual;
	}

	public Date getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public Date getFechaCancela() {
		return fechaCancela;
	}

	public void setFechaCancela(Date fechaCancela) {
		this.fechaCancela = fechaCancela;
	}

	public Date getFechaAjusta() {
		return fechaAjusta;
	}

	public void setFechaAjusta(Date fechaAjusta) {
		this.fechaAjusta = fechaAjusta;
	}

	public Date getFechaRecauda() {
		return fechaRecauda;
	}

	public void setFechaRecauda(Date fechaRecauda) {
		this.fechaRecauda = fechaRecauda;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public Integer getUsuarioCreaId() {
		return usuarioCreaId;
	}

	public void setUsuarioCreaId(Integer usuarioCreaId) {
		this.usuarioCreaId = usuarioCreaId;
	}

	public Integer getUsuarioCancelaId() {
		return usuarioCancelaId;
	}

	public void setUsuarioCancelaId(Integer usuarioCancelaId) {
		this.usuarioCancelaId = usuarioCancelaId;
	}

	public Integer getUsuarioAjustaId() {
		return usuarioAjustaId;
	}

	public void setUsuarioAjustaId(Integer usuarioAjustaId) {
		this.usuarioAjustaId = usuarioAjustaId;
	}

	public Integer getUsuarioRecaudaId() {
		return usuarioRecaudaId;
	}

	public void setUsuarioRecaudaId(Integer usuarioRecaudaId) {
		this.usuarioRecaudaId = usuarioRecaudaId;
	}

	public Double getTotalPistas() {
		return totalPistas;
	}

	public void setTotalPistas(Double totalPistas) {
		this.totalPistas = totalPistas;
	}

	public Empleado getConductor() {
		return conductor;
	}

	public void setConductor(Empleado conductor) {
		this.conductor = conductor;
	}

	public Autobus getAutobus() {
		return autobus;
	}

	public void setAutobus(Autobus autobus) {
		this.autobus = autobus;
	}

	public AutobusExterno getAutobusExterno() {
		return autobusExterno;
	}

	public void setAutobusExterno(AutobusExterno autobusExterno) {
		this.autobusExterno = autobusExterno;
	}

	public Integer getNumeroBoletos() {
		return numeroBoletos;
	}

	public void setNumeroBoletos(Integer numeroBoletos) {
		this.numeroBoletos = numeroBoletos;
	}

	public Double getCostoLavado() {
		return costoLavado;
	}

	public void setCostoLavado(Double costoLavado) {
		this.costoLavado = costoLavado;
	}

	public Double getImporteRecaudado() {
		return importeRecaudado;
	}

	public void setImporteRecaudado(Double importeRecaudado) {
		this.importeRecaudado = importeRecaudado;
	}

	public Double getImporteDeposito() {
		return importeDeposito;
	}

	public void setImporteDeposito(Double importeDeposito) {
		this.importeDeposito = importeDeposito;
	}

}
