package recaudacion.bean.entidades.general;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ZONA_MARCA", schema = "GENERAL")
public class ZonaMarca implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@OneToOne
	@JoinColumn(name = "ZONA_ID", nullable = false)
	private Zona zona;

	@Id
	@OneToOne
	@JoinColumn(name = "MARCA_ID", nullable = false)
	private Marca marcaId;

	public Zona getZona() {
		return zona;
	}

	public void setZonaId(Zona zona) {
		this.zona = zona;
	}

	public Marca getMarcaId() {
		return marcaId;
	}

	public void setMarcaId(Marca marcaId) {
		this.marcaId = marcaId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((marcaId == null) ? 0 : marcaId.hashCode());
		result = prime * result + ((zona == null) ? 0 : zona.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ZonaMarca other = (ZonaMarca) obj;
		if (marcaId == null) {
			if (other.marcaId != null)
				return false;
		} else if (!marcaId.equals(other.marcaId))
			return false;
		if (zona == null) {
			if (other.zona != null)
				return false;
		} else if (!zona.equals(other.zona))
			return false;
		return true;
	}
	
	
}
