package recaudacion.bean.entidades.dao;

import java.util.List;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.general.Empleado;

public class EmpleadoDAO extends DAO {
	
	private String ids = null;
	private List<Empleado> lstEmpleado = null;
	private Empleado empleado = null;

	public void buscaConductoresXId() {
		try {
			String hql = getQueryBuscConductoresXIds();
			Query<Empleado> query = getDb().createQuery(hql,Empleado.class);
			setLstEmpleado(query.list());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	private String getQueryBuscConductoresXIds() {
		return "Select c from recaudacion.bean.entidades.general.Empleado c where cveEmpleado in ("+getIds()+")";
	}
	
	public void buscaConductorPorClave(String clave, Integer marcaId) {
		try {
			String hql = getQueryBuscConductoresXClave(marcaId);
			Query<Empleado> query = getDb().createQuery(hql,Empleado.class);
			query.setParameter(0, clave);
			setEmpleado((Empleado) query.uniqueResult());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		} 
	}
	
	private String getQueryBuscConductoresXClave(Integer marcaId) {
		String sql =  "Select c from recaudacion.bean.entidades.general.Empleado c where cveEmpleado like ? ";
		if(marcaId != null){
			sql += "and marcaId = " + marcaId;
		}
		return sql;
	}

	public List<Empleado> getLstEmpleado() {
		return lstEmpleado;
	}

	public void setLstEmpleado(List<Empleado> lstEmpleado) {
		this.lstEmpleado = lstEmpleado;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}
}
