package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import recaudacion.bean.entidades.general.Autobus;
import recaudacion.bean.entidades.general.Empleado;

@Entity
@Table(name="VIAJE_RUTA", schema="CCUENTAS")
public class ViajeRuta implements Serializable
{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VIAJERUTA_ID")
	@SequenceGenerator(name="VIAJERUTA_ID", sequenceName="CCUENTAS.VIAJE_RUTA_SEQ", allocationSize=1)
	@Column(name="VIAJERUTA_ID")
	private Integer viajeRutaId = null;
	
	@ManyToOne
	@JoinColumn(name="CONDUCTOR1_ID")
	private Empleado conductor1 = null;
	
	@ManyToOne
	@JoinColumn(name="CONDUCTOR2_ID")
	private Empleado conductor2 = null;
	
	@ManyToOne
	@JoinColumn(name="AUTOBUS_ID")
	private Autobus autobus = null;
	
	@ManyToOne
	@JoinColumn(name="AUTOBUSEXT_ID")
	private AutobusExterno autobusExt = null;
	
	@Column(name="NOMBRE_CONTRATO")
	private String nombreContrato = null;
	
	@Column(name="TELEFONO_CONTRATANTE")
	private String telefonoContratante = null;
	
	@Column(name="CORREO_CONTRATANTE")
	private String correoContratante = null;
	
	@Column(name="FECHOR_INICIO")
	private Date fecHorInicio = null;
	
	@Column(name="FECHOR_FIN")
	private Date fecHorFinal = null;
	
	@Column(name="LUGAR")
	private String lugar = null;
	
	@Column(name="ORIGEN")
	private String origen = null;
	
	@Column(name="DESTINO")
	private String destino = null;
	
	@Column(name="KILOMETROS")
	private Integer kilometros = null;
	
	@Column(name="LITROS")
	private Integer litros = null;
	
	@Column(name="NUM_GUARDIAS")
	private Integer numGuardias = null;
	
	@Column(name="IMPORTE_GUARDIAS")
	private Double importeGuardias = null;
	
	@Column(name="IMPORTE_ALIMENTOS")
	private Double importeAlimentos = null;
	
	@Column(name="RAZON_SOCIAL")
	private String razonSocial = null;
	
	@Column(name="COMPANIA")
	private String compania = null;
	
	@Column(name="IMPORTE_SERVICIO")
	private Double importeServicio = null;
	
	@Column(name="SUELDO_CONDUCTOR")
	private Double sueldoConductor = null;
	
	@Column(name="STATUS")
	private Integer status = null;
	
	@Column(name="FECHA_ALTA")
	private Date fechaAlta = null;
	
	@Column(name="USUARIO_ALTA")
	private Integer usuarioAlta = null;

	@Column(name="OBSERVACIONES")
	private String observaciones = null;
	
	@Column(name="N_ASIENTOS")
	private Integer nAsientos = null;
	
	@Transient
	private Double importeXLitro = null;
	
	@Transient
	private Double total = null;
	
	public Integer getViajeRutaId() {
		return viajeRutaId;
	}

	public void setViajeRutaId(Integer viajeRutaId) {
		this.viajeRutaId = viajeRutaId;
	}

	public Empleado getConductor1() {
		return conductor1;
	}

	public void setConductor1(Empleado conductor1) {
		this.conductor1 = conductor1;
	}

	public Empleado getConductor2() {
		return conductor2;
	}

	public void setConductor2(Empleado conductor2) {
		this.conductor2 = conductor2;
	}

	public Autobus getAutobus() {
		return autobus;
	}

	public void setAutobus(Autobus autobus) {
		this.autobus = autobus;
	}

	public AutobusExterno getAutobusExt() {
		return autobusExt;
	}

	public void setAutobusExt(AutobusExterno autobusExt) {
		this.autobusExt = autobusExt;
	}

	public String getNombreContrato() {
		return nombreContrato;
	}

	public void setNombreContrato(String nombreContrato) {
		this.nombreContrato = nombreContrato;
	}

	public String getTelefonoContratante() {
		return telefonoContratante;
	}

	public void setTelefonoContratante(String telefonoContratante) {
		this.telefonoContratante = telefonoContratante;
	}

	public String getCorreoContratante() {
		return correoContratante;
	}

	public void setCorreoContratante(String correoContratante) {
		this.correoContratante = correoContratante;
	}

	public Date getFecHorInicio() {
		return fecHorInicio;
	}

	public void setFecHorInicio(Date fecHorInicio) {
		this.fecHorInicio = fecHorInicio;
	}

	public Date getFecHorFinal() {
		return fecHorFinal;
	}

	public void setFecHorFinal(Date fecHorFinal) {
		this.fecHorFinal = fecHorFinal;
	}

	public String getLugar() {
		return lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public Integer getKilometros() {
		return kilometros;
	}

	public void setKilometros(Integer kilometros) {
		this.kilometros = kilometros;
	}

	public Integer getLitros() {
		return litros;
	}

	public void setLitros(Integer litros) {
		this.litros = litros;
	}

	public Integer getNumGuardias() {
		return numGuardias;
	}

	public void setNumGuardias(Integer numGuardias) {
		this.numGuardias = numGuardias;
	}

	public Double getImporteGuardias() {
		return importeGuardias;
	}

	public void setImporteGuardias(Double importeGuardias) {
		this.importeGuardias = importeGuardias;
	}

	public Double getImporteAlimentos() {
		return importeAlimentos;
	}

	public void setImporteAlimentos(Double importeAlimentos) {
		this.importeAlimentos = importeAlimentos;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getCompania() {
		return compania;
	}

	public void setCompania(String compania) {
		this.compania = compania;
	}

	public Double getImporteServicio() {
		return importeServicio;
	}

	public void setImporteServicio(Double importeServicio) {
		this.importeServicio = importeServicio;
	}

	public Double getSueldoConductor() {
		return sueldoConductor;
	}

	public void setSueldoConductor(Double sueldoConductor) {
		this.sueldoConductor = sueldoConductor;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Integer getUsuarioAlta() {
		return usuarioAlta;
	}

	public void setUsuarioAlta(Integer usuarioAlta) {
		this.usuarioAlta = usuarioAlta;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Integer getNAsientos() {
		return nAsientos;
	}

	public void setNAsientos(Integer nAsientos) {
		this.nAsientos = nAsientos;
	}

	public Integer getnAsientos() {
		return nAsientos;
	}

	public void setnAsientos(Integer nAsientos) {
		this.nAsientos = nAsientos;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Double getImporteXLitro() {
		return importeXLitro;
	}

	public void setImporteXLitro(Double importeXLitro) {
		this.importeXLitro = importeXLitro;
	}

}