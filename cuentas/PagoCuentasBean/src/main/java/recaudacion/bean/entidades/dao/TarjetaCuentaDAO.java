package recaudacion.bean.entidades.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.Rol;
import recaudacion.bean.entidades.ccuentas.TarjetaCuenta;
import recaudacion.bean.entidades.general.Autobus;
import recaudacion.bean.entidades.general.Empleado;
import recaudacion.bean.serv.inter.Constantes;

public class TarjetaCuentaDAO extends DAO {

	private TarjetaCuenta tarjetaCuenta = null;
	private Integer idTarjeta = null;
	private List<TarjetaCuenta> lstTarjetas = null;

	public void consultaTarjetCuentaDia(Integer conductorId, Date fechaEmision) {
		try {
			String hql = queryBuscaTarjetaCuentaDia();
			Query<TarjetaCuenta> query = getDb().createQuery(hql,TarjetaCuenta.class);
			query.setParameter(0, conductorId);
			query.setParameter(1, fechaEmision);
			setTarjetaCuenta((TarjetaCuenta) query.uniqueResult());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}

	private String queryBuscaTarjetaCuentaDia() {
		return "select x from recaudacion.bean.entidades.ccuentas.TarjetaCuenta x where x.conductor.idEmpleado = ? and x.fechaEmision = ?  ";
	}

	public void insertaTarjetaCuenta() throws Exception {
		try {
			PreparedStatement ps1 = null;
			PreparedStatement ps2 = null;

			Integer id = null;
				String query1 = "SELECT CCUENTAS.TARJETA_CUENTA_SEQ.NEXTVAL FROM DUAL";
				ps2 = getConexion().prepareStatement(query1);
				ResultSet rs = ps2.executeQuery();
				while (rs.next()) {
					id = new Integer(rs.getInt("NEXTVAL"));
				}

				String query2 = getQueryInsertaTarjeta();
				ps1 = getConexion().prepareStatement(query2);
				ps1.setObject(1, id);
				ps1.setObject(2, getTarjetaCuenta().getIdRol());
				ps1.setObject(3, getTarjetaCuenta().getConductor() == null ? null : getTarjetaCuenta().getConductor().getIdEmpleado());
				ps1.setObject(4, getTarjetaCuenta().getAutobus() == null ? null : getTarjetaCuenta().getAutobus().getIdAutobus());
				ps1.setObject(5, getTarjetaCuenta().getAutobusExterno() == null ? null
						: getTarjetaCuenta().getAutobusExterno().getIdAutobus());
				ps1.setObject(6, getTarjetaCuenta().getRegionId());
				ps1.setObject(7, getTarjetaCuenta().getMarcaId());
				ps1.setObject(8, getTarjetaCuenta().getZonaId());
				ps1.setObject(9, getTarjetaCuenta().getImporteCuenta());
				ps1.setTimestamp(10, new Timestamp(getTarjetaCuenta().getFechaEmision().getTime()));
				ps1.setObject(11, getTarjetaCuenta().getEstatus());
				ps1.setObject(12, getTarjetaCuenta().getUsuarioCreaId());
				ps1.setObject(13, getTarjetaCuenta().getValorTarjeta());
				ps1.execute();
				setIdTarjeta(id);
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	private String getQueryInsertaTarjeta() {
		return "	INSERT INTO CCUENTAS.TARJETA_CUENTA ("
				+ "		TARJETA_CUENTA_ID, ROL_ID, CONDUCTOR_ID, AUTOBUS_ID, AUTOBUSEXT_ID, REGION_ID, "
				+ "		MARCA_ID, ZONA_ID, IMPORTE_CUENTA, FECHA_EMISION, ESTATUS, USUARIOCREA_ID, VALOR_TARJETA )"
				+ "		VALUES( ?, ?,?,?,?,?,?,?,?,?,?,?, ?)";
	}
	

	public void recuperaTarjetaCuentaXIdViaje(Integer idCuenta) {
		try {
			String hql = queryBuscaTarjetaCuentaXIdViaje();
			Query<TarjetaCuenta> query = getDb().createQuery(hql,TarjetaCuenta.class);
			query.setParameter(0, idCuenta);
			setTarjetaCuenta((TarjetaCuenta) query.uniqueResult());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	private String queryBuscaTarjetaCuentaXIdViaje() {
		return "select x from recaudacion.bean.entidades.ccuentas.TarjetaCuenta x where x.idTarjetaCuenta = ? ";
	}

	public void recuperaTarjetasPorConductor(Integer idConductor) {
		try {
			String hql = queryBuscaTarjetasPorConductor();
			Query<TarjetaCuenta> query = getDb().createQuery(hql,TarjetaCuenta.class);
			query.setParameter(0, idConductor);
			setLstTarjetas(query.list());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}
	
	public Integer consultaTotalCuentasConductor(Integer conductorId){
		Long total = null;
		try {
			String hql = queryBuscaTotalCuentasConductor();
			Query<Long> query = getDb().createQuery(hql,Long.class);
			query.setParameter(0, conductorId);
			total = (Long) query.uniqueResult();
			return total.intValue();
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}
	
	public List<TarjetaCuenta> buscaCuentasRecaudadas(Rol rol,Date fecInicial,Date fecFinal,Integer idRecaudador) throws Exception
	{
		List<TarjetaCuenta> cuentas = new ArrayList<TarjetaCuenta>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		int i = 3;
		try {
			ps = getConexion().prepareStatement(queryBuscaCtasRecaudadas(rol,idRecaudador));
			ps.setTimestamp(1, new Timestamp(fecInicial.getTime()));
			ps.setTimestamp(2, new Timestamp(fecFinal.getTime()));
			if(idRecaudador != null){
				ps.setObject(i, idRecaudador);
				i++;
			}
			if(rol != null){
				ps.setObject(i, rol.getRolId());
			}
			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				TarjetaCuenta tarjeta = new TarjetaCuenta();
				Autobus bus = new Autobus();
				bus.setIdAutobus(new Integer(rs.getInt("AUTOBUS_ID")));
				bus.setNumeroEconomico(rs.getString("UNIDAD"));
				Empleado empleado = new Empleado();
				empleado.setIdEmpleado(new Integer(rs.getInt("CONDUCTOR_ID")));
				empleado.setClave(rs.getString("CLAVE"));
				empleado.setNombre(rs.getString("CONDUCTOR"));
				
				tarjeta.setAutobus(bus);
				tarjeta.setConductor(empleado);
				tarjeta.setFechaEmision(rs.getDate("FECHA_EMITIDO"));
				tarjeta.setImporteRecaudado(rs.getDouble("IMPORTE_EMITIDO"));
				tarjeta.setValorTarjeta(rs.getDouble("CUOTA_EMITIDA"));
				tarjeta.setFechaRecauda(rs.getDate("FECHA_RECAUDADO"));
				tarjeta.setImporteRecaudado(rs.getDouble("IMPORTE_RECAUDADO"));
				cuentas.add(tarjeta);
			}
			return cuentas;
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}
	
	
	private String queryBuscaCtasRecaudadas(Rol rol, Integer idrecaudador) {
		String query = "SELECT	 TC.AUTOBUS_ID, A.NUMECONOMICO AS UNIDAD, TC.CONDUCTOR_ID, E.CVEEMPLEADO AS CLAVE,	(E.NOMBRE||' '||E.PATERNO||' '||E.MATERNO) AS CONDUCTOR,"
				+ "				TC.FECHA_EMISION AS FECHA_EMITIDO,	TC.IMPORTE_RECAUDADO AS IMPORTE_EMITIDO,	TC.VALOR_TARJETA AS CUOTA_EMITIDA,"
				+ "				TC.FECHA_RECAUDA AS FECHA_RECAUDADO,	TC.IMPORTE_RECAUDADO AS IMPORTE_RECAUDADO"
				+ "		FROM	CCUENTAS.TARJETA_CUENTA TC, GENERAL.AUTOBUS A,  GENERAL.EMPLEADO E"
				+ "		WHERE TC.AUTOBUS_ID = A.AUTOBUS_ID"
				+ "		AND TC.CONDUCTOR_ID = E.EMPLEADO_ID"
				+ "		AND ESTATUS = 3"
				+ "		AND TC.FECHA_RECAUDA BETWEEN ? AND ? ";
					if(idrecaudador != null){
						query += "	AND TC.USUARIORECAUDA_ID = ?";
					}
					if(rol != null){
						query += "	AND ROL_ID = ? ";
					}
		return query;
	}
	private String queryBuscaTotalCuentasConductor(){ 
		return "select count(x.idTarjetaCuenta) from recaudacion.bean.entidades.ccuentas.TarjetaCuenta x where x.conductor.idEmpleado = ?  and x.estatus != " + Constantes.ESTATUS_CTAS_RECAUDADA;
	}

	private String queryBuscaTarjetasPorConductor() {
		return "select x from recaudacion.bean.entidades.ccuentas.TarjetaCuenta x where x.conductor.idEmpleado = ? and x.estatus = 1";
	}

	public TarjetaCuenta getTarjetaCuenta() {
		return tarjetaCuenta;
	}

	public void setTarjetaCuenta(TarjetaCuenta tarjetaCuenta) {
		this.tarjetaCuenta = tarjetaCuenta;
	}

	public Integer getIdTarjeta() {
		return idTarjeta;
	}

	public void setIdTarjeta(Integer idTarjeta) {
		this.idTarjeta = idTarjeta;
	}

	public List<TarjetaCuenta> getLstTarjetas() {
		return lstTarjetas;
	}

	public void setLstTarjetas(List<TarjetaCuenta> lstTarjetas) {
		this.lstTarjetas = lstTarjetas;
	}
}
