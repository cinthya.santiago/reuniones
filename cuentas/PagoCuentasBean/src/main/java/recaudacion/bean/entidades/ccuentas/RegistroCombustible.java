package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import recaudacion.bean.entidades.general.Autobus;
import recaudacion.bean.entidades.general.Empleado;
import recaudacion.bean.entidades.general.Marca;

@Entity
@Table(name="REGISTRO_COMBUSTIBLES", schema="CCUENTAS")
public class RegistroCombustible implements Serializable
{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="REGISTROCOMBUSTIBLE_ID")
	@SequenceGenerator(name="REGISTROCOMBUSTIBLE_ID", sequenceName="CCUENTAS.REGISTRO_COMBUSTIBLES_SEQ", allocationSize=1)
	@Column(name="REGISTROCOMBUSTIBLE_ID")
	private Integer registroCombustibleId = null;
	
	@ManyToOne
	@JoinColumn(name="AUTOBUS_ID")
	private Autobus autobus = null;
	
	@ManyToOne
	@JoinColumn(name="AUTOBUSEXT_ID")
	private AutobusExterno autobusExt = null;
	
	@Column(name="LITROS")
	private Integer litros = null;
	
	@Column(name="TOTAL")
	private Double total = null;
	
	@ManyToOne
	@JoinColumn(name="CONDUCTOR_ID")
	private Empleado conductor = null;
	
	@Column(name="FECHORACT")
	private Date fecHorAct = null;
	
	@Column(name="FECHRECAUDA")
	private Date fechRecauda = null;
	
	@Column(name="USUARIORECAUDA_ID")
	private Integer usuarioRecaudaId = null;
	
	@Column(name="ESTATUS")
	private Integer status = null;
	
	@ManyToOne
	@JoinColumn(name="ISLACOMBUSTIBLE_ID")
	private IslaCombustible islaCombustible = null;
	
	@Column(name="TIPO_CARGO")
	private Integer tipoCargo = null;

	@ManyToOne
	@JoinColumn(name="MARCA_ID")
	private Marca marca;
	
	@Column(name="IMPORTEXLITRO")
	private Double importeXLitro;
	
	public Integer getRegistroCombustibleId() {
		return registroCombustibleId;
	}

	public void setRegistroCombustibleId(Integer registroCombustibleId) {
		this.registroCombustibleId = registroCombustibleId;
	}

	public Autobus getAutobus() {
		return autobus;
	}

	public void setAutobus(Autobus autobus) {
		this.autobus = autobus;
	}

	public AutobusExterno getAutobusExt() {
		return autobusExt;
	}

	public void setAutobusExt(AutobusExterno autobusExt) {
		this.autobusExt = autobusExt;
	}

	public Integer getLitros() {
		return litros;
	}

	public void setLitros(Integer litros) {
		this.litros = litros;
	}

	public Empleado getConductor() {
		return conductor;
	}

	public void setConductor(Empleado conductor) {
		this.conductor = conductor;
	}

	public Date getFecHorAct() {
		return fecHorAct;
	}

	public void setFecHorAct(Date fecHorAct) {
		this.fecHorAct = fecHorAct;
	}

	public Date getFechRecauda() {
		return fechRecauda;
	}

	public void setFechRecauda(Date fechRecauda) {
		this.fechRecauda = fechRecauda;
	}

	public Integer getUsuarioRecaudaId() {
		return usuarioRecaudaId;
	}

	public void setUsuarioRecaudaId(Integer usuarioRecaudaId) {
		this.usuarioRecaudaId = usuarioRecaudaId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public IslaCombustible getIslaCombustible() {
		return islaCombustible;
	}

	public void setIslaCombustible(IslaCombustible islaCombustible) {
		this.islaCombustible = islaCombustible;
	}

	public Integer getTipoCargo() {
		return tipoCargo;
	}

	public void setTipoCargo(Integer tipoCargo) {
		this.tipoCargo = tipoCargo;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public Double getImporteXLitro() {
		return importeXLitro;
	}

	public void setImporteXLitro(Double importeXLitro) {
		this.importeXLitro = importeXLitro;
	}
	
}