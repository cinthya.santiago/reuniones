package recaudacion.bean.entidades.dao;

import java.sql.PreparedStatement;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.DetalleTramo;

public class DetalleTramoDAO extends DAO {

	public List<DetalleTramo> buscaTodosDetalleTramos() {
		List<DetalleTramo> lstDetalles = null;
		try {

			String hql = buscaTodosDetallesTramo();
			Query<DetalleTramo> query = getDb().createQuery(hql, DetalleTramo.class);
			lstDetalles = query.list();

		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
		return lstDetalles;
	}

	private String buscaTodosDetallesTramo() {
		return "select d from recaudacion.bean.entidades.ccuentas.DetalleTramo d";
	}

	public List<DetalleTramo> buscaDetalleTramosXTramoXTramo(Integer idTramo) {
		try {
			String hql = getQueryBuscaDetalleTramos(idTramo);
			Query<DetalleTramo> query = getDb().createQuery(hql, DetalleTramo.class);
			return query.list();
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}

	private String getQueryBuscaDetalleTramos(Integer idTramo) {
		return "select d from recaudacion.bean.entidades.ccuentas.DetalleTramo d where d.key.tramoId = " + idTramo;
	}

	public void guardaDetalle(Vector<Object> lstDetalles) throws Exception {
		try {

			Iterator<?> it = lstDetalles.iterator();
			while (it.hasNext()) {
				DetalleTramo detalle = (DetalleTramo) it.next();
				guarda(detalle);
			}
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	public void eliminaSecuenciaTramo(Integer idTramo) throws Exception {
		PreparedStatement ps1 = null;
		try {
			String query1 = "DELETE FROM CCUENTAS.DETALLE_TRAMO WHERE TRAMO_ID = " + idTramo;
			ps1 = getConexion().prepareStatement(query1);
			ps1.execute();

		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	

}
