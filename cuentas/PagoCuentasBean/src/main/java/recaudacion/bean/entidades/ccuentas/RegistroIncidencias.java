package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import recaudacion.bean.entidades.general.Autobus;
import recaudacion.bean.entidades.general.Empleado;

@Entity
@Table(name="REGISTRO_INCIDENCIAS", schema="CCUENTAS")
public class RegistroIncidencias implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id	
	@SequenceGenerator(name="INCIDENCIAID", sequenceName="CCUENTAS.REGISTRO_INCIDENCIAS_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="INCIDENCIAID")
	@Column(name="REGISTROINCIDENCIAS_ID")
	private Integer incidenciaId;
	
	@ManyToOne
	@JoinColumn(name="CONDUCTOR_ID")
	private Empleado conductor;
	
	@ManyToOne
	@JoinColumn(name="AUTOBUS_ID")
	private Autobus autobus;
	
	@ManyToOne
	@JoinColumn(name="AUTOBUSEXT_ID")
	private AutobusExterno autobusExterno;
	
	@ManyToOne
	@JoinColumn(name="MARCAROL_ID")
	private MarcaRol marcaRol;
	
	@Column(name="FECHA_INGRESO")
	private Date fechaIngreso;
	
	@Column(name="FECHA_SALIDA")
	private Date fechaSalida;
	
	@Column(name="ESTATUS")
	private Integer estatus;
	
	@Column(name="FECHORACT")
	private Date fecHorAct;
	
	@Column(name="USUARIO_ID")
	private Integer usuarioId;

	public Integer getIncidenciaId() {
		return incidenciaId;
	}

	public void setIncidenciaId(Integer incidenciaId) {
		this.incidenciaId = incidenciaId;
	}

	public Empleado getConductor() {
		return conductor;
	}

	public void setConductor(Empleado conductor) {
		this.conductor = conductor;
	}

	public Autobus getAutobus() {
		return autobus;
	}

	public void setAutobus(Autobus autobus) {
		this.autobus = autobus;
	}

	public AutobusExterno getAutobusExterno() {
		return autobusExterno;
	}

	public void setAutobusExterno(AutobusExterno autobusExterno) {
		this.autobusExterno = autobusExterno;
	}

	public MarcaRol getMarcaRol() {
		return marcaRol;
	}

	public void setMarcaRol(MarcaRol marcaRol) {
		this.marcaRol = marcaRol;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public Date getFechaSalida() {
		return fechaSalida;
	}

	public void setFechaSalida(Date fechaSalida) {
		this.fechaSalida = fechaSalida;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public Date getFecHorAct() {
		return fecHorAct;
	}

	public void setFecHorAct(Date fecHorAct) {
		this.fecHorAct = fecHorAct;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}
	
	
}
