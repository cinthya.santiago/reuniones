package recaudacion.bean.entidades.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.DepositoEnCamino;
import recaudacion.bean.entidades.ccuentas.RptAdicionales;
import recaudacion.bean.entidades.ccuentas.TarjetaCuenta;
import recaudacion.bean.entidades.general.Parada;

public class DepositoEnCaminoDAO extends DAO {

	private List<DepositoEnCamino> lstDepositos = null;

	public void buscaDepositoConducto(Integer idConductor) {
		setLstDepositos(new ArrayList<>());
		try {
			String hql = queryBuscaDepositosConductor();
			Query<DepositoEnCamino> query = getDb().createQuery(hql,DepositoEnCamino.class);
			query.setParameter(0, idConductor);
			setLstDepositos(query.list());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	private String queryBuscaDepositosConductor() {
		return "select x from recaudacion.bean.entidades.ccuentas.DepositoEnCamino x where x.conductor.idEmpleado = ? ";
	}

	public void buscaDepositoTarjetaConducto(Integer idConductor, Integer tarjetaId) {
		setLstDepositos(new ArrayList<>());
		try {
			String hql = queryBuscaDepositosTarjetaConductor();
			Query<DepositoEnCamino> query = getDb().createQuery(hql,DepositoEnCamino.class);
			query.setParameter(0, idConductor);
			query.setParameter(1, tarjetaId);
			setLstDepositos(query.list());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	private String queryBuscaDepositosTarjetaConductor() {
		return 	"select x from recaudacion.bean.entidades.ccuentas.DepositoEnCamino x "+
				" where x.conductor.idEmpleado = ? "+
				" and x.tarjetaCuenta.idTarjetaCuenta = ? "+
				" and x.estatus = "+DepositoEnCamino.DEPOSITO_ACTIVO;
	}

	public void guardaDepositoPorLista(Vector<Object> depositos) throws Exception {
		try {

			Iterator<?> it = depositos.iterator();
			while(it.hasNext()){
				DepositoEnCamino deposito = (DepositoEnCamino) it.next();
				guarda(deposito);
			}
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}
	
	public List<DepositoEnCamino> recuperaTarjetasConductor(Integer conductorId) throws Exception { 
		PreparedStatement ps = null;
		ResultSet rs = null;
		setLstDepositos(new Vector<>());
		
		try {
			ps = getConexion().prepareStatement(queryTarjetasDepositos());
			ps.setInt(1, conductorId);
			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				DepositoEnCamino dp = new DepositoEnCamino();
				dp.setDepositoCaminoId(rs.getInt("DEPOSITOCAMINO_ID"));
				dp.setImporteDeposito(rs.getDouble("IMPORTE_DEPOSITO"));
				dp.setFechaDeposito(rs.getDate("FECHA_DEPOSITO"));
				dp.setUsuarioRecuada(rs.getInt("USUARIO_RECAUDA")); //CVEEMPLEADO
				dp.setCveUsuario(rs.getString("CVEUSUARIO"));
				dp.setEstatus(rs.getInt("ESTATUS"));
				
				TarjetaCuenta tarjeta = new TarjetaCuenta();
				Integer tarjetaId = new Integer(rs.getInt("TARJETA_CUENTA_ID"));
				tarjeta.setIdTarjetaCuenta(tarjetaId);
				tarjeta.setFechaEmision(rs.getDate("FECHA_EMISION"));
				tarjeta.setImporteCuenta(rs.getDouble("IMPORTE_CUENTA"));
				
				Parada parada = new Parada();
				parada.setIdParada(rs.getInt("PARADA_ID"));
				parada.setClave(rs.getString("CVEPARADA"));
				
				dp.setTarjetaCuenta(tarjeta);
				dp.setParada(parada);
				
				getLstDepositos().add(dp);
				
			}
			return getLstDepositos();
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}
	private String queryTarjetasDepositos() {
		String sql = "	SELECT DP.DEPOSITOCAMINO_ID, DP.IMPORTE_DEPOSITO, DP.FECHA_DEPOSITO, DP.PARADA_ID , P.CVEPARADA,"
				+ "		DP.USUARIO_RECAUDA, U.CVEUSUARIO, DP.ESTATUS, TC.TARJETA_CUENTA_ID, TC.FECHA_EMISION, TC.IMPORTE_CUENTA"
				+ "	 FROM CCUENTAS.DEPOSITO_EN_CAMINO DP, CCUENTAS.TARJETA_CUENTA TC, CCUENTAS.PARADA P, GENERAL.USUARIO U"
				+ "  WHERE DP.TARJETA_CUENTA_ID(+) = TC.TARJETA_CUENTA_ID"
				+ " AND DP.PARADA_ID = P.PARADA_ID(+)"
				+ " AND TC.CONDUCTOR_ID = ? " 
				+ " AND DP.USUARIO_RECAUDA = U.USUARIO_ID(+)"
				+ " AND TC.ESTATUS = 1"
				+ " ORDER BY  TC.FECHA_EMISION ASC";
		return sql;
	}


	
	public List<RptAdicionales> buscaTotalesDepositos(Date fecInicial,Date fecFinal,Integer recaudadorId) throws Exception {

		List<RptAdicionales> depositos = new ArrayList<RptAdicionales>();
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getConexion().prepareStatement(queryTotalesDepositosCamino(recaudadorId));
			ps.setTimestamp(1, new Timestamp(fecInicial.getTime()));
			ps.setTimestamp(2, new Timestamp(fecFinal.getTime()));
			if (recaudadorId != null) {
				ps.setObject(3, recaudadorId);
			}

			rs = ps.executeQuery();
			while (rs.next()) {
				RptAdicionales rpt = new RptAdicionales();
				rpt.setFolio(new Integer(rs.getInt("DEPOSITOCAMINO_ID")));
				rpt.setCantidad((rs.getObject("TERMINAL")));
				rpt.setImporte(new Double(rs.getDouble("IMPORTE_DEPOSITO")));
				depositos.add(rpt);
			}
			return depositos;
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}
	
	private String queryTotalesDepositosCamino(Integer recaudadorId) {
		String sql = "SELECT DP.DEPOSITOCAMINO_ID, (SELECT P.CVEPARADA FROM CCUENTAS.PARADA P WHERE P.PARADA_ID = DP.PARADA_ID)  AS TERMINAL, DP.IMPORTE_DEPOSITO"
				+ "		FROM CCUENTAS.DEPOSITO_EN_CAMINO DP" + "		WHERE DP.FECHORACT BETWEEN  ?  AND  ?  ";
		if (recaudadorId != null) {
			sql += "		AND DP.USUARIO_RECAUDA = ? ";
		}
		return sql;
	}

	public List<DepositoEnCamino> getLstDepositos() {
		return lstDepositos;
	}

	public void setLstDepositos(List<DepositoEnCamino> lstDepositos) {
		this.lstDepositos = lstDepositos;
	}
}