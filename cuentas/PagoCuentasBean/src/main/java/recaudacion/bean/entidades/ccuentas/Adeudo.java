package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import recaudacion.bean.entidades.general.Empleado;

@Entity
@Table(schema="CCUENTAS", name="ADEUDO")
public class Adeudo implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id	
	@SequenceGenerator(name="ADEUDOID", sequenceName="CCUENTAS.ADEUDO_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ADEUDOID")
	@Column(name="ADEUDO_ID")
	private Integer adeudoId;
	
	@ManyToOne
	@JoinColumn(name="CONCEPTOADEUDO_ID")
	private ConceptoAdeudo conceptoAdeudo;
	
	@ManyToOne
	@JoinColumn(name="CONDUCTOR_ID")
	private Empleado conductor;
	
	@Column(name="IMPORTE_ADEUDO")
	private Double importeAdeudo;
	
	@Column(name="NUMERO_PERIODOS")
	private Integer numeroPeriodos;
	
	@Column(name="FECHA_EMISION")
	private Date fechaEmision;
	
	@Column(name="STATUS")
	private Integer estatus;
	
	@Column(name="FECHORACT")
	private Date fecHorAct;
	
	@Column(name="USUARIO_ID")
	private Integer usuarioId;

	public Integer getAdeudoId() {
		return adeudoId;
	}

	public void setAdeudoId(Integer adeudoId) {
		this.adeudoId = adeudoId;
	}

	public Empleado getConductor() {
		return conductor;
	}

	public void setConductor(Empleado conductor) {
		this.conductor = conductor;
	}

	public Double getImporteAdeudo() {
		return importeAdeudo;
	}

	public void setImporteAdeudo(Double importeAdeudo) {
		this.importeAdeudo = importeAdeudo;
	}

	public Integer getNumeroPeriodos() {
		return numeroPeriodos;
	}

	public void setNumeroPeriodos(Integer numeroPeriodos) {
		this.numeroPeriodos = numeroPeriodos;
	}

	public Date getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public Date getFecHorAct() {
		return fecHorAct;
	}

	public void setFecHorAct(Date fecHorAct) {
		this.fecHorAct = fecHorAct;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	public ConceptoAdeudo getConceptoAdeudo() {
		return conceptoAdeudo;
	}

	public void setConceptoAdeudo(ConceptoAdeudo conceptoAdeudo) {
		this.conceptoAdeudo = conceptoAdeudo;
	}
}