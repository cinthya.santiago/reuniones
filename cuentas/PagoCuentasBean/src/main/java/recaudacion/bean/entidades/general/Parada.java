package recaudacion.bean.entidades.general;

import java.util.Date;
import java.util.Vector;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import asu.bean.entidades.dto.ObjetoGenerico;


@Entity
@Table(name="PARADA",schema="CCUENTAS")
public class Parada extends ObjetoGenerico{
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator ="PARADA_ID")
	@SequenceGenerator(name="PARADA_ID", sequenceName="CCUENTAS.PARADA_SEQ", allocationSize = 1)
	@Column(name="PARADA_ID")
	public Integer idParada;
	
	@Column(name="CVEPARADA")
	protected String cveParada;
	
	@Column(name="DESCPARADA")
	protected String descParada;
	
	@Column(name="STATUS")
	protected Integer status;
	
	@Column(name="FECHORACT")
	protected Date fecHorAct;
	
	@ManyToOne
	@JoinColumn(name="USUARIO_ID")
	protected Usuario usuario;
	
	public Integer getIdParada() {
		return idParada;
	}
	public void setIdParada(Integer idParada) {
		this.idParada = idParada;
	}
	public String getCveParada() {
		return cveParada;
	}
	public void setCveParada(String cveParada) {
		this.cveParada = cveParada;
	}
	public String getDescParada() {
		return descParada;
	}
	public void setDescParada(String descParada) {
		this.descParada = descParada;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	@Override
	public Integer getId()
	{
		return this.getIdParada();
	}
	@Override
	public void setId(Integer id)
	{
		this.setIdParada(id);
	}
	@Override
	public void setClave(String clave)
	{
		this.setCveParada(clave);
	}
	@Override
	public String getClave()
	{
		return this.getCveParada();
	}
	@Override
	public String getDescripcion()
	{
		return this.getDescParada();
	}
	@Override
	public void setDescripcion(String newDescripcion)
	{
		this.setDescParada(newDescripcion);
	}
	@Override
	public void getQueryPorId()
	{
	}
	@Override
	public void getQueryPorClave()
	{
		setQueryGenerico("select p from recaudacion.bean.entidades.general.Parada p where p.cveParada like ?");
		setParametros(new Vector<Object>());
		getParametros().add(getCveParada());
	}
	@Override
	public void getQueryPorDescripcion()
	{
		setQueryGenerico("select p from recaudacion.bean.entidades.general.Parada p where p.descParada like ? and status = 1 ");
		setParametros(new Vector<>());
		getParametros().add("%"+getDescParada()+"%");
	}
	public Date getFecHorAct() {
		return fecHorAct;
	}
	public void setFecHorAct(Date fecHorAct) {
		this.fecHorAct = fecHorAct;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
