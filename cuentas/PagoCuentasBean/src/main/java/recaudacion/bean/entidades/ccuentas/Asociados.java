package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import recaudacion.bean.entidades.general.Marca;

@Entity
@Table(name = "ASOCIADO", schema = "CCUENTAS")
public class Asociados implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "ASOCIADOID", sequenceName = "CCUENTAS.ASOCIADO_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ASOCIADOID")
	@Column(name="ASOCIADO_ID")
	private Integer asociadoId;

	@Column(name="NOMBRE")
	private String nombre;

	@Column(name="FECHORACT")
	private Date fechoract;

	@Column(name="USUARIO_ID")
	private Integer usuarioId;

	@Column(name="ESTATUS")
	private Integer estatus;

	@Column(name="CLAVEJDE")
	private String claveJde;
	
	@ManyToOne
	@JoinColumn(name="MARCA_ID")
	private Marca marca;

	public String getClaveJde() {
		return claveJde;
	}

	public void setClaveJde(String claveJde) {
		this.claveJde = claveJde;
	}

	public Integer getAsociadoId() {
		return asociadoId;
	}

	public void setAsociadoId(Integer asociadoId) {
		this.asociadoId = asociadoId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechoract() {
		return fechoract;
	}

	public void setFechoract(Date fechoract) {
		this.fechoract = fechoract;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}
	
//	public java.util.Vector<Object> toVector() {
//		java.util.Vector<Object> vector = new java.util.Vector<Object>();
//		vector.add(this);
//		vector.add(getClass());
//		vector.add(getNombre());
//		vector.add(getClaveJde());
//		vector.add(getEstatus() == null ? "Baja" : getEstatus().intValue() == 0 ? "Baja" : "Alta");
//		vector.add(getMarca());
//		return vector;
//	}
}
