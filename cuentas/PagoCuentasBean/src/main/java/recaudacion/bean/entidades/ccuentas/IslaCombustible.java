package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ISLA_COMBUSTIBLE", schema="CCUENTAS")
public class IslaCombustible implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ISLACOMBUSTIBLE_ID")
	private Integer islaCombustibleId = null;
	
	@Column(name="DESCRIPCION")
	private String descripcion = null;
	
	@Column(name="STATUS")
	private Integer status = null;
	
	@Column(name="FECHORACT")
	private Date fecHorAct = null;
	
	@Column(name="USUARIO_ID")
	private Integer usuarioId = null;

	public Integer getIslaCombustibleId() {
		return islaCombustibleId;
	}

	public void setIslaCombustibleId(Integer islaCombustibleId) {
		this.islaCombustibleId = islaCombustibleId;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getFecHorAct() {
		return fecHorAct;
	}

	public void setFecHorAct(Date fecHorAct) {
		this.fecHorAct = fecHorAct;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}
	
	public String toString() {
		return getDescripcion();
	}
	
}