package recaudacion.bean.entidades.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.RegistroCombustible;
import recaudacion.bean.entidades.ccuentas.RptAdicionales;

public class RegistroCombustibleDAO extends DAO {

	RegistroCombustible registroCombustible = null;

	public List<RegistroCombustible> buscaCombustibleConductor(Integer idConductor) {
		try {
			String hql = queryBuscaCombustibleConductor();
			Query<RegistroCombustible> query = getDb().createQuery(hql, RegistroCombustible.class);
			query.setParameter(0, idConductor);
			return query.list();
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}

	private String queryBuscaCombustibleConductor() {
		return "select x from RegistroCombustible x where x.conductor.idEmpleado = ? ";
	}

	public void guardaRegistroCombustiblePorLista(Vector<Object> lstCombustible) throws Exception {
		try {
			Iterator<?> it = lstCombustible.iterator();
			while (it.hasNext()) {
				RegistroCombustible combustible = (RegistroCombustible) it.next();
				guarda(combustible);
			}
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
	}

	public void eliminaRegistroCombustible(RegistroCombustible registroCombustible) {
		try {

			getDb().delete(registroCombustible);

		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		} 
	}

	public List<RptAdicionales> buscaTotalesVentaCombustible(Date fecInicial,Date fecFinal,Integer usuarioId) throws Exception {

		List<RptAdicionales> registros = new ArrayList<RptAdicionales>();
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getConexion().prepareStatement(queryTotalesCombustible(usuarioId));
			ps.setTimestamp(1, new Timestamp(fecInicial.getTime()));
			ps.setTimestamp(2, new Timestamp(fecFinal.getTime()));
			if (usuarioId != null) {
				ps.setObject(3, usuarioId);
			}
			rs = ps.executeQuery();

			while (rs.next()) {
				RptAdicionales rpt = new RptAdicionales();
				rpt.setFolio(new Integer(rs.getInt("FOLIO")));
				rpt.setCantidad(new Double(rs.getDouble("LITROS")));
				rpt.setImporte(new Double(rs.getDouble("TOTAL")));
				rpt.setFecVenta(rs.getDate("FECHRECAUDA"));
				registros.add(rpt);
			}
			return registros;
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
		
	}
	
	private String queryTotalesCombustible(Integer usuarioId) {
		String sql = "SELECT REGISTROCOMBUSTIBLE_ID AS FOLIO, LITROS, TOTAL, FECHRECAUDA "
				+ "		FROM  CCUENTAS.REGISTRO_COMBUSTIBLES" + "		WHERE FECHRECAUDA BETWEEN  ?  AND  ?  ";
		if (usuarioId != null) {
			sql += "	AND USUARIORECAUDA_ID = ? ";
		}
		sql += " ORDER BY FECHRECAUDA";
		return sql;
	}
	
	public RegistroCombustible getRegistroCombustible() {
		return registroCombustible;
	}

	public void setRegistroCombustible(RegistroCombustible registroCombustible) {
		this.registroCombustible = registroCombustible;
	}
}
