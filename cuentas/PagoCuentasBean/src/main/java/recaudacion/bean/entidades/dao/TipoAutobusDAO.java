package recaudacion.bean.entidades.dao;

import java.util.List;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.general.TipoAutobus;

public class TipoAutobusDAO extends DAO {
	private List<TipoAutobus> lstTipoAutobus=null;
	
	public List<TipoAutobus> buscaTipoAutobus() {
		try {

			String hql = getQueryBuscaTipoAutobus();
			Query<TipoAutobus> query = getDb().createQuery(hql,TipoAutobus.class);
			setLstTipoAutobus(query.list());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
		return getLstTipoAutobus();
	}

	public String getQueryBuscaTipoAutobus() {
		return "select x from recaudacion.bean.entidades.general.TipoAutobus x  where x.estatus = 1  and x.desctipoBus is not null order by x.desctipoBus asc";
	}

	public List<TipoAutobus> getLstTipoAutobus() {
		return lstTipoAutobus;
	}

	public void setLstTipoAutobus(List<TipoAutobus> lstTipoAutobus) {
		this.lstTipoAutobus = lstTipoAutobus;
	}
}
