package recaudacion.bean.entidades.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.ConceptoAdeudo;

public class ConceptoAdeudoDAO extends DAO {
	private List<ConceptoAdeudo> lstConceptoAdeudos = null;

	public void buscaConceptoAdeudosActivos() {
		setLstConceptoAdeudos(new ArrayList<>());
		try {
			String hql = queryBuscaConceptosActivos();
			Query<ConceptoAdeudo> query = getDb().createQuery(hql,ConceptoAdeudo.class);
			setLstConceptoAdeudos(query.list());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	private String queryBuscaConceptosActivos() {
		return "select x from recaudacion.bean.entidades.ccuentas.ConceptoAdeudo x where x.estatus = 1 ";
	}

	public List<ConceptoAdeudo> getLstConceptoAdeudos() {
		return lstConceptoAdeudos;
	}

	public void setLstConceptoAdeudos(List<ConceptoAdeudo> lstConceptoAdeudos) {
		this.lstConceptoAdeudos = lstConceptoAdeudos;
	}
}
