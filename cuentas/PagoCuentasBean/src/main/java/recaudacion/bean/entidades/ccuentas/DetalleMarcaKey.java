package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import recaudacion.bean.entidades.general.Marca;

@Embeddable
public class DetalleMarcaKey implements Serializable{

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name="MARCA_ID")
	private Marca marca;
	
	@ManyToOne
	@JoinColumn(name="MARCAROL_ID")
	private MarcaRol marcaRol;

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public MarcaRol getMarcaRol() {
		return marcaRol;
	}

	public void setMarcaRol(MarcaRol marcaRol) {
		this.marcaRol = marcaRol;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((marca == null) ? 0 : marca.hashCode());
		result = prime * result + ((marcaRol == null) ? 0 : marcaRol.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DetalleMarcaKey other = (DetalleMarcaKey) obj;
		if (marca == null) {
			if (other.marca != null)
				return false;
		} else if (!marca.equals(other.marca))
			return false;
		if (marcaRol == null) {
			if (other.marcaRol != null)
				return false;
		} else if (!marcaRol.equals(other.marcaRol))
			return false;
		return true;
	}
	
	
}
