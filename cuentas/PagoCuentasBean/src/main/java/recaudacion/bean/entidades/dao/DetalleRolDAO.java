package recaudacion.bean.entidades.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.DetalleRol;

public class DetalleRolDAO extends DAO
{
	private List<DetalleRol> lstDetalles = null;

	public void guardaRol() throws Exception {
		try {
			Iterator<?> it = getLstDetalles().iterator();
			while(it.hasNext()){
				DetalleRol detalle = (DetalleRol)it.next();
				guarda(detalle);
			}
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}


	public List<DetalleRol> getLstDetalles() {
		return lstDetalles;
	}

	public void setLstDetalles(List<DetalleRol> lstDetalles) {
		this.lstDetalles = lstDetalles;
	}


	public List<DetalleRol> buscaDetalleRol(Integer rolId) {
		setLstDetalles(new ArrayList<DetalleRol>());
		try {
			String hql = queryBuscaDetalleRol();
			Query<DetalleRol> query = getDb().createQuery(hql,DetalleRol.class);
			query.setParameter(0, rolId);
			return query.list();
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}


	private String queryBuscaDetalleRol() {
		return "select x from recaudacion.bean.entidades.ccuentas.DetalleRol x where x.rolId = ? ";
	}
}