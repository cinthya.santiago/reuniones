package recaudacion.bean.entidades.ccuentas;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DetalleTramoKey implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Column(name="TRAMO_ID")
	private Integer tramoId = null;
	
	@Column(name="DIASEMANA")
	private String diaSemana = null;
	
	@Column(name="INICIO")
	private Timestamp inicio = null;

	public Integer getTramoId() {
		return tramoId;
	}

	public void setTramoId(Integer tramoId) {
		this.tramoId = tramoId;
	}

	public String getDiaSemana() {
		return diaSemana;
	}

	public void setDiaSemana(String diaSemana) {
		this.diaSemana = diaSemana;
	}

	public Timestamp getInicio() {
		return inicio;
	}

	public void setInicio(Timestamp inicio) {
		this.inicio = inicio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((diaSemana == null) ? 0 : diaSemana.hashCode());
		result = prime * result + ((inicio == null) ? 0 : inicio.hashCode());
		result = prime * result + ((tramoId == null) ? 0 : tramoId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DetalleTramoKey other = (DetalleTramoKey) obj;
		if (diaSemana == null) {
			if (other.diaSemana != null)
				return false;
		} else if (!diaSemana.equals(other.diaSemana))
			return false;
		if (inicio == null) {
			if (other.inicio != null)
				return false;
		} else if (!inicio.equals(other.inicio))
			return false;
		if (tramoId == null) {
			if (other.tramoId != null)
				return false;
		} else if (!tramoId.equals(other.tramoId))
			return false;
		return true;
	}
	
	
}
