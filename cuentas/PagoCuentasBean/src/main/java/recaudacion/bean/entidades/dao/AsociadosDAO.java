package recaudacion.bean.entidades.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.List;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.Asociados;

@SuppressWarnings({ "unchecked", "rawtypes"})
public class AsociadosDAO extends DAO {
	private List<Asociados> lstAsociados = null;

	private String descripcion = null;

	
	public List<Asociados> buscaTodosAsociados() {
		try {

			String hql = getQueryBuscaTodosAsociados();
			Query query = getDb().createQuery(hql,Asociados.class);
			setLstAsociados(query.list());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
		return getLstAsociados();
	}

	public String getQueryBuscaTodosAsociados() {
		return "select e from recaudacion.bean.entidades.ccuentas.Asociados e  order by e.nombre";
	}
	
	public List<Asociados> buscaAsociadosActivos() {
		try {

			String hql = getQueryBuscaAsociadosActivos();
			if (getDescripcion() != null) {
				hql += getCondicionDescripcion();
			}
			Query query = getDb().createQuery(hql);
			setLstAsociados(query.list());
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
		return getLstAsociados();
	}

	public String getQueryBuscaAsociadosActivos() {
		return "select e from recaudacion.bean.entidades.ccuentas.Asociados e where e.estatus = 1  order by e.nombre asc";
	}

	public String getCondicionDescripcion() {
		return "  and e.nombre like '%" + getDescripcion() + "%'";
	}

	public void guardaAsociados(Asociados asociado) {
		try {
			
			if (asociado.getAsociadoId() == null) {
				guardaAsociadoNuevo(asociado);
			} else {
				getDb().saveOrUpdate(asociado);
			}

		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		} 
	}

	
	private void guardaAsociadoNuevo(Asociados asociado){ 
		try {

			PreparedStatement ps1 = null;
			PreparedStatement ps2 = null;
			Integer id = null;
			try {
				String query1 = "SELECT CCUENTAS.ASOCIADO_SEQ.nextval from dual";
				ps2 = getConexion().prepareStatement(query1);
				ResultSet rs = ps2.executeQuery();
				while(rs.next()){
					id = new Integer(rs.getInt("NEXTVAL"));
				}
				
				String  query2 = getQueryASociado();
				ps1 = getConexion().prepareStatement(query2);
				ps1.setObject(1, id);
				ps1.setObject(2, asociado.getNombre());
				ps1.setTimestamp(3, new Timestamp(asociado.getFechoract().getTime()));
				ps1.setObject(4, asociado.getUsuarioId());
				ps1.setObject(5, asociado.getEstatus());
				ps1.setObject(6, asociado.getClaveJde());
				ps1.setObject(7, asociado.getMarca().getId());
				ps1.execute();
			} catch (Exception e) {
				MensajesDebug.imprimeMensaje(e);
				throw e;
			} finally {
				try {
					cierraRecursos(ps1, null);
					cierraRecursos(ps2, null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		
			
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		} 
	}
	
	private String getQueryASociado() {
		return "	INSERT INTO CCUENTAS.ASOCIADO ( ASOCIADO_ID, NOMBRE, FECHORACT, USUARIO_ID, ESTATUS, CLAVEJDE, MARCA_ID)"
				+ "                        VALUES ( ?, ?, ?, ?, ?, ?, ? )";
	}
	
	public List<Asociados> getLstAsociados() {
		return lstAsociados;
	}

	public void setLstAsociados(List<Asociados> lstAsociados) {
		this.lstAsociados = lstAsociados;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
