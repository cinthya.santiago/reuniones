package recaudacion.bean.entidades.dao;

import java.util.List;
import java.util.Vector;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.CatalogoVistas;

public class CatalogoVistasDAO extends DAO {

	public List<CatalogoVistas> buscaCatalogoVistas() {
		List<CatalogoVistas> lstCatalogoVistas = new Vector<CatalogoVistas>();
		try {
			String hql = getQueryCatalogoVistas();
			Query<CatalogoVistas> query = getDb().createQuery(hql,CatalogoVistas.class);
			lstCatalogoVistas = query.list();
			return lstCatalogoVistas;
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	private String getQueryCatalogoVistas() {
		return "select x from CatalogoVistas where x.estatus = 1 ";
	}
}
