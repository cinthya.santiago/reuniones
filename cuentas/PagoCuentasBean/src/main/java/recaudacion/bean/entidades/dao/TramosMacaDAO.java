package recaudacion.bean.entidades.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import org.hibernate.query.Query;

import asu.bean.entidades.dao.DAO;
import infraestructura.comun.utilerias.MensajesDebug;
import recaudacion.bean.entidades.ccuentas.TramoMarca;

public class TramosMacaDAO extends DAO {
	private List<TramoMarca> lstTramoMarca = null;
	private TramoMarca tramoMarca = null;

	public List<TramoMarca> buscaTramoMarca(Integer idMarca) {
		try {
			String hql = queryBuscaTramoMarca(idMarca);

			Query<TramoMarca> query = getDb().createQuery(hql,TramoMarca.class);
			setLstTramoMarca(query.list());
			return getLstTramoMarca();

		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	private String queryBuscaTramoMarca(Integer idMarca) {
		String sql = "select x from recaudacion.bean.entidades.ccuentas.TramoMarca x where x.estatus = 1 ";
		if (idMarca != null) {
			sql += "and x.key.marca.id = " + idMarca;
		}
		return sql;
	}
	
	public TramoMarca buscaTramoMarcaPorDescripcion(String descripcion) {
		try {
			String hql = queryBuscaTramoMarcaPorDescripcion(descripcion);

			Query<TramoMarca> query = getDb().createQuery(hql,TramoMarca.class);
			TramoMarca tramo = (TramoMarca)query.uniqueResult();
			
			return tramo;
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	private String queryBuscaTramoMarcaPorDescripcion(String descripcion) {
		String sql = "select x from recaudacion.bean.entidades.ccuentas.TramoMarca x where x.estatus = 1 ";
		return sql;
	}

	
	public Integer guardaTramoMarca() throws Exception {
		Integer idTramo = null;
		if(getTramoMarca().getTramoMarcaId() == null){
			idTramo = recuperaSecuenciaTramosMarca();
			getTramoMarca().setTramoMarcaId(idTramo);
		}
		try {
			guarda(getTramoMarca());
			idTramo = getTramoMarca().getTramoMarcaId();
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
		return idTramo;
	}

	public Integer recuperaSecuenciaTramosMarca() throws Exception {
		try {
			PreparedStatement ps1 = null;

				String query1 = "SELECT CCUENTAS.TRAMOS_MARCA_SEQ.NEXTVAL FROM DUAL";
				ps1 = getConexion().prepareStatement(query1);
				ResultSet rs = ps1.executeQuery();
				if (rs.next()) 
					return rs.getInt("NEXTVAL");
				throw new Exception("No se encontro secuencia");
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}

	
	
	public void eliminaTramoMarca() {

		try {

			getDb().delete(getTramoMarca());

		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		} 
	}

	public List<TramoMarca> getLstTramoMarca() {
		return lstTramoMarca;
	}

	public void setLstTramoMarca(List<TramoMarca> lstTramoMarca) {
		this.lstTramoMarca = lstTramoMarca;
	}

	public TramoMarca getTramoMarca() {
		return tramoMarca;
	}

	public void setTramoRol(TramoMarca tramoMarca) {
		this.tramoMarca = tramoMarca;
	}
}