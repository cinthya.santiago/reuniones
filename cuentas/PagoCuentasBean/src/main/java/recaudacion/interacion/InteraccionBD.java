package recaudacion.interacion;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import asu.bean.transaccion.Transaccion;
import infraestructura.comun.utilerias.MensajesDebug;



public abstract class InteraccionBD extends Transaccion{
	
	
	private static final long serialVersionUID = 1L;

	protected <T> List<T> busca(String query,Fetch<T> fetch)
	{
		List<T> elementos = new ArrayList<T>();
		PreparedStatement ps =null;
		ResultSet rs= null;
		
		try {
			ps = getConexion().prepareStatement(query);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				T objeto = fetch.fetch(rs);
				elementos.add(objeto);
			}
			
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
		finally{
			closeResources( ps, rs);
		}
		return elementos;
	}

	protected <T> List<T> busca(String query,List<Object> parametros,Fetch<T> fetch) throws Exception
	{
		List<T> elementos= new ArrayList<T>();
		PreparedStatement ps =null;
		ResultSet rs= null;
		
		try {
			ps = getConexion().prepareStatement(query);
			ps.setFetchSize(1000);
			
			for (int i = 0; i < parametros.size(); i++) {
				Object param = parametros.get(i);
				if(param instanceof Date)
					ps.setTimestamp(i+1, new Timestamp(((Date)param).getTime()));
				else
					ps.setObject(i+1, param);
			}
			
			rs = ps.executeQuery();
			while (rs.next()) {
				T objeto = fetch.fetch(rs);
				elementos.add(objeto);
			}
			
		} catch (Exception e) {

			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
		finally{
			closeResources(ps, rs);
		}
		return elementos;
	}
	
	protected <T> List<T> busca(String query, Object parametro,Fetch<T> fetch) throws Exception
	{
		List<T> elementos= new ArrayList<T>();
		PreparedStatement ps =null;
		ResultSet rs= null;

		try {
			ps = getConexion().prepareStatement(query);
			ps.setFetchSize(1000);

			if(parametro instanceof Date)
				ps.setTimestamp(1, new Timestamp(((Date)parametro).getTime()));
			else
				ps.setObject(1, parametro);

			rs = ps.executeQuery();
			while (rs.next()) {
				T objeto = fetch.fetch(rs);
				elementos.add(objeto);
			}

		} catch (Exception e) {

			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
		finally{
			closeResources(ps, rs);
		}
		return elementos;
	}
	
	protected <T> T buscaUnElemento(String query,Fetch<T> fetch)
	{
		PreparedStatement ps =null;
		ResultSet rs= null;
		
		try {
			ps = getConexion().prepareStatement(query);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				T objeto = fetch.fetch(rs);
				return objeto;
			}
			
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
		}
		finally{
			closeResources( ps, rs);
		}
		return null;
	}
	
	protected int ejecuta(String query,@SuppressWarnings("rawtypes") List parametros) throws Exception
	{
		int rows = 0;
		PreparedStatement ps = null;
		
		try {
			ps = getConexion().prepareStatement(query);
			
			System.out.println("+++++++++++++++++++++++");
			for (int i = 1; i < parametros.size()+1; i++) {
				int j = i-1;
				Object param = parametros.get(j);
				System.out.print(param+",");
				if(param instanceof Date){
					ps.setTimestamp(i, new Timestamp(((Date)param).getTime()));
				}
				else if(param instanceof BigDecimal){
					ps.setBigDecimal(i, ((BigDecimal)param).setScale(2, BigDecimal.ROUND_HALF_UP));
				}
				else{
					ps.setObject(i, param);
				}
			}
			System.out.println("");
			rows = ps.executeUpdate();
			
			MensajesDebug.imprimeMensaje("Se afectaron "+rows);
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
		finally {
			closeResources( ps, null);
		}
		return rows;
	}
	
	protected void ejecutaBatch(String query, List<List<Object>> elementos) throws Exception
	{
		PreparedStatement ps = null;
		
		try {
			MensajesDebug.imprimeMensaje("["+getClass().getCanonicalName()+"] Iniciando proceso batch.");
			getConexion().setAutoCommit(false);
			ps = getConexion().prepareStatement(query,PreparedStatement.NO_GENERATED_KEYS);
			
			ps.setFetchSize(500);
			
			int rows = 0;
			
			Iterator<List<Object>> itElementos = elementos.iterator();
			while (itElementos.hasNext()) 
			{
				
				List<Object> parametros = (List<Object>) itElementos.next();
				
				for (int i = 1; i < parametros.size()+1; i++) 
				{
					int j = i-1;
//					System.out.println(param.toString());
					Object param = parametros.get(j);
					if(param instanceof Date){
						ps.setTimestamp(i, new Timestamp(((Date)param).getTime()));
					}
					else if(param instanceof BigDecimal){
						ps.setBigDecimal(i, ((BigDecimal)param).setScale(2, BigDecimal.ROUND_HALF_UP));
					}
					else{
						ps.setObject(i, param);
					}
				}
				ps.addBatch();
				
				if(rows % 2000 == 0)
				{
					MensajesDebug.imprimeMensaje("Ejecutando "+rows+" ....");
					ps.executeBatch();
				}
				rows ++;
			}
			ps.executeBatch();
			MensajesDebug.imprimeMensaje("["+getClass().getCanonicalName()+"] Termino de ejecutar batch.");
			
			MensajesDebug.imprimeMensaje("["+getClass().getCanonicalName()+"] Se afectaron "+rows+".");
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		}
		finally {
			closeResources( ps, null);
		}
	}
	

    protected void closeResources(PreparedStatement ps, ResultSet rs){
        try {
            if (ps != null){
                ps.close();
            }
        } catch (SQLException e) {
            MensajesDebug.imprimeMensaje(e);
        }
        
        try {
            if (rs != null){
                rs.close();
            }
        } catch (SQLException e) {
            MensajesDebug.imprimeMensaje(e);
        }
    }
	
	
}