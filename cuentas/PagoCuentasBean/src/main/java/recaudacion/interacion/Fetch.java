package recaudacion.interacion;

import java.sql.ResultSet;

public interface Fetch<T> {
	
	T fetch(ResultSet rs) throws Exception;
}