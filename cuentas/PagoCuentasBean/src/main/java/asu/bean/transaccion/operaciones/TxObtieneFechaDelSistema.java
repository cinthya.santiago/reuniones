/**
 * 
 */
package asu.bean.transaccion.operaciones;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.Date;

import asu.bean.transaccion.Transaccion;
import infraestructura.comun.utilerias.MensajesDebug;

/**
 *
 * @org VSP
 * @author DIEGO LUNA
 * @date 21/08/2014
 * @description 
 * 
 */
public class TxObtieneFechaDelSistema extends Transaccion
{
	private Date sysDate = null;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Serializable ejecutaTransaccion() throws Exception
	{
		
		try {
			ResultSet rs = getConexion().createStatement().executeQuery("SELECT SYSDATE FROM DUAL");
			if(rs.next())
				sysDate = rs.getTimestamp("SYSDATE");
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e); 
			throw e;
		} 
		
		return this;
	}

	/**
	 * @return the sysDate
	 */
	public Date getSysDate()
	{
		return sysDate;
	}

}
