package asu.bean.transaccion.catalogos;

import java.io.Serializable;

//import asu.bean.entidades.ccuentas.ValorTarjeta;
//import asu.bean.entidades.dao.ccuentas.ValorTarjetaDAO;
//import asu.bean.entidades.dao.general.MarcaDAO;
//import asu.bean.entidades.general.Marca;
import asu.bean.transaccion.Transaccion;

public class TxMttoTarjetaCuenta extends Transaccion {

	private static final long serialVersionUID = 1L;

	public static final int BUSCA_MARCAS_ACTIVAS = 1;
	public static final int BUSCA_TODAS_VALOR_TARJETA = 2;
	public static final int BUSCA_VALOR_TARJETA_XMARCA = 3;
	public static final int INACTIVA_VALOR_TARJETA = 4;
	public static final int GUARDA_VALOR_TARJETA = 5;
	public static final int BUSCA_TARJETAS_POR_GRUPO_CONDUCTORES = 6;

@Override
	public Serializable ejecutaTransaccion() throws Exception {
//		switch (opcion.intValue()) {
//		case BUSCA_MARCAS_ACTIVAS:
//			buscaMarcasActivas();
//			break;
//
//		case BUSCA_TODAS_VALOR_TARJETA:
//			buscaValorTarjeta();
//			break;
//
//		case BUSCA_VALOR_TARJETA_XMARCA:
//			buscaValorTarjetaXMarca();
//			break;
//
//		case GUARDA_VALOR_TARJETA:
//			guardaTarjetaValor();
//			break;
//
//		case BUSCA_TARJETAS_POR_GRUPO_CONDUCTORES:
//			buscaTarjetasPorGrupoConductores();
//			break;
//
//		default:
//			break;
//		}
		return this;
	}
//
//	private void buscaMarcasActivas() {
//		try {
//			MarcaDAO dao = new MarcaDAO();
//			dao.buscaMarcas();
//			setvMarca(dao.getvMarcas());
//		} catch (Exception e) {
//			MensajesDebug.imprimeMensaje(e);
//		}
//	}
//
//	private void buscaValorTarjeta() {
//		try {
//			ValorTarjetaDAO dao = new ValorTarjetaDAO();
//			dao.buscaTodasTarjetas();
//			setvTarjetas(dao.getvTarjetas());
//		} catch (Exception e) {
//			MensajesDebug.imprimeMensaje(e);
//		}
//	}
//
//	private void buscaValorTarjetaXMarca() {
//		try {
//			ValorTarjetaDAO dao = new ValorTarjetaDAO();
//			dao.setMarcaId(getMarcaId());
//			dao.buscaValorTarjetasXmarca();
//			setvTarjetas(dao.getvTarjetas());
//		} catch (Exception e) {
//			MensajesDebug.imprimeMensaje(e);
//		}
//	}
//
//	private void guardaTarjetaValor() {
//		try {
//			ValorTarjetaDAO dao = new ValorTarjetaDAO();
//			dao.setTarjeta(getValorTarjeta());
//			dao.guardaTarjeta();
//		} catch (Exception e) {
//			MensajesDebug.imprimeMensaje(e);
//		}
//	}
//
//	private void buscaTarjetasPorGrupoConductores(){
//		try {
//			ValorTarjetaDAO dao = new ValorTarjetaDAO();
//			setLstTarjetas(dao.consultaTarjetasPorGrupoConductores(getIdsConductores()));
//			System.out.println();
//		} catch (Exception e) {
//			MensajesDebug.imprimeMensaje(e);
//		}
//	}
//	public Vector<Marca> getvMarca() {
//		return vMarca;
//	}
//
//	public void setvMarca(Vector<Marca> vMarca) {
//		this.vMarca = vMarca;
//	}
//
//	public Integer getOpcion() {
//		return opcion;
//	}
//
//	public void setOpcion(Integer opcion) {
//		this.opcion = opcion;
//	}
//
//	public Vector<ValorTarjeta> getvTarjetas() {
//		return vTarjetas;
//	}
//
//	public void setvTarjetas(Vector<ValorTarjeta> vTarjetas) {
//		this.vTarjetas = vTarjetas;
//	}
//
//	public Integer getMarcaId() {
//		return marcaId;
//	}
//
//	public void setMarcaId(Integer marcaId) {
//		this.marcaId = marcaId;
//	}
//
//	public ValorTarjeta getValorTarjeta() {
//		return valorTarjeta;
//	}
//
//	public void setValorTarjeta(ValorTarjeta valorTarjeta) {
//		this.valorTarjeta = valorTarjeta;
//	}
//
//	public String getIdsConductores() {
//		return idsConductores;
//	}
//
//	public void setIdsConductores(String idsConductores) {
//		this.idsConductores = idsConductores;
//	}
//
//	public List<TarjetaCuenta> getLstTarjetas() {
//		return lstTarjetas;
//	}
//
//	public void setLstTarjetas(List<TarjetaCuenta> lstTarjetas) {
//		this.lstTarjetas = lstTarjetas;
//	}

}
