package asu.bean.transaccion.operaciones;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.hibernate.query.Query;

import asu.bean.entidades.dto.ObjetoGenerico;
import asu.bean.transaccion.Transaccion;

/**
*
* @org VSP
* @author DIEGO LUNA
* @date 05/08/2014
* @description 
* 
*/

public class TxBusquedaListaObjetos extends Transaccion {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<?> lista = null;
	
	private ObjetoGenerico objBusqueda = null;
		
	private ArrayList<?> aListaObjetos = null;
	
	private Vector<Vector<Object>> vDatos = null; 
	

	@Override
	public Serializable ejecutaTransaccion()
	{
		Query<?> consulta = getDb().createQuery(getObjBusqueda().getQueryGenerico());
		consulta.setMaxResults(50);
		
		int cont =0;

		for(Object x:getObjBusqueda().getParametros())
		{
			consulta.setParameter(cont, x);
			cont++;
		}
		lista = consulta.list();
		
		armaDatos();
		return this;
	}

	private void armaDatos()
	{
		if(getLista() != null && !getLista().isEmpty())
		{
			ArrayList<Object> listaObj = new ArrayList<Object>();
			
			for(Object c:getLista())
			{
				if(vDatos == null)
				{
					vDatos = new Vector<Vector<Object>>();
				}
				
				ObjetoGenerico obj = (ObjetoGenerico) c;
				
				Vector<Object> vRenglon = new Vector<Object>();
				
				vRenglon.add(obj.getClave());
				vRenglon.add(obj.getDescripcion());
				vRenglon.add(obj);
				
				vDatos.add(vRenglon);
				listaObj.add(obj);
				
			}
			
			aListaObjetos = listaObj;
			
		}
	}

	public List<?> getLista() {
		return lista;
	}

	public void setLista(List<?> lista) {
		this.lista = lista;
	}


	/**
	 * @param objBusqueda the objBusqueda to set
	 */
	public void setObjBusqueda(ObjetoGenerico objBusqueda) {
		this.objBusqueda = objBusqueda;
	}

	/**
	 * @return the objBusqueda
	 */
	public ObjetoGenerico getObjBusqueda() {
		return objBusqueda;
	}

	/**
	 * @return the vDatos
	 */
	public Vector<Vector<Object>> getvDatos() {
		return vDatos;
	}

	/**
	 * @return the aListaObjetos
	 */
	public ArrayList<?> getaListaObjetos()
	{
		return aListaObjetos;
	}



}
