package asu.bean.transaccion;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.hibernate.Session;

import infraestructura.comun.utilerias.MensajesDebug;

public abstract class  Transaccion implements Serializable
{
	
	
	private static final long serialVersionUID = 1L;
	private transient Session db;
	private transient Connection conexion;
	
	public abstract Serializable ejecutaTransaccion() throws Exception;

	public Session getDb() 
	{
		return db;
	}

	public void setDb(Session db) 
	{
		this.db = db;
	}

	public Connection getConexion() {
		return conexion;
	}


	public void setConexion(Connection conexion) {
		this.conexion = conexion;
	}
	
	public void closePreparedStatement(PreparedStatement ps) {
		if(ps != null) {
			try {
				ps.close();
			} catch (SQLException e) {
				MensajesDebug.imprimeMensaje(e);
			}
		}
	}
	
	public void closeResultSet(ResultSet rs) {
		if(rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				MensajesDebug.imprimeMensaje(e);
			}
		}
	}
	

}
