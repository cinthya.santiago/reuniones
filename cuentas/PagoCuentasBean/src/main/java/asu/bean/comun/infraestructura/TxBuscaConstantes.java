package asu.bean.comun.infraestructura;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.hibernate.jdbc.Work;




import asu.bean.transaccion.Transaccion;
import infraestructura.comun.utilerias.MensajesDebug;

public class TxBuscaConstantes extends Transaccion 
{

	private HashMap<String, Object>  constantes = new HashMap<String, Object>();
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public HashMap<String, Object> getConstantes() {
		return constantes;
	}

	public void setConstantes(HashMap<String, Object> constantes) {
		this.constantes = constantes;
	}

	@Override
	public Serializable ejecutaTransaccion() throws Exception {
		BuscaDatos  buscaDatos = new BuscaDatos();
		getDb().doWork(buscaDatos);
		setConstantes(buscaDatos.getConstantes());
		return this;
	}


	private class BuscaDatos implements Work{

		private HashMap<String, Object>  constantes = new HashMap<String, Object>();

		@Override
		public void execute(Connection arg0) throws SQLException 
		{
			setConstantes(new HashMap<String, Object>());
			PreparedStatement ps = null;
			ResultSet rs = null;


			String query = "SELECT * FROM CCUENTAS.CONSTANTES C WHERE C.ESTATUS = 1";
			try 
			{
				ps = arg0.prepareStatement(query);
				rs = ps.executeQuery();
				while (rs.next()) 
				{
					String descripcion = rs.getString("DESCRIPCION");
					String valor = rs.getString("VALOR");

					getConstantes().put(descripcion, valor);
				}

			} catch (Exception e) 
			{
				MensajesDebug.imprimeMensaje(e);
			}
			finally{
				closeResultSet(rs);
				closePreparedStatement(ps);
			}

		}

		public HashMap<String, Object> getConstantes() {
			return constantes;
		}

		public void setConstantes(HashMap<String, Object> constantes) {
			this.constantes = constantes;
		}
	}
}
