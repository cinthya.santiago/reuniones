package asu.bean.entidades.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.hibernate.Session;

import infraestructura.comun.utilerias.MensajesDebug;

public abstract class DAO 
{

	private Session db;
	private Connection conexion;
	
	
	public Session getDb() {
		return db;
	}

	public void setDb(Session db) {
		this.db = db;
	}

	public Connection getConexion() {
		return conexion;
	}

	public void setConexion(Connection conexion) {
		this.conexion = conexion;
	}

	public void cierraRecursos(PreparedStatement ps, ResultSet rs)throws Exception
	{
		if(rs != null)
		{
			rs.close();
		}
		
		if(ps != null)
		{
			ps.close();
		}
	}
	
	public Object guarda(Object obj) throws Exception {
		try {
			getDb().saveOrUpdate(obj);
			return obj;
		} catch (Exception e) {
			MensajesDebug.imprimeMensaje(e);
			throw e;
		} 
	}
	
	
}

