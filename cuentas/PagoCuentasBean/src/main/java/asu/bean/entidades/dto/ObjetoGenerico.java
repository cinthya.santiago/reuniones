package asu.bean.entidades.dto;

import java.io.Serializable;
import java.util.Vector;

/**
*
* @org VSP
* @author DIEGO LUNA
* @date 08/08/2014
* @description CLASE WRAPPER PARA ENTIDADES
* 
*/
public abstract class ObjetoGenerico implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	protected Vector<Object> parametros = new Vector<Object>(10);
	
	protected String queryGenerico = "";
	
	public abstract Integer getId();

	public abstract void setId(Integer id);
	
	public abstract void setClave(String clave);
	
	public abstract String getClave();

	public abstract String getDescripcion();

	public abstract void setDescripcion(String newDescripcion);

	public abstract void getQueryPorId();
	
	public abstract void getQueryPorClave();
	
	public abstract void getQueryPorDescripcion();
	
	
	public Vector<Object> getParametros() 
	{
		return parametros;
	}
	
	public  void setParametros(Vector<Object> parametros)
	{
		this.parametros= parametros;
	}

	public String getQueryGenerico()
	{
		
		return queryGenerico;
	}

	public void setQueryGenerico(String queryGenerico) 
	{
		this.queryGenerico = queryGenerico;
	}

}
