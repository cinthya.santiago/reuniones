package asu.bean.serv.impl;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.internal.SessionImpl;

import asu.bean.transaccion.Transaccion;
import infraestructura.comun.utilerias.MensajesDebug;

public class Ejecutor {

	public Serializable ejecuta(Transaccion tx) throws Exception {
		
		Session session = SessionAccesoDatos.getSesion().openSession();
		Transaction unaTx = session.beginTransaction();
		
		try {
			MensajesDebug.imprimeMensaje("INICIO "+new  Date()+" "+tx.getClass().getName());

			Connection conexion = ((SessionImpl)session).connection();
			
			tx.setDb(session);
			tx.setConexion(conexion);
			tx = (Transaccion) tx.ejecutaTransaccion();
			
			unaTx.commit();
		} 
		catch (Exception e) 
		{
			MensajesDebug.imprimeMensaje(e);
			unaTx.rollback();
			throw e;
		}
		finally {
			session.close();
		}
		MensajesDebug.imprimeMensaje("FIN "+new  Date()+" "+tx.getClass().getName());

		return tx;
	}

}
