package asu.bean.serv.inter;

import java.io.Serializable;

import javax.ejb.Remote;

@Remote
public interface SesionEJB {
	
	public Serializable ejecutaTransaccion(byte[] pbyteArr) throws Exception;
		

}
