/* ====================================================================
 *
 * Licencia DMH.
 *
 * Copyright (c) 2002-2004 DMH.  Todos los derechos reservados.
 *
 * La redistribucion y uso en forma de codigo fuente o binaria, con o sin
 * modificacion, estan permitidos siempre que las siguientes condiciones
 * se cumplan:
 *
 * 1. Redistribuciones del codigo fuente deben retener el aviso de derechos
 *    de autor, esta lista de condiciones y el aviso de no responsabilidad
 *    anexo.
 *
 * 2. Redistribuciones en formato binario deben reproducir el aviso de
 *    derechos de autor, esta lista de condiciones y el aviso de no
 *    responsabilidad anexo en la documentacion y/o en otros materiales
 *    anexos en la distribucion.
 
 * 3. La documentacion de usuario final incluida con la redistribucion, si
 *    existe alguna, debe incluir el siguiente reconocimiento:
 *       "Este producto incluye software desarrollado por DMH
 *        (http://www.dmh.com.mx/)."
 *    Alternativamente, este reconocimiento puede aparecer en el software directamente,
 *    si y en donde este tipo de reconocimientos a terceras partes normalmente se muestran.
 *
 * 4. El nombre DMH no debera ser usado para apoyar o promover productos derivados
 *    de este software sin previa autorizacion. Para permisos escritos, por favor
 *    contactar a contacto@dmh.com.mx.
 *
 * 5. Productos derivados de este software no pueden ser llamados "DMH"
 *    ni DMH puede aparecer en sus nombres sin autorizacion previa por
 *    escrito de DMH.
 *
 * ESTE SOFTWARE SE PROVEE ``COMO ESTA'' Y CUALQUIER GARANTIA EXPRESA O
 * IMPLICADA, INCLUYENDO, PERO SIN LIMITACION A, LAS GARANTIAS IMPLICADAS
 * DE COMERCIALIZACION Y APLICABILIDAD PARA UN PROPOSITO PARTICULAR ESTAN
 * EXCLUIDAS.  EN NINGUN EVENTO DMH O LA GENTE QUE CONTRIBUYO AL DESARROLLO
 * DE ESTE SOFTWARE SERA RESPONSABLE POR NINGUN DANO DIRECTO, INDIRECTO,
 * INCIDENTAL, ESPECIAL, EJEMPLAR, O CONSECUENTE (INCLUIDOS, PERO NO LIMITADOS
 * A, LA PROCURACION DE SERVICIOS O BIENES SUBSTITUTOS; PERDIDA DE USO, DATOS,
 * O GANACIAS; O INTERRUPCION DEL NEGOCIO) CUALQUIER CAUSAL Y EN CUALQUIER
 * TEORIA DE RESPONSABILIDAD, YA SEA BAJO UN CONTRATO, RESPONSABILIDAD ESTRICTA,
 * (INCLUYENDO NEGLIGENCIA O CUALQUIER OTRA) COMO RESULTADO DE CUALQUIER MANERA
 * POR EL USO DE ESTE SOFTWARE, AUN CUANDO SE HAYA AVISADO DE LA POSIBILIDAD
 * DE ESTE TIPO DE DANOS.
 * ====================================================================
 */
package asu.bean.serv.inter;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Vector;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

import infraestructura.comun.utilerias.MensajesDebug;


/**
 * Esta clase provee la facilidad de convertir un objeto Java a un arreglo
 * de Bytes para poder facilitar su serializaci&oacute;n.
 * <P>
 * Incluye una funcion para comprimir el tamaño del objeto ya serializado y
 * eficientar el proceso de transferencia.
 * 
 * @author DMH
 */
public class Serializador
{
    
    /*
     Serializa un objeto en un arreglo de bytes 
     
     */
    // protected static final int MAXIMO_ARRAY=8000000;
    
    /**
     * Este m&eacute;todo convierte el objeto en un arreglo de bytes e 
     * incluye un proceso de compresi&oacute;n.
     * 
     * @return Byte Array con el objeto serializado
     * @param pOObjeto el objeto a serializar
     */
    public static byte[] ObjToByteArr(Object pOObjeto) throws Exception
    {
        try
        {
            
            ByteArrayOutputStream lbyteArr = new ByteArrayOutputStream();
            Deflater miCompresor = new Deflater(Deflater.BEST_SPEED, true);
            DeflaterOutputStream dos = new DeflaterOutputStream(lbyteArr, miCompresor);
            ObjectOutputStream lOOutStrm = new ObjectOutputStream(dos);
            lOOutStrm.writeObject(pOObjeto);
            lOOutStrm.close();
            
            byte[] m_array = lbyteArr.toByteArray();
            
            MensajesDebug.imprimeMensaje(
                    "Compresion: Bytes sin comprimir:"
                    + miCompresor.getTotalIn()
                    + " Bytes comprimidos:"
                    + miCompresor.getTotalOut());
            
            return m_array; //salida;
            
        }
        catch (Exception e)
        {
            MensajesDebug.imprimeMensaje("Error Serializacion: " + e.getMessage());
            MensajesDebug.imprimeMensaje(e);
            throw new Exception();
        }
    }
    
    /**
     * Este m&eacute;todo regresa un objeto a partir de un arreglo de bytes
     * en el que fue convertido al serializarse.
     * 
     * @return Object el objeto reconstruido.
     * @param pbyteArr el array donde se encuentra el objeto serialiado.
     */
    public static Object byteArrToObj(byte[] pbyteArr) throws Exception
    {
        try
        {
            
            Inflater inf = new Inflater(true);
            InflaterInputStream iis =
                new InflaterInputStream(new ByteArrayInputStream(pbyteArr), inf);
            ObjectInputStream in = new ObjectInputStream(iis);
            Object miObjeto = in.readObject();
            in.close();
            MensajesDebug.imprimeMensaje(
                    "Descompresion: Bytes comprimidos:"
                    + inf.getTotalIn()
                    + " Bytes sin comprimir:"
                    + inf.getTotalOut());
            
            return miObjeto;
        }
        catch (Exception e)
        {
            MensajesDebug.imprimeMensaje("Error Deserializacion: " + e.getMessage());
            MensajesDebug.imprimeMensaje(e);
            throw e;
        }
    }
    
    public static Object getClonVector(Vector<?> pVector)
    {
        Object lOVector = new Object();
        byte[] resultado;
        
        try
        {
            resultado = ObjToByteArr(pVector);
            lOVector = byteArrToObj(resultado);
        }
        catch (Exception e)
        {
            
            MensajesDebug.imprimeMensaje(" Exception : " + e + " -" + e.getMessage());
        }
        return lOVector;
    }
    
    /**
     * M&eacute;todo para clonar objetos.
     * @author Ing. Gabriel Dolores Garc&iacute;a.
     * @param objToClone
     * @return Una copia del objeto a clonar, null si el objeto no se puede clonar. 
     * @since 15/05/2006
     */
    public static Object getClon(Object objToClone)
    {
        Object clon = null;
        try
        {
            clon = byteArrToObj(ObjToByteArr(objToClone));
        }
        catch (Exception e)
        {
            MensajesDebug.imprimeMensaje(" Exception : " + e + " -" + e.getMessage());
        }
        return clon;
    }
    
}