/**
 * 
 */
package asu.bean.serv.impl;


/**
 * @author LUNA
 *ee
 */
public class Constantes
{
	public static String dsPool = "erpco/DS/arp/poolcomun";
	public static String AUTORIZA_CANCELACION = "393";
	public static String AUTORIZA_AJUSTE = "393";
	public static Integer CONCEPTO_PISTA = 6;
	public static Integer CONCEPTO_ADEUDO = 2;
	
	/*
	 * Constantes centro de pago
	 * */
	public static final String INICIO = "INICIO";
	public static final String FIN = "FIN";
	public static final String CENTROPAGO = "CENTROPAGO";
	public static final String REGION= "REGION";
	public static final String MARCA= "MARCA";
}
