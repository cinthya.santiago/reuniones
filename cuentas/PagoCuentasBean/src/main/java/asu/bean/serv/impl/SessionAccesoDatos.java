package asu.bean.serv.impl;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionAccesoDatos {
	


	private static SessionFactory sesion;

	public static SessionFactory getSesion() {
		
		if(sesion == null ){
			Configuration configuracion = new Configuration().configure("/asu/bean/serv/impl/hibernate_cfg.xml");
//			ServiceRegistry registroServicio = new ServiceRegistryBuilder().applySettings(configuracion.getProperties()).buildServiceRegistry();
//			sesion = configuracion.buildSessionFactory(registroServicio);
			
			sesion = configuracion.buildSessionFactory();
		}
		return sesion;
	}

}
