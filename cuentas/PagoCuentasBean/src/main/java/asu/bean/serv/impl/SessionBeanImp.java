package asu.bean.serv.impl;

import java.io.Serializable;

import javax.ejb.Stateless;

import asu.bean.serv.inter.Serializador;
import asu.bean.serv.inter.SesionEJB;
import asu.bean.transaccion.Transaccion;
import infraestructura.comun.utilerias.MensajesDebug;

@Stateless(mappedName="fachada/SessionAccesoServer")
public class SessionBeanImp implements SesionEJB {
	
	private Ejecutor ejecutor;
	
	public SessionBeanImp() {
		ejecutor = new Ejecutor();
	}

	@Override
	public Serializable ejecutaTransaccion(byte[] pbyteArr) throws Exception {
		
		Transaccion tx = (Transaccion) Serializador.byteArrToObj(pbyteArr);
		MensajesDebug.imprimeMensaje("** Ejecuta transaccion "+tx.getClass().getName()+" **");
		tx = (Transaccion) ejecutor.ejecuta(tx);
		MensajesDebug.imprimeMensaje("** Fin transaccion "+tx.getClass().getName()+" **");
		byte[] resultado = Serializador.ObjToByteArr(tx);
		return resultado;
	}


}
